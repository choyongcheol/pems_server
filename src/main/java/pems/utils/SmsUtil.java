package pems.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.codec.binary.Base64;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import pems.Constants;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Component
public class SmsUtil {

	/**
	 * SMS 발송
	 */
	public static String toSendSMS(String phoneNumber, String message) throws Exception {
		HttpHeaders headers = new HttpHeaders();
		Map<String, Object> params = new HashMap<>();
		Map<String, Object> toNumber = new HashMap<>();
		long time = LocalDateTime.now(ZoneId.of("UTC")).toInstant(ZoneOffset.UTC).toEpochMilli();

		headers.add("content-type", "application/json");
		headers.add("x-ncp-iam-access-key", Constants.ACCESS_KEY);
		headers.add("x-ncp-apigw-timestamp", Long.toString(time));
		headers.add("x-ncp-apigw-signature-v2", makeSignature(Constants.SMS_URL, time));
		params.put("type", "SMS");
		params.put("from", "0312990100");
		params.put("content", message);
		toNumber.put("to", phoneNumber);
		params.put("messages", Collections.singletonList(toNumber));

		ObjectMapper mapper = new ObjectMapper();
		String body = mapper.writeValueAsString(params);
		HttpEntity<?> request = new HttpEntity<>(body, headers);
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8"))) ;

		return restTemplate.postForObject(new URI(Constants.N_CLOUD + Constants.SMS_URL), request, String.class);
	}

	/*
		지금 등록된 네이버 클라우드에 템블릿 wonikpne : 원익피앤이입니다.'테스트(test)'
	{
                "plusFriendId":"@원익피앤이",
                "templateCode":"wonikpne",
                "messages":[
                    {
                        "to":"01012345678",
                        "content":"원익피앤이입니다.'테스트(test)'"
                    }
                ]
        }*/
	public static String toSendKakao(String phoneNumber, String templateCode, String message) throws Exception {
		HttpHeaders headers = new HttpHeaders();
		Map<String, Object> params = new HashMap<>();
		Map<String, Object> messages = new HashMap<>();
		long time = LocalDateTime.now(ZoneId.of("UTC")).toInstant(ZoneOffset.UTC).toEpochMilli();

		headers.add("content-type", "application/json");
		headers.add("x-ncp-iam-access-key", Constants.ACCESS_KEY);
		headers.add("x-ncp-apigw-timestamp", Long.toString(time));
		headers.add("x-ncp-apigw-signature-v2", makeSignature(Constants.ALIMTALK_URL, time));

		params.put("plusFriendId", "@원익피앤이");
		params.put("templateCode", templateCode);
		messages.put("to", phoneNumber);
		messages.put("content", message);
		params.put("messages", Collections.singletonList(messages));

		ObjectMapper mapper = new ObjectMapper();
		String body = mapper.writeValueAsString(params);
		HttpEntity<?> request = new HttpEntity<>(body, headers);
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8"))) ;

		return restTemplate.postForObject(new URI(Constants.N_CLOUD + Constants.ALIMTALK_URL), request, String.class);
	}


	public static String makeSignature(String url, Long time) throws Exception {
		String space = " "; // one space
		String newLine = "\n"; // new line
		String method = "POST"; // method
		String timestamp = Long.toString(time); // current timestamp (epoch)
		String message = method + space + url + newLine + timestamp + newLine + Constants.ACCESS_KEY;
		SecretKeySpec signingKey = new SecretKeySpec(Constants.SECRET_KEY.getBytes(StandardCharsets.UTF_8), "HmacSHA256");
		Mac mac = Mac.getInstance("HmacSHA256");
		mac.init(signingKey);
		byte[] rawHmac = mac.doFinal(message.getBytes(StandardCharsets.UTF_8));
		return Base64.encodeBase64String(rawHmac);
	}






	/*
	public static String toSendMessage_22(String phoneNumber, String message) throws Exception {

		//카카오토 호출
		toSendKakao(phoneNumber);

		//SMS
		String nCloudUri = "https://sens.apigw.ntruss.com/sms/v2/services/" + Constants.KEY_ID + "/messages";
		long time = LocalDateTime.now(ZoneId.of("UTC")).toInstant(ZoneOffset.UTC).toEpochMilli();

		HttpHeaders headers = new HttpHeaders();
		headers.add("content-type", "application/json");
		headers.add("x-ncp-apigw-timestamp", Long.toString(time));
		headers.add("x-ncp-iam-access-key", Constants.ACCESS_KEY);
		headers.add("x-ncp-apigw-signature-v2", makeSignature(Constants.SMS_URL, time));

		Map<String, Object> params = new HashMap<>();
		Map<String, Object> toNumber = new HashMap<>();


//        {
//            "messages": [
//                {
//                    "to": "01012345678"
//                }
//            ],
//            "from": "0312990100",
//            "type": "SMS",
//            "content": "TEST"
//        }
		params.put("type", "SMS");
		params.put("from", "0312990100");
		params.put("content", message);
		toNumber.put("to", phoneNumber);
		params.put("messages", Collections.singletonList(toNumber));

//        String body = AppContext.GSON.toJson(params);
		ObjectMapper mapper = new ObjectMapper();
		String body = mapper.writeValueAsString(params);

		HttpEntity<?> request = new HttpEntity<>(body, headers);

		RestTemplate restTemplate = new RestTemplate();
		return restTemplate.postForObject(new URI(nCloudUri), request, String.class);

	}
*/


}
