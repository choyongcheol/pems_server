package pems.utils;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;

public class BinUtil {

	/**
	 * 랜덤액세스 파일에서 short integer 2byte를 읽어서 big-endian 방식으로 리턴
	 */
	public static short readShort(RandomAccessFile in) throws IOException {
		byte[] tempB2 = new byte[2];
		in.read(tempB2, 0, 2);
		byte[] temp = {tempB2[1], tempB2[0]};
		ByteBuffer buf = ByteBuffer.wrap(temp);
		return buf.getShort();
	}

	/**
	 * 랜덤액세스 파일에서 integer 4byte를 읽어서 big-endian 방식으로 리턴
	 */
	public static int readInt(RandomAccessFile in) throws IOException {
		byte[] tempB4 = new byte[4];
		in.read(tempB4, 0, 4);

		byte[] temp = {tempB4[3], tempB4[2], tempB4[1], tempB4[0]};
		ByteBuffer buf = ByteBuffer.wrap(temp);
		return buf.getInt();
	}

	/**
	 * 랜덤액세스 파일에서 unsigned integer 4byte를 읽어서 big-endian 방식으로 리턴
	 */
	public static long readUnsignedInt(RandomAccessFile in) throws IOException {
		byte[] tempB4 = new byte[4];
		in.read(tempB4, 0, 4);

		byte[] temp = {0, 0, 0, 0, tempB4[3], tempB4[2], tempB4[1], tempB4[0]};
		ByteBuffer buf = ByteBuffer.wrap(temp);
		return buf.getLong();
	}

	/**
	 * 랜덤액세스 파일에서 Long 8byte를 읽어서 big-endian 방식으로 리턴
	 */
	public static long readLong(RandomAccessFile in) throws IOException {
		byte[] tempB8 = new byte[8];
		in.read(tempB8, 0, 8);

		byte[] temp = {tempB8[7], tempB8[6], tempB8[5], tempB8[4], tempB8[3], tempB8[2], tempB8[1], tempB8[0]};
		ByteBuffer buf = ByteBuffer.wrap(temp);
		return buf.getLong();
	}

	/**
	 * 랜덤액세스 파일에서 Float 4byte를 읽어서 big-endian 방식으로 리턴
	 */
	public static float readFloat(RandomAccessFile in) throws IOException {
		byte[] tempB4 = new byte[4];
		in.read(tempB4, 0, 4);

		byte[] temp = {tempB4[3], tempB4[2], tempB4[1], tempB4[0]};
		ByteBuffer buf = ByteBuffer.wrap(temp);
		return buf.getFloat();
	}

	public static int readByte(byte[] b, int startPos) throws IOException {
		int temp = b[startPos];

		return temp;
	}

	public static String readByte(byte[] b, int startPos, int length) throws IOException {
		byte[] c = new byte[length];

		System.arraycopy(b, startPos, c, 0, length);

		String s = new String(c, "ms949");

		return s;

	}

	public static String byteToString(byte[] b, int startPos, int length) throws IOException {
		byte[] c = new byte[length];

		System.arraycopy(b, startPos, c, 0, length);

		String s = new String(c, "ms949").trim();

		return s;

	}

	public static short byteToShort(byte[] b, int startPos) throws IOException {
		byte[] tempB2 = new byte[2];

		System.arraycopy(b, startPos, tempB2, 0, 2);

		byte[] temp = {tempB2[1], tempB2[0]};
		ByteBuffer buf = ByteBuffer.wrap(temp);
		return buf.getShort();
	}

	public static int byteToInt(byte[] b, int startPos) throws IOException {
		byte[] tempB4 = new byte[4];

		System.arraycopy(b, startPos, tempB4, 0, 4);

		byte[] temp = {tempB4[3], tempB4[2], tempB4[1], tempB4[0]};
		ByteBuffer buf = ByteBuffer.wrap(temp);

		return buf.getInt();
	}

	public static float byteToFloat(byte[] b, int startPos) throws IOException {
		byte[] tempB4 = new byte[4];

		System.arraycopy(b, startPos, tempB4, 0, 4);

		byte[] temp = {tempB4[3], tempB4[2], tempB4[1], tempB4[0]};
		ByteBuffer buf = ByteBuffer.wrap(temp);

		return buf.getFloat();
	}

	public static int byteToLong(byte[] b, int startPos) throws IOException {
		byte[] tempB4 = new byte[4];

		System.arraycopy(b, startPos, tempB4, 0, 4);

		byte[] temp = {tempB4[3], tempB4[2], tempB4[1], tempB4[0]};
		ByteBuffer buf = ByteBuffer.wrap(temp);

		return buf.getInt();
	}

	public static double byteToDouble(byte[] b, int startPos) throws IOException {
		byte[] tempB4 = new byte[8];

		System.arraycopy(b, startPos, tempB4, 0, 4);

		byte[] temp = {tempB4[7], tempB4[6], tempB4[5], tempB4[4], tempB4[3], tempB4[2], tempB4[1], tempB4[0]};
		ByteBuffer buf = ByteBuffer.wrap(temp);

		return buf.getDouble();
	}


}
