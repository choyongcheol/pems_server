package pems.utils ;

import java.util.Arrays ;
import java.util.List ;
import java.util.Map ;
import java.util.Random ;
import java.util.regex.Pattern ;

public class Utils {

	/**
	 * int 반환
	 */
	public static final int isInt(String str) {
		try {
			if (str != null) {
				if ("NULL".equals(str.toUpperCase())) return 0 ;
				else return Integer.parseInt(str) ;
			} else {
				return 0 ;
			}
		} catch (Exception e) {
			return 0 ;
		}
	}

	/**
	 * csv형식 데이터를 List 반환
	 * @param csv "1,2,3"
	 * @return ["1", "2", "3"]
	 */
	public static List<String> csvToStringList(String csv) {
		return Arrays.asList(csv.replaceAll("\\s", "").split(",")) ;
	}

	/**
	 * 좌측에 문자를 채움
	 * @param str 문자열
	 * @param n 몇칸
	 * @param c 어떤 문자
	 */
	public static String padRight(String str, int n, Object c) {
		return str + String.format("%" + (n - str.length()) + "s", "").replace(" ", String.valueOf(c)) ;
	}

	/**
	 * 우측에 문자를 채움
	 * @param str
	 * @param n
	 * @param c
	 * @return
	 */
	public static String padLeft(String str, int n, Object c) {
		return String.format("%" + (n - str.length()) + "s", "").replace(" ", String.valueOf(c)) + str ;
	}

	/**
	 * 길이 len의 무작위 hex코드 생성
	 */
	public static String getRandomHex(int len) {
		Random rnd = new Random() ;
		String str = "";
		StringBuffer temp = new StringBuffer(str) ;
		int tempLength = temp.length();

		int rIdx;
		while(tempLength < len) {
			rIdx = rnd.nextInt(2) ;
			switch (rIdx) {
				case 0:
					// a-f
					temp.append((char) ((int) (rnd.nextInt(6)) + 97)) ;
				break ;
				case 1:
					// 0-9
					temp.append((rnd.nextInt(10))) ;
				break ;
				default:
				break ;
			}
			tempLength = temp.length();
		}

		return temp.toString() ;
	}

	/**
	 * 범위내 난수 생성
	 * @param min
	 * @param max
	 */
	public static int getRamdonInt(int min, int max) {
		return (int) ((Math.random() * max) + min) ;
	}

	/**
	 * String null check
	 */
	public static Boolean isEmpty(String text) {
		String strNull = "null";
		return ((text == null) || (text.trim().length() == 0) || (text.trim().equalsIgnoreCase(strNull))) ;
	}

	/**
	 * Object null check
	 */
	public static Boolean isEmpty(Object obj) {
		if (obj == null) {
			return true ;
		}
		if ((obj instanceof String) && (((String) obj).trim().length() == 0)) {
			return true ;
		}
		if (obj instanceof Map) {
			return ((Map<?, ?>) obj).isEmpty() ;
		}
		if (obj instanceof List) {
			return ((List<?>) obj).isEmpty() ;
		}
		if (obj instanceof Object[]) {
			return (((Object[]) obj).length == 0) ;
		}
		return false ;
	}

	/**
	 * 휴대폰 번호 마스킹(010****1234 / 011***1234)
	 */
	public static String maskPhoneNumber(String str) {
		String phoneNumber ;
		String strMinus = "-";

		if (isEmpty(str)) {
			phoneNumber = "" ;
		} else {
			// 공백제거
			phoneNumber = str.trim().replaceAll(" ", "") ;

			// '-'가 포함되어있으면 모두 삭제
			if (phoneNumber.contains(strMinus)) {
				phoneNumber = phoneNumber.replaceAll("[^0-9]", "") ;
			}

			// 11자리 또는 10자리가 되지 않으면 공백 ""
			if (phoneNumber.length() > 11 || phoneNumber.length() < 10) {
				phoneNumber = "" ;
			} else {
				// 11자리 휴대폰 번호 마스킹 처리
				if (phoneNumber.length() == 11) {
					String num1 = phoneNumber.substring(0, 3) ;
					String num3 = phoneNumber.substring(7) ;

					phoneNumber = num1 + "****" + num3 ;
					// 10자리 휴대폰 번호 마스킹 처리
				} else if (phoneNumber.length() == 10) {
					String num1 = phoneNumber.substring(0, 3) ;
					String num3 = phoneNumber.substring(6) ;
					phoneNumber = num1 + "***" + num3 ;
				}
			}
		}

		return phoneNumber ;
	}

	/**
	 *	 암호 유효성 검사
	 */
	public static Integer passwordValidation(String pass) {
		int cnt = 0;

        Pattern digit = Pattern.compile("[0-9]");
        Pattern letter = Pattern.compile("[a-zA-z]");
        Pattern special = Pattern.compile ("[!@#$%&*()_+=|<>?{}\\[\\]~-]");

		if (digit.matcher(pass).find()) cnt++;
		if (letter.matcher(pass).find()) cnt++;
		if (special.matcher(pass).find()) cnt++;

		return cnt;
	}

	/**
	 * null을 빈문자 ""로 변경
	 */
	public static String nullToEmpty(String str) {
	    return str == null ? "" : str;
	}

	/**
	 * null을 빈문자 텍스트로 변경
	 */
	public static String nullToText(String str, String text) {
	    return str == null ? text : str;
	}
}
