package pems.utils;

import com.mortennobel.imagescaling.AdvancedResizeOp;
import com.mortennobel.imagescaling.MultiStepRescaleOp;
import framework.exception.ServiceException;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import pems.Constants;
import pems.vo.FileInfo;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class PemsFileUtils {


	/**
	 * 로컬에 파일 생성
	 */
	public static FileInfo createTempFile(HttpServletRequest request, String path) throws Exception {
		MultipartHttpServletRequest multiReq = (MultipartHttpServletRequest) request;
		Iterator<String> iterator = multiReq.getFileNames();
		MultipartFile multipartFile = null;

		while (iterator.hasNext()) {
			multipartFile = multiReq.getFile(iterator.next());
		}

		return createFile(multipartFile, path);
	}

	public static List<FileInfo> createTempFiles(HttpServletRequest request, String path) throws Exception {
		List<FileInfo> list = new ArrayList<FileInfo>();
		MultipartHttpServletRequest multiReq = (MultipartHttpServletRequest) request;
		List<MultipartFile> fileList = multiReq.getFiles("file[]");

		for (MultipartFile mf : fileList) {
			list.add(createFile(mf, path));
		}

		return list;
	}

	private static FileInfo createFile(MultipartFile multipartFile, String path) throws Exception {
		FileInfo fi = null;

		if (! Utils.isEmpty(multipartFile)) {
			fi = new FileInfo(multipartFile);

			if (! checkExtension(Constants.IMAGE_FILE_WHITE_LIST, fi.getExtend())
					&& ! checkExtension(Constants.EXCEL_FILE_WHITE_LIST, fi.getExtend())
					&& ! checkExtension(Constants.UPDATE_FILE_WHITE_LIST, fi.getExtend()) 
					&& ! checkExtension(Constants.RESULT_FILE_WHITE_LIST, fi.getExtend())) {
				throw new ServiceException(- 1, "지원하는 파일 형식이 아닙니다_(" + fi.getOriginal() + ")");
			}

			mkdir(path);

			StringBuffer filePath = new StringBuffer(1024);
			filePath.append(path);
			filePath.append(File.separator);
			filePath.append(fi.getUploaded());

			fi.setPath(path);
			fi.setUploadedFilePath(String.valueOf(filePath));

			multipartFile.transferTo(new File(fi.getUploadedFilePath()));
		}

		return fi;
	}

	/**
	 * 로컬에 이미지 저장
	 */
	public static List<FileInfo> createTempImages(HttpServletRequest request, String path) throws Exception {
		List<FileInfo> list = new ArrayList<FileInfo>();
		MultipartHttpServletRequest multiReq = (MultipartHttpServletRequest) request;
		List<MultipartFile> fileList = multiReq.getFiles("file[]");

		for (MultipartFile mf : fileList) {
			list.add(createImage(mf, path, 480, 240));
		}

		if (fileList.size() == 0) {
			Iterator<String> iterator = multiReq.getFileNames();
			while (iterator.hasNext()) {
				MultipartFile multipartFile = multiReq.getFile(iterator.next());
				list.add(createImage(multipartFile, path, 230, 170));
			}
		}

		return list;
	}

	private static FileInfo createImage(MultipartFile multipartFile, String path, int w, int h) throws Exception {
		FileInfo fi = null;

		if (! Utils.isEmpty(multipartFile)) {
			fi = new FileInfo(multipartFile);
			fi.setPath(path);

			String saveFileName = fi.getUploaded();

			if (! checkExtension(Constants.IMAGE_FILE_WHITE_LIST, fi.getExtend())) {
				throw new ServiceException(- 1, "지원하는 형식이 아닙니다");
			}

//			if (fi.getSize() > 10000000) {
//				throw new ServiceException(-1, "첨부파일은 10MB을 초과할 수 없습니다.") ;
//			}

			mkdir(path);
			path += File.separator;

			multipartFile.transferTo(new File(path + saveFileName));

			resize(path + saveFileName, path + "thumb_" + saveFileName, "fix", 230, 170);
			if (fi.getSize() > 500000) {
				resize(path + saveFileName, path + saveFileName, "w", 500, 0);
			}

		}

		return fi;
	}

	/**
	 * 이미지 리사이징
	 *
	 * @param filePath 기존 이미지 경로
	 * @param savePath 리사이징 후 이미지 경로
	 * @param w        가로
	 * @param h        세로
	 */
	public static void resize(String filePath, String savePath, String resizeType, int w, int h) {
		BufferedImage img = null;
		BufferedImage resizedImage = null;

		try {
			File f = new File(filePath);
			img = ImageIO.read(f);

			if ("w".equals(resizeType)) {    // w 기준 비율로 리사이즈
				double ratio = (double) w / (double) img.getWidth(null);
				w = (int) (img.getWidth(null) * ratio);
				h = (int) (img.getHeight(null) * ratio);
			} else if ("h".equals(resizeType)) {    // h 기준 비율로 리사이즈
				double ratio = (double) h / (double) img.getHeight(null);
				w = (int) (img.getWidth(null) * ratio);
				h = (int) (img.getHeight(null) * ratio);
			} else if ("fix".equals(resizeType)) {    // w, h 고정크기 리사이즈
			}

			MultiStepRescaleOp rescale = new MultiStepRescaleOp(w, h);

			rescale.setUnsharpenMask(AdvancedResizeOp.UnsharpenMask.Soft);
			resizedImage = rescale.filter(img, null);

			File saveFile = new File(savePath);
			ImageIO.write(resizedImage, saveFile.getName().substring(saveFile.getName().lastIndexOf(".") + 1), saveFile);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (img != null)
				img.flush();
			if (resizedImage != null)
				resizedImage.flush();
		}

	}

	/**
	 * byte 변환
	 */
	public static byte[] toByteArray(File file) throws Exception {
		byte[] imageInByte = null;
		File fileTmp = null;

		if (file != null) {
			fileTmp = file;
			if (! fileTmp.exists()) {
				return null;
			}

			BufferedImage image = ImageIO.read(file);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(image, file.getName().substring(file.getName().lastIndexOf(".") + 1), baos);

			baos.flush();
			imageInByte = baos.toByteArray();
			baos.close();
		}

		return imageInByte;
	}

	/**
	 * byte 변환
	 */
	public static byte[] toByteArray(String filePath) throws Exception {
		byte[] imageInByte = null;
		String pathTmp = "";
		String regEx = "[~`!@#$%\\^&*()]"; // 특수문자

		if (! "".equals(filePath) && filePath != null) { // null 체크
			pathTmp = filePath.replaceAll(regEx, ""); // 특수문자 치환
			File file = new File(pathTmp);

			if (! file.exists()) {
				return null;
			}

			BufferedImage image = ImageIO.read(file);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(image, file.getName().substring(file.getName().lastIndexOf(".") + 1), baos);

			baos.flush();
			imageInByte = baos.toByteArray();
			baos.close();
		}

		return imageInByte;
	}

	/**
	 * 허용된 파일인지 체크함
	 */
	private static boolean checkExtension(String[] list, String ex) {
		return Arrays.asList(list).contains(ex.toLowerCase());
	}

	/**
	 * 폴더 생성
	 */
	public static void mkdir(String dirPath) {
		try {
			File f = new File(dirPath);
			if (! f.exists())
				f.mkdirs();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 폴더 삭제
	 */
	public static void rmdir(String dirPath) {
		try {
			File f = new File(dirPath);
			for (File t : f.listFiles()) {
				if (t.isFile()) {
					t.delete();
				} else {
					rmdir(t.getPath());
				}
			}

			f.delete();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
