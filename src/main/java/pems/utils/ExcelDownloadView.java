package pems.utils;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.web.servlet.view.document.AbstractXlsxView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.*;
import java.net.URLEncoder;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ExcelDownloadView extends AbstractXlsxView {

	@Override
	protected void buildExcelDocument(Map<String, Object> map, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
		// 파일명 설정
		String browser = request.getHeader("User-Agent") ;
		String excelFileName = getExcelFileName(String.valueOf(map.get("excelFileName")), browser) ;
		String contentType = getContentType(browser) ;

//		SXSSFWorkbook workbook = new SXSSFWorkbook(100) ;
//		workbook.setCompressTempFiles(true);
		XSSFSheet sheet = (XSSFSheet) workbook.createSheet(excelFileName);

		response.setHeader("Content-Disposition", "attachment; fileName=\"" + excelFileName + ".xlsx" + "\";") ;
		response.setHeader("Content-Transfer-Encoding", "binary") ;
		response.setContentType(contentType) ;

		/*

		// row(행) 생성
		Row row = sheet.createRow(rowCount++);

		// cell(셀) 생성
		Cell cell = row.createCell(cellCount++);

		// cell(셀) 값 입력
		cell.setCellValue('값');
//		cell.setCellStyle(cellStyle);
*/

		// 본문 폰트스타일 지정
		Font font = workbook.createFont(); // 폰트변수선언
		font.setFontName("맑은 고딕") ; // 폰트명
		font.setFontHeightInPoints((short) 10) ; // 폰트크기
		font.setColor(HSSFColor.HSSFColorPredefined.BLACK.getIndex()) ; // 폰트색상
		font.setBold(true);

		CellStyle cellStyle = workbook.createCellStyle();
		cellStyle.setAlignment(HorizontalAlignment.CENTER);
		cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		cellStyle.setBorderLeft(BorderStyle.THIN);
		cellStyle.setBorderRight(BorderStyle.THIN);
		cellStyle.setBorderTop(BorderStyle.THIN);
		cellStyle.setBorderBottom(BorderStyle.THIN);
		cellStyle.setWrapText(true);
		cellStyle.setFillForegroundColor(HSSFColor.HSSFColorPredefined.GREY_25_PERCENT.getIndex());
		cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		cellStyle.setFont(font) ;

		// 포맷 설정
		cellStyle.setDataFormat(workbook.createDataFormat().getFormat("yyyy-mm-dd")); // 날짜
		cellStyle.setDataFormat(workbook.createDataFormat().getFormat("0.00%")); // 퍼센트
		cellStyle.setDataFormat(workbook.createDataFormat().getFormat("#,##0")); // 금액
		cellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("#,##0")); // 금액
		cellStyle.setDataFormat(workbook.getCreationHelper().createDataFormat().getFormat("#,##0")); // 금액

		List<String> colName = (List<String>) map.get("colName");
		List<Map<String, Object>> colValueList = (List<Map<String, Object>>) map.get("colValue");
		int rowCount = 0;

		// 내용 생성
		for (int i = 0, j = colValueList.size(); i < j; i++) {
			int k = 0 ;
			Row row = sheet.createRow(rowCount++);
			Map<String, Object> colValue = colValueList.get(i);

			if (rowCount == 1) {
				for (String key : colValue.keySet()) {
					Cell cell = row.createCell(k++) ;
					cell.setCellValue(key) ;
					cell.setCellStyle(cellStyle) ;
				}
			} else {
				for (String key : colValue.keySet()) {
					Cell cell = row.createCell(k++) ;
					String value = String.valueOf(colValue.get(key)) ;

					if (Utils.isInt(value) > 0) {
						cell.setCellValue(new Double(value)) ;
					} else {
						cell.setCellValue(value) ;
					}
					cell.setCellStyle(cellStyle) ;
				}
			}
		}

		/** 컬럼 Width 확보 추가 */
		for (int i = 0; i < colName.size(); i++) {
			sheet.autoSizeColumn(i) ;
			sheet.setColumnWidth(i, (sheet.getColumnWidth(i)) + 1000) ;
		}
	}

	private String getExcelFileName(String fileName, String browser) throws Exception {
		// 브라우저에 따른 파일명 인코딩 설정
		if (browser.indexOf("MSIE") > -1) {
			fileName = URLEncoder.encode(fileName, "UTF-8").replaceAll("\\+", "%20");
		} else if (browser.indexOf("Trident") > -1) {       // IE11
			fileName = URLEncoder.encode(fileName, "UTF-8").replaceAll("\\+", "%20");
		} else if (browser.indexOf("Firefox") > -1) {
			fileName = "\"" + new String(fileName.getBytes("UTF-8"), "8859_1") + "\"";
		} else if (browser.indexOf("Opera") > -1) {
			fileName = "\"" + new String(fileName.getBytes("UTF-8"), "8859_1") + "\"";
		} else if (browser.indexOf("Chrome") > -1) {
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < fileName.length(); i++) {
				char c = fileName.charAt(i);
				if (c > '~') {
					sb.append(URLEncoder.encode("" + c, "UTF-8"));
				} else {
					sb.append(c);
				}
			}
			fileName = sb.toString();
		} else if (browser.indexOf("Safari") > -1){
			fileName = "\"" + new String(fileName.getBytes("UTF-8"), "8859_1")+ "\"";
		} else {
			fileName = "\"" + new String(fileName.getBytes("UTF-8"), "8859_1")+ "\"";
		}

		return fileName ;
	}

	private String getContentType(String browser){
		String contentType = "application/download;charset=utf-8";
		switch(browser) {
			case "Firefox":
			case "Opera":
				contentType = "application/octet-stream; text/html; charset=UTF-8;";
				break;
			default:
				contentType = "application/x-msdownload; text/html; charset=UTF-8;";
				break;
		}

		return contentType ;
	}

}
