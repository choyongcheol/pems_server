package pems.utils;

import framework.exception.ServiceException;
import pems.Constants;

import javax.crypto.Cipher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;

public class RSA {

	// 키 생성
	public static void getRsaKey(HttpServletRequest request) throws ServiceException {
		HttpSession session = request.getSession();

		try {
			KeyPairGenerator generator;

			generator = KeyPairGenerator.getInstance("RSA");
			generator.initialize(Constants.KEY_SIZE);

			KeyPair keyPair = generator.genKeyPair();
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");

			PublicKey publicKey = keyPair.getPublic();    // 공개키
			PrivateKey privateKey = keyPair.getPrivate(); // 암호할 키

			RSAPublicKeySpec publicSpec = (RSAPublicKeySpec) keyFactory.getKeySpec(publicKey, RSAPublicKeySpec.class);

			String publicKeyModulus = publicSpec.getModulus().toString(16);
			String publicKeyExponent = publicSpec.getPublicExponent().toString(16);

			session.setAttribute("publicKey", publicKey);
			session.setAttribute("privateKey", privateKey);
			session.setAttribute("publicKeyModulus", publicKeyModulus);
			session.setAttribute("publicKeyExponent", publicKeyExponent);
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			e.printStackTrace();
		}
	}

	// 복호화
	public static String decryptRsa(PrivateKey privateKey, String securedValue) throws Exception {
		securedValue = securedValue.substring(0, securedValue.length() - 14);
		Cipher cipher = Cipher.getInstance("RSA");
		byte[] encryptedBytes = hexToByteArray(securedValue);
		cipher.init(Cipher.DECRYPT_MODE, privateKey);
		byte[] decryptedBytes = cipher.doFinal(encryptedBytes);
		return new String(decryptedBytes, "utf-8"); // 문자 인코딩 주의.
	}

	public static byte[] hexToByteArray(String hex) {
		if (hex == null || hex.length() == 0) {
			return null;
		}

		byte[] ba = new byte[hex.length() / 2];
		for (int i = 0; i < ba.length; i++) {
			ba[i] = (byte) Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
		}

		return ba;
	}
}
