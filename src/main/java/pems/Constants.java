package pems ;

public class Constants {

//	public static final String ROOT_PATH = "C:/Users/Admin/Desktop/workspace/pems/src/main/webapp/" ;
	public static final String ROOT_PATH = "/home/wonikpne/tomcat/webapps/ROOT/" ;


	public static final String TEMP_FILE_PATH = ROOT_PATH + "WEB-INF/temp/" ;
	public static final String UPDATE_FILE_PATH = ROOT_PATH + "WEB-INF/update/" ;
	public static final String RESULT_PATH = ROOT_PATH + "WEB-INF/resultFile/" ;

	/* 이미지 확장자 */
	public static final String[] IMAGE_FILE_WHITE_LIST = { "jpg", "jpeg", "png", "gif"} ;

	/* 엑셀 확장자 */
	public static final String[] EXCEL_FILE_WHITE_LIST = { "xls", "xlsx", "csv" } ;

	/* 결과데이터 확장자 */
	public static final String[] RESULT_FILE_WHITE_LIST = { "json", "sch", "cts", "cyc" } ;

	/* 업데이트 파일 확장자 */
	public static final String[] UPDATE_FILE_WHITE_LIST = { "zip" } ;

	/** RSA 암호화 키사이즈 */
	public static final int KEY_SIZE = 2048 ;

	public static final String N_CLOUD = "https://sens.apigw.ntruss.com";
	public static final String SMS_KEY_ID = "ncp%3Asms%3Akr%3A271890775141%3Apems";
	public static final String KAKAOKEY_ID = "ncp%3Akkobizmsg%3Akr%3A2718907%3Apems_kakao";

	public static final String SMS_URL = "/sms/v2/services/" + SMS_KEY_ID + "/messages" ;
	public static final String ALIMTALK_URL = "/alimtalk/v2/services/" + KAKAOKEY_ID + "/messages" ;

	public static final String ACCESS_KEY = "GMQ7P4DZq1GgkZwaxRKK";
	public static final String SECRET_KEY = "ayLuTKnppMv2RiQ2X7m0GkLNhkzczoZhuWxZA80q";

	public static final String[] EXCEL_HEADER_NAME_CTS_RECORD = {"step", "Type", "State", "Time", "Voltage(V)", "Current(A)", "Capacity(Ah)", "Watt(W)", "WH(KWh)", "Avg(V)", "Avg(A)", "Imp"} ;

	public static final String USER_COLUMN_CHANNEL_ID = "channel_no,avg_voltage,avg_current,aux_count,can_count,step_time,total_time,step_no,total_cycle_num,relay_num,last_calibration_date,state" ;
	public static final String USER_COLUMN_CHANNEL_NAME = "고유번호,전압,전류,AUX 수,CAN 수,stepTime,totTime,stepNo,totCycle,Relay 횟수,교정날짜,상태" ;
	public static final String USER_COLUMN_CHANNEL_VISIBLE = "Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y" ;

	public static final String USER_COLUMN_CYC_ID = "lSaveSequence,fStepTime_Day,fStepTime,fVoltage,fCurrent,chCode,InputState,fOvenTemperature,fOvenHumidity,fChillerRefTemp,fChillerCurTemp,iChillerRefPump,iChillerCurPump,dCellBALChData,fVoltageInput,fVoltagePower,fVoltageBus,fCapacity,fChargeAh,fDisChargeAh,fWatt,fWattHour,fChargeWh,fDisChargeWh,lSyncDateTime,fCVTime,fAvgCurrent,fAvgVoltage,fLoadVoltage,fLoadCurrent,fPwrSetVoltage,fPwrCurVoltage,chCommState,chOutputState" ;
	public static final String USER_COLUMN_CYC_NAME = "No(Index),Day,Time(sec),V(V),I(A),chCode,chInputState,챔버온도,챔버습도,fChillerRefTemp,fChillerCurTemp,iChillerRefPump,iChillerCurPump,dCellBALChData,Input V(V),POWER V(V),Bus V(V),C(Ah),충전C(Ah),방전C(Ah),W(W),Wh(Wh),충전Wh(Wh),방전Wh(Wh),작업시간,fCVTime,fAvgCurrent,fAvgVoltage,fLoadVoltage,fLoadCurrent,fPwrSetVoltage,fPwrCurVoltage,chCommState,chOutputState" ;
	public static final String USER_COLUMN_CYC_VISIBLE = "Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y" ;


}
