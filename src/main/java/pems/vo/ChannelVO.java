package pems.vo;

import lombok.Data;

@Data
public class ChannelVO extends CommonVO {

	// tb_channel_info
	private String channelNo;
	private String moduleNo;
	private String chNo;
	private String avgVoltage;
	private String avgCurrent;
	private String auxCount;
	private String canCount;
	private String stepNo;
	private String stepDay;
	private String stepTime;
	private String totalDay;
	private String totalTime;
	private String totalCycleNum;
	private String relayNum;
	private String lastCalibrationDate;
	private String state;


}
