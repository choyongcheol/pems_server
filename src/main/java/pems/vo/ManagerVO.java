package pems.vo;

import lombok.Data;

import java.util.List;

@Data
public class ManagerVO extends UserVO {

	private String moduleNo;
	private String moduleManagerNo;
	private String agentName ;

	private List<UserVO> userList ;

}
