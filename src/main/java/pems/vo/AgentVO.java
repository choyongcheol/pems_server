package pems.vo;

import lombok.Data;

@Data
public class AgentVO extends CommonVO {

	private String fileSavedPath ;

	// tb_agent_info
	private String agentNo;
	private String agentName;
	private String pcIp;
	private String agentVersion;
	private String moduleCount;
	private String comment;
	private String firstReceiveDate;
	private String lastReceiveDate;
	private String regDate;

}
