package pems.vo;

import lombok.Data;

@Data
public class ChillerVO {

	// tb_chiller_info
	private String chillerNo;
	private String moduleNo;
	private String assignChannelNum;
	private String chillerRoomNumber;
	private String flowAlmCode;
	private String flowMode;
	private String flowState;
	private String pvFlow;
	private String pvTemp;
	private String svFlow;
	private String svTemp;
	private String tempAlmCode;
	private String tempMode;
	private String tempState;
	private String regDate;


}
