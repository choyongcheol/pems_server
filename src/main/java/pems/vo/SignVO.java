package pems.vo;

import java.util.Base64 ;

import org.mindrot.jbcrypt.BCrypt ;
import org.springframework.util.StringUtils ;

import lombok.Data ;

@Data
public class SignVO {

	private String signId ;
	private String signName ;
	private String signPassword ;
	private String signPhoneNo ;
	
	
	public String getEncryptedSignPassword() {
		return StringUtils.isEmpty(this.signPassword) ? this.signPassword : 
			BCrypt.hashpw(this.signPassword, BCrypt.gensalt()) ; 
	}

	public String getEncryptedSignPhoneNo() {
		return StringUtils.isEmpty(this.signPhoneNo) ? this.signPhoneNo : 
			new String(Base64.getEncoder().encode(this.signPhoneNo.getBytes())) ;
	}

	public String getDecryptedSignupPhoneNo() {

		return StringUtils.isEmpty(this.signPhoneNo) ? this.signPhoneNo :
			new String(Base64.getDecoder().decode(this.signPhoneNo.getBytes())) ;
	}
	
}