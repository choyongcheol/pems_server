package pems.vo;

import lombok.Data;

@Data
public class CommonVO {

	private String agentNo;

	private String userNo;
	private String userName;

	private String sDate ;
	private String eDate ;

	// tb_emg_code_history
	private String alarmDiv ;

	// tb_city_info
	private String cityNo ;
	private String countryName ;
	private String cityName ;

	// tb_site_info
	private String siteNo;
	private String address;
	private String siteName;

	// tb_factory_info
	private String site_no;
	private String factoryName;
	private String tel;
	private String mapSize;

	// tb_area_info
	private String areaNo;
	private String factoryNo;
	private String floor;
	private String areaName;
	private String status;
	private String pos;
	private String size;
	private String comment;
	private String regUserNo;
	private String regDate;

	// tb_process_info
	private String processNo ;
	private String processName ;

	// tb_process_channel_info
	private String channelName ;





}
