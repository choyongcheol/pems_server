package pems.vo;

import java.util.Base64;

import org.springframework.util.StringUtils;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode (callSuper = false)
public class UserVO extends SignVO {

	/**
	 * 회원 번호
	 */
	private String userNo;

	/**
	 * 회원 로그인 아이디
	 */
	private String userId;

	/**
	 * 회원 이름
	 */
	private String userName;

	/**
	 * 회원 연락처
	 */
	private String phoneNo;

	/**
	 * 부서
	 */
	private String dept ;

	/**
	 * 직급
	 */
	private String position ;

	private String siteName ;
	private String factoryName ;
	private String areaName ;
//	private String moduleNo ;
	private String moduleName ;
//	private String moduleManagerNo ;


	/**
	 * 이메일
	 */
	private String email ;

	/**
	 * 인증번호
	 */
	private String verificationCode;

	/**
	 * 인증번호 매칭 키
	 */
	private String validationCode;

	/**
	 * 회원 가입일
	 */
	private String regDate;

	public String getEncryptedPhoneNo() {
		return StringUtils.isEmpty(this.phoneNo) ? this.phoneNo : new String(Base64.getEncoder().encode(this.phoneNo.getBytes()));
	}

	public String getDecryptedPhoneNo() {
		return StringUtils.isEmpty(this.phoneNo) ? this.phoneNo : new String(Base64.getDecoder().decode(this.phoneNo.getBytes()));
	}

}
