package pems.vo;

import lombok.Data;
import org.springframework.util.StringUtils;

import java.util.Base64;

@Data
public class ColumnVO {

	private String columnsNo ;

	private String userNo ;

	private String tableName ;

	private String columnId ;

	private String columnName ;

	private String columnVisible ;

	private String regDate ;

}
