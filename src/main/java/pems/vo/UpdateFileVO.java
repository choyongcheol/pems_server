package pems.vo;

import lombok.Data;

@Data
public class UpdateFileVO extends FileInfo {

	private String fileType;
	private String updateFileNo;
	private String userNo;
	private String userName;
	private String comment;
	private String regDate;

}
