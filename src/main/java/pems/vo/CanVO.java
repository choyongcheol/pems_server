package pems.vo;

import lombok.Data;

@Data
public class CanVO {

	// tb_can_info
	private String canNo;
	private String channelNo;
	private String canId;
	private String canItem;
	private String divisionCode1;
	private String divisionCode2;
	private String divisionCode3;
	private String endMax;
	private String endMin;
	private String canName;
	private String safetyMax;
	private String safetyMin;
	private String canValue;
	private String regDate;

}
