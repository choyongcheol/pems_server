package pems.vo;

import lombok.Data;

@Data
public class ProcessVO extends CommonVO {

	// tb_process_info
	private String processNo;
	private String agentNo;
	private String processName;
	private String channelCount;
	private String agentName;
	private String regDate;

	// tb_process_channel_info
	private String processChannelNo ;
	private String channelName ;
	private String fileName ;

	//	tb_cts_header
	private String ctsHeaderNo ;

	// tb_cts_record
	private String nIndexFrom ;
	private String nIndexTo ;

}
