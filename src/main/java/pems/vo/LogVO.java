package pems.vo;

import lombok.Data;

@Data
public class LogVO {

	private String logNo ;

	private String referer ;

	private String browser ;

	private String userAgent ;

	private String ip ;

	private String displayLanguage ;

	private String os ;

	private String regDate ;
}
