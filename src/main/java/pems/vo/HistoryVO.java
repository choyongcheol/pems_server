package pems.vo;

import lombok.Data;

@Data
public class HistoryVO extends CommonVO {

	// tb_emg_code_history
	private String emgNo;
	private String moduleNo;
//	private String alarmDiv;
	private String alarmCode;
	private String emgCode;
	private String eqpCode;
	private String moduleId;
	private String emgDateTime;
	private String emgDescription;
	private String emgValue1;
	private String fileName;
	private String regDate;

}
