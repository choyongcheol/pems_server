package pems.vo;

import lombok.Data;

@Data
public class ModuleVO extends CommonVO {

	private String agentName ;

	// tb_module_info
	private String moduleNo;
	private String areaNo;
	private String agentNo;
	private String moduleCount;
	private String eqpType;
	private String eqpCode;
	private String blockNum;
	private String sbcIp;
	private String sbcSwDescription;
	private String sbcSwVersion;
	private String pcIp;
	private String pcSwDescription;
	private String pcSwVersion;
	private String connectState;
	private String connectTime;
	private String connectTimeDay;
	private String moduleId;
	private String systemType;
	private String protocolVersion;
	private String modelName;
	private String osversion;
	private String voltRange;
	private String voltRangeCount;
	private String currentRange;
	private String currentRangeCount;
	private String totalBoardCount;
	private String channelPerBoard;
	private String installChannelCount;
	private String upsInstallDate;
	private String installDate;
	private String comment;
	private String regDate;

}
