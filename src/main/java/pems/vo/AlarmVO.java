package pems.vo;

import lombok.Data;

@Data
public class AlarmVO {

	// tb_alarm_set
	private String alarmSetNo;
	private String alarmDiv;
	private String alarmCode;
	private String alarmMessage;
	private String alarmDesc;
	private String smsYn;
	private String smsContent;
	private String emailYn;
	private String emailTitle;
	private String emailContent;
	private String status;
	private String regDate;

}
