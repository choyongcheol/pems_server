package pems.vo;

import lombok.Data;

@Data
public class AuxVO {

	// tb_aux_info
	private String auxNo;
	private String channelNo;
	private String auxTempTableType;
	private String auxItem;
	private String divisionCode1;
	private String divisionCode2;
	private String divisionCode3;
	private String auxName;
	private String auxValue;
	private String endMax;
	private String endMin;
	private String fixMax;
	private String fixMin;
	private String useVentLink;
	private String safetyMax;
	private String safetyMin;
	private String regDate;

}
