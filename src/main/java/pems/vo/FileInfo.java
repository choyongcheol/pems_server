package pems.vo ;

import org.apache.commons.io.FilenameUtils;
import org.springframework.web.multipart.MultipartFile ;

import lombok.Data ;
import lombok.NoArgsConstructor ;

@Data
@NoArgsConstructor
public class FileInfo {

	private Integer fileNo ;
	private String path ;
	private String original ;
	private String uploaded ;
	private String uploadedFilePath ;
	private String extend ;
	private String contentType ;
	private long size ;

	/**
	 * constructor
	 */
	public FileInfo(MultipartFile mf) {
		String original = mf.getOriginalFilename() ;
		String extend = FilenameUtils.getExtension(original) ;
		String contentType = mf.getContentType() ;
		long size = mf.getSize() ;

		String uploaded = createSystemFileName().concat(".").concat(extend) ;

		// this.setPath(path);
		this.setUploaded(uploaded) ;
		this.setOriginal(original) ;
		this.setExtend(extend) ;
		this.setContentType(contentType) ;
		this.setSize(size) ;
	}

	/**
	 * 파일명 생성
	 */
	private static String createSystemFileName() {
		StringBuffer sb = new StringBuffer() ;

		sb.append((char)((Math.random() * 26) + 97)) ;
		sb.append((char)((Math.random() * 26) + 97)) ;
		sb.append((char)((Math.random() * 26) + 97)) ;
		sb.append((char)((Math.random() * 26) + 97)) ;
		sb.append((char)((Math.random() * 26) + 97)) ;
		
		return sb + String.valueOf(System.currentTimeMillis()) ;
	}
}
