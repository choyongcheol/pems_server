package pems.vo;

import lombok.Data;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Base64;

@Data
@Scope (value = "session")
@Component ("webSession")
public class WebSessionVO {

	/**
	 * 그룹
	 */
	private String userGroupId;

	/**
	 * 회원 고유번호
	 */
	private String userNo;

	/**
	 * 로그인 아이디
	 */
	private String userId;

	/**
	 * 회원 이름
	 */
	private String userName;

	/**
	 * 비밀번호
	 */
	private String password;

	/**
	 * 부서
	 */
	private String dept ;

	/**
	 * 직급
	 */
	private String position ;

	/**
	 * 이메일
	 */
	private String email ;

	/**
	 * 회원 전화번호
	 */
	private String phoneNo;

	public String getEncryptedPhoneNo() {
		return StringUtils.isEmpty(this.phoneNo) ? this.phoneNo : new String(Base64.getEncoder().encode(this.phoneNo.getBytes())) ;
	}

	public String getDecryptedPhoneNo() {
		return StringUtils.isEmpty(this.phoneNo) ? this.phoneNo : new String(Base64.getDecoder().decode(this.phoneNo.getBytes())) ;
	}

}
