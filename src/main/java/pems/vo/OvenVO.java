package pems.vo;

import lombok.Data;

@Data
public class OvenVO {

	// tb_oven_info
	private String ovenNo;
	private String ModuleNo;
	private String ovenRoomNumber;
	private String almCode;
	private String assignChannelNum;
	private String ovenMode;
	private String pvHumi;
	private String pvTemp;
	private String state;
	private String svHumi;
	private String svTemp;
	private String regDate;


}
