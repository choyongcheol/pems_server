package pems.vo;

import lombok.Data;

@Data
public class MainVO {

	private Integer reqCategoryNo;
	private String categoryNo;
	private String categoryName;
	private String mediumCategoryNo;

	private String largeCategoryNo;
	private String searchWord;
	private String guide;


}
