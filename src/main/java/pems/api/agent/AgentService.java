package pems.api.agent;

import com.fasterxml.jackson.databind.ObjectMapper;
import framework.exception.ServiceException;
import org.apache.commons.io.FileUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import pems.Constants;
import pems.vo.AgentVO;
import pems.vo.AlarmVO;
import pems.utils.BinUtil;
import pems.utils.LEDataInputStream;
import pems.utils.PemsFileUtils;
import pems.utils.SmsUtil;
import pems.vo.FileInfo;
import pems.web.module.ModuleMapper;
import pems.vo.ChannelVO;
import pems.vo.ModuleVO;
import pems.vo.UserVO;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileInputStream;
import java.util.*;

@Service
public class AgentService {

	@Resource
	private AgentMapper mapper;

	@Resource
	private ModuleMapper moduleMapper;


	public List<Map<String, Object>> getSiteList() {
		return mapper.getSiteList() ;
	}

	public List<Map<String, Object>> getFactoryList(Map<String, Object> param) {
		return mapper.getFactoryList(param) ;
	}

	public List<Map<String, Object>> getAreaList(Map<String, Object> param) {
		return mapper.getAreaList(param) ;
	}

	/**
	 *  에이전트 등록
	 */
	public void setInsertAgentInfo(HashMap<String, Object> param) throws Exception {
		mapper.setInsertAgentInfo(param) ;
	}

	public AgentVO getAgentInfo(AgentVO vo) throws Exception {
		return mapper.getAgentInfo(vo);
	}

	/**
	 * Agent에서 업로드하는 데이터파일 처리
	 */
	public void setAgentFileUpload(HttpServletRequest request, AgentVO vo, Map<String, Object> param) throws Exception {
		MultipartHttpServletRequest multiReq = (MultipartHttpServletRequest) request;
		MultipartFile mFile = multiReq.getFile("file");

		if (mFile != null) {
			JSONParser parser = new JSONParser();
			AgentVO agentInfo = getAgentInfo(vo);
			FileInfo fileInfo = PemsFileUtils.createTempFile(request
					, Constants.TEMP_FILE_PATH
							+ agentInfo.getFileSavedPath());
			String extend = fileInfo.getExtend();

			try {
				 if ("json".equals(extend)) {
					 // 데이터 수신일 갱신
					 mapper.setUpdateReceiveDate(param) ;

					ModuleVO mVO = new ModuleVO();
					JSONObject jsonObject = (JSONObject) parser.parse(FileUtils.readFileToString(new File(fileInfo.getUploadedFilePath()), "ms949"));
					String moduleId = String.valueOf(jsonObject.get("Module_Id"));

					mVO.setModuleId(moduleId);
					mVO.setAgentNo(agentInfo.getAgentNo());
					mVO = moduleMapper.getModuleInfo(mVO);

					if (fileInfo.getOriginal().contains("Module_info")) {
						setModuleFileData(agentInfo, jsonObject);
					} else if (fileInfo.getOriginal().contains("channel_info")) {
						setChannelFileData(mVO, jsonObject);
					} else if (fileInfo.getOriginal().contains("emg_code")) {
						setEmgFileData(mVO, fileInfo, jsonObject);
					}
				} else {
					// sch, cts, cyc
					setBinaryFileData(fileInfo, param);
				}
			} finally {
				File f = new File(fileInfo.getUploadedFilePath()) ;
				if (f.isFile()) f.delete() ;
			}
		}
	}

	/**
	 * 결과데이터 파일처리
	 */
	private void setBinaryFileData(FileInfo fileInfo, Map<String, Object> param) throws Exception {
		LEDataInputStream is = null ;
		String[] dirArr = String.valueOf(param.get("resultDir")).split("\\\\");
		param.put("processName", dirArr[dirArr.length - 3]);
		param.put("channelName", dirArr[dirArr.length - 2]);

		Map<String, Object> pInfo = getProcessInfo(param);
		Map<String, Object> pcInfo = getProcessChannelInfo(fileInfo, param);
		String processChannelNo = String.valueOf(param.get("process_channel_no")) ;

//pi = {reg_date=2022-05-08 17:02:36, process_name=result, process_no=1}
//pcInfo = {channel_name=M01Ch01[001], reg_date=2022-05-08 17:02:36, file_name=210324_CV eGT EV DV_충방전싸이클_#4(CH1)_급충부터 6cycle.sch, process_no=1, process_channel_no=1}
//param = {fileName=210324_CV eGT EV DV_충방전싸이클_#4(CH1)_급충부터 6cycle.sch, agent_seq=1, resultDir=C:\data\result\M01Ch01[001]\210324_CV eGT EV DV_충방전싸이클_#4(CH1)_급충부터 6cycle.sch, agentNo=1, processName=result, channelName=M01Ch01[001], processNo=1, process_channel_no=1}

		try {
			is = new LEDataInputStream(new FileInputStream(fileInfo.getUploadedFilePath())) ;

			if ("sch".equals(fileInfo.getExtend())) {
				setSchFileData(is, param) ;
			} else if ("cts".equals(fileInfo.getExtend())) {
				setCtsFileData(is, processChannelNo) ;
			} else if ("cyc".equals(fileInfo.getExtend())) {
				setCycFileData(is, processChannelNo) ;
			}
		} finally {
			is.close();
		}
	}

	/**
	 * cyc 파일처리
	 */
	private void setCycFileData(LEDataInputStream is, String processChannelNo) throws Exception {
		String charSet = "ms949";

		byte[] b64 = new byte[64];
		byte[] b128 = new byte[128];
		Map<String, Object> headerMap = new HashMap<>();
		List<Map<String, Object>> list = new ArrayList<>();

		// PS_FILE_ID_HEADER ~
		headerMap.put("process_channel_no", processChannelNo);
		headerMap.put("nFileID", is.readUnsignedInt());
		headerMap.put("nFileVersion", is.readUnsignedInt());
		is.read(b64, 0, b64.length);
		headerMap.put("szCreateDateTime", new String(b64, charSet).trim());
		is.read(b128, 0, b128.length);
		headerMap.put("szDescrition", new String(b128, charSet).trim());
		is.read(b128, 0, b128.length);
		headerMap.put("szReserved", new String(b128, charSet).trim());
		mapper.setInsertCycHeader(headerMap);
		Map<String, Object> cycHeaderInfo = mapper.getCycHeader(headerMap) ;
		// ~ PS_FILE_ID_HEADER

		// cyc_record 마지막 seq
		Integer latestSeq = mapper.getLatestCycSeq(cycHeaderInfo);
		latestSeq = (latestSeq == null ) ? 0 : latestSeq ;

		List<String> cycKeyList = new ArrayList<>();
		int nColumnCount = is.readInt();
		for (int i = 0; i < nColumnCount; i++) {
			cycKeyList.add(getAwColumnItem(is.readShort()));
		}

		int structSize = nColumnCount * 4;
		byte[] structByte = new byte[structSize];
		while (is.read(structByte, 0, structSize) != - 1) {
			int lSaveSequence = 0 ;
			Map<String, Object> map = new HashMap<>();

			try {
				map.put("cyc_header_no", cycHeaderInfo.get("cyc_header_no"));
				for (int i = 0; i < cycKeyList.size(); i++) {
					String key = cycKeyList.get(i);

					if ("lSaveSequence".equals(key)) {
						lSaveSequence = (int) BinUtil.byteToFloat(structByte, (i * 4)) - 1;
						map.put(key, lSaveSequence) ;
					} else if ("lSyncDate".equals(key)) {
						map.put(key, BinUtil.byteToInt(structByte, (i * 4))) ;
					} else {
						map.put(key, BinUtil.byteToFloat(structByte, (i * 4))) ;
					}
				}

				if (lSaveSequence > latestSeq) {
					list.add(map);
				}

				if (list.size() > 999) {
					mapper.setInsertCycRecord(list);
					list = new ArrayList<>() ;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (list.size() > 0) {
			mapper.setInsertCycRecord(list);
		}
	}

	/**
	 * cts 파일처리
	 */
	private void setCtsFileData(LEDataInputStream id, String processChannelNo) throws Exception {
		String charSet = "ms949";
		byte[] b32 = new byte[32];
		byte[] b64 = new byte[64];
		byte[] b128 = new byte[128];
		Map<String, Object> headerMap = new HashMap<>();

		// PS_FILE_ID_HEADER ~
		headerMap.put("process_channel_no", processChannelNo);
		headerMap.put("nFileID", id.readUnsignedInt()) ;
		headerMap.put("nFileVersion", id.readUnsignedInt()) ;
		id.read(b64, 0, b64.length);
		headerMap.put("szCreateDateTime", new String(b64, charSet).trim()) ;
		id.read(b128, 0, b128.length);
		headerMap.put("szDescrition", new String(b128, charSet).trim()) ;
		id.read(b128, 0, b128.length);
		headerMap.put("szReserved", new String(b128, charSet).trim()) ;
		// ~ PS_FILE_ID_HEADER

		// PS_TEST_FILE_HEADER ~
		id.read(b64, 0, b64.length);
		headerMap.put("szStartTime", new String(b64, charSet).trim()) ;
		id.read(b64, 0, b64.length);
		headerMap.put("szEndTime", new String(b64, charSet).trim()) ;
		id.read(b64, 0, b64.length);
		headerMap.put("szSerial", new String(b64, charSet).trim()) ;
		id.read(b32, 0, b32.length);
		headerMap.put("szUserID", new String(b32, charSet).trim()) ;
		id.read(b128, 0, b128.length);
		headerMap.put("szDescript", new String(b128, charSet).trim()) ;
		id.read(b128, 0, b128.length);
		headerMap.put("szTray_CellBcr", new String(b128, charSet).trim()) ;
		int nRecordSize = id.readInt() ;
		headerMap.put("nRecordSize", nRecordSize) ;
		short[] wRecordItem = new short[40];
		for(int i = 0 ; i < wRecordItem.length ; i++) {
			id.readShort();
		}
		headerMap.put("wRecordItem", "미사용");
		mapper.setInsertCtsHeader(headerMap);
		Map<String, Object> ctsHeaderInfo = mapper.getCtsHeader(headerMap) ;
		// ~ PS_TEST_FILE_HEADER

		// cts_record 마지막 seq
		Integer latestSeq = mapper.getLatestCtsNo(ctsHeaderInfo);
		latestSeq = (latestSeq == null ) ? 0 : latestSeq ;

		int structSize = 216;
		byte[] structByte = new byte[structSize];
		while (id.read(structByte, 0, structSize) != - 1) {
			Map<String, Object> map = new HashMap<>();

			try {
				map.put("cts_header_no", ctsHeaderInfo.get("cts_header_no"));
				map.put("chNo", BinUtil.readByte(structByte, 0)) ;
				map.put("chStepNo", BinUtil.readByte(structByte, 1)) ;
				map.put("chState", BinUtil.readByte(structByte, 2)) ;
				map.put("chStepType", BinUtil.readByte(structByte, 3)) ;
				map.put("chDataSelect", BinUtil.readByte(structByte, 4)) ;
				map.put("chCode", BinUtil.readByte(structByte, 5)) ;
				map.put("chGradeCode", BinUtil.readByte(structByte, 6)) ;
				map.put("chCommState", BinUtil.readByte(structByte, 7)) ;
				map.put("chOutputState", BinUtil.readByte(structByte, 8)) ;
				map.put("chInputState", BinUtil.readByte(structByte, 9)) ;
				map.put("cReserved1", BinUtil.readByte(structByte, 10)) ;
				map.put("cReserved2", BinUtil.readByte(structByte, 11)) ;
				map.put("nIndexFrom", BinUtil.byteToInt(structByte, 12)) ;
				map.put("nIndexTo", BinUtil.byteToInt(structByte, 16)) ;
				map.put("nCurrentCycleNum", BinUtil.byteToInt(structByte, 20)) ;
				map.put("nTotalCycleNum", BinUtil.byteToInt(structByte, 24)) ;

				int lSaveSequence = BinUtil.byteToInt(structByte, 28) ;
				map.put("lSaveSequence", lSaveSequence) ;

				map.put("nAccCycleGroupNum1", BinUtil.byteToInt(structByte, 32)) ;
				map.put("nAccCycleGroupNum2", BinUtil.byteToInt(structByte, 36)) ;
				map.put("nAccCycleGroupNum3", BinUtil.byteToInt(structByte, 40)) ;
				map.put("nAccCycleGroupNum4", BinUtil.byteToInt(structByte, 44)) ;
				map.put("nAccCycleGroupNum5", BinUtil.byteToInt(structByte, 48)) ;
				map.put("nMultiCycleGroupNum1", BinUtil.byteToInt(structByte, 52)) ;
				map.put("nMultiCycleGroupNum2", BinUtil.byteToInt(structByte, 56)) ;
				map.put("nMultiCycleGroupNum3", BinUtil.byteToInt(structByte, 60)) ;
				map.put("nMultiCycleGroupNum4", BinUtil.byteToInt(structByte, 64)) ;
				map.put("nMultiCycleGroupNum5", BinUtil.byteToInt(structByte, 68)) ;
				map.put("lSyncDate", BinUtil.byteToInt(structByte, 72)) ;

				map.put("fVoltage", BinUtil.byteToFloat(structByte, 76)) ;
				map.put("fCurrent", BinUtil.byteToFloat(structByte, 80)) ;
				map.put("fCapacity", BinUtil.byteToFloat(structByte, 84)) ;
				map.put("fWatt", BinUtil.byteToFloat(structByte, 88)) ;
				map.put("fWattHour", BinUtil.byteToFloat(structByte, 92)) ;
				map.put("fStepTime", BinUtil.byteToFloat(structByte, 96)) ;
				map.put("fTotalTime", BinUtil.byteToFloat(structByte, 100)) ;
				map.put("fImpedance", BinUtil.byteToFloat(structByte, 104)) ;
				map.put("fOvenTemperature", BinUtil.byteToFloat(structByte, 108)) ;
				map.put("fOvenHumidity", BinUtil.byteToFloat(structByte, 112)) ;
				map.put("fAvgVoltage", BinUtil.byteToFloat(structByte, 116)) ;
				map.put("fAvgCurrent", BinUtil.byteToFloat(structByte, 120)) ;
				map.put("fChargeAh", BinUtil.byteToFloat(structByte, 124)) ;
				map.put("fDisChargeAh", BinUtil.byteToFloat(structByte, 128)) ;
				map.put("fCapacitance", BinUtil.byteToFloat(structByte, 132)) ;
				map.put("fChargeWh", BinUtil.byteToFloat(structByte, 136)) ;
				map.put("fDisChargeWh", BinUtil.byteToFloat(structByte, 140)) ;
				map.put("fCVTime", BinUtil.byteToFloat(structByte, 144)) ;
				map.put("fSyncTime", BinUtil.byteToFloat(structByte, 148)) ;
				map.put("fVoltageInput", BinUtil.byteToFloat(structByte, 152)) ;
				map.put("fVoltagePower", BinUtil.byteToFloat(structByte, 156)) ;
				map.put("fVoltageBus", BinUtil.byteToFloat(structByte, 160)) ;
				map.put("fStepTime_Day", BinUtil.byteToFloat(structByte, 164)) ;
				map.put("fTotalTime_Day", BinUtil.byteToFloat(structByte, 168)) ;
				map.put("fCVTime_Day", BinUtil.byteToFloat(structByte, 172)) ;
				map.put("fLoadVoltage", BinUtil.byteToFloat(structByte, 176)) ;
				map.put("fLoadCurrent", BinUtil.byteToFloat(structByte, 180)) ;
				map.put("fChillerRefTemp", BinUtil.byteToFloat(structByte, 184)) ;
				map.put("fChillerCurTemp", BinUtil.byteToFloat(structByte, 188)) ;

				map.put("iChillerRefPump", BinUtil.byteToInt(structByte, 192)) ;
				map.put("iChillerCurPump", BinUtil.byteToInt(structByte, 196)) ;

				map.put("dCellBALChData", BinUtil.byteToDouble(structByte, 200)) ;
				map.put("fPwrSetVoltage", BinUtil.byteToFloat(structByte, 208)) ;
				map.put("fPwrCurVoltage", BinUtil.byteToFloat(structByte, 212)) ;

				if (lSaveSequence > latestSeq) {
					mapper.setInsertCtsRecord(map);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * sch 파일처리
	 */
	private void setSchFileData(LEDataInputStream id, Map<String, Object> param) throws Exception {
		String charSet = "ms949";
		byte[] b3 = new byte[3];
		byte[] b10 = new byte[10];
		byte[] b64 = new byte[64];
		byte[] b128 = new byte[128];
		byte[] b256 = new byte[256];
		Map<String, Object> headerMap = new HashMap<>();
		Map<String, Object> modelMap = new HashMap<>();
		Map<String, Object> scheduleMap = new HashMap<>();
		Map<String, Object> cellCheckMap = new HashMap<>();
		Map<String, Object> recordMap = new HashMap<>();
		List<Map<String, Object>> list = new ArrayList<>();

		// File Header ~
		headerMap.put("process_channel_no", param.get("process_channel_no"));
		headerMap.put("nFileID", getReadUnInt(id));
		headerMap.put("nFileVersion", getReadUnInt(id));
		headerMap.put("szCreateDateTime", getBytes(id, b64, charSet));
		headerMap.put("szDescription", getBytes(id, b128, charSet));
		headerMap.put("szReserved", getBytes(id, b128, charSet));
		mapper.setInsertSchHeader(headerMap);
		Map<String, Object> headerInfo = mapper.getSchHeader(headerMap) ;
		String schHeaderNo = String.valueOf(headerInfo.get("sch_header_no")) ;
		// File Header ~

		// Model Info ~
		modelMap.put("sch_header_no", schHeaderNo);
		modelMap.put("lID", getReadInt(id));
		modelMap.put("lType", getReadInt(id));
		modelMap.put("szName", getBytes(id, b64, charSet));
		modelMap.put("szDescription", getBytes(id, b128, charSet));
		modelMap.put("szCreator", getBytes(id, b64, charSet));
		modelMap.put("szModifiedTime", getBytes(id, b64, charSet));
		mapper.setInsertSchModel(modelMap);
		// Model Info ~

		// Test Info ~
		scheduleMap.put("sch_header_no", schHeaderNo);
		scheduleMap.put("lID", getReadInt(id));
		scheduleMap.put("lType", getReadInt(id));
		scheduleMap.put("szName", getBytes(id, b64, charSet));
		scheduleMap.put("szDescription", getBytes(id, b128, charSet));
		scheduleMap.put("szCreator", getBytes(id, b64, charSet));
		scheduleMap.put("szModifiedTime", getBytes(id, b64, charSet));
		mapper.setInsertSchProcess(scheduleMap);
		// Test Info ~

		// Cell Check Info ~
		cellCheckMap.put("sch_header_no", schHeaderNo);
		cellCheckMap.put("fMaxV", getReadFloat(id));
		cellCheckMap.put("fMinV", getReadFloat(id));
		cellCheckMap.put("fMaxI", getReadFloat(id));
		cellCheckMap.put("fCellDeltaV", getReadFloat(id));
		cellCheckMap.put("fSTDCellDeltaMaxV", getReadFloat(id));
		cellCheckMap.put("fSTDCellDeltaMinV", getReadFloat(id));
		cellCheckMap.put("fCellDeltaMaxV", getReadFloat(id));
		cellCheckMap.put("fCellDeltaMinV", getReadFloat(id));
		cellCheckMap.put("bfaultcompAuxV_Vent_Flag", getReadUnByte(id));
		cellCheckMap.put("breserved1", getBytes(id, b3, charSet));
		cellCheckMap.put("fMaxT", getReadFloat(id));
		cellCheckMap.put("fMinT", getReadFloat(id));
		cellCheckMap.put("fMaxC", getReadFloat(id));
		cellCheckMap.put("lMaxW", getReadInt(id));
		cellCheckMap.put("lMaxWh", getReadInt(id));

		short[] ican_function_division = new short[10];
		for (int i = 0; i < ican_function_division.length; i++) {
			ican_function_division[i] = id.readShort() ;
		}
		cellCheckMap.put("ican_function_division", Arrays.toString(ican_function_division)) ;
		cellCheckMap.put("cCan_compare_type", getBytes(id, b10, charSet));
		cellCheckMap.put("cCan_data_type", getBytes(id, b10, charSet));

		float[] fcan_Value = new float[10];
		for (int i = 0; i < fcan_Value.length; i++) {
			fcan_Value[i] = id.readFloat() ;
		}
		cellCheckMap.put("fcan_Value", Arrays.toString(fcan_Value)) ;

		short[] iaux_function_division = new short[10];
		for (int i = 0; i < iaux_function_division.length; i++) {
			iaux_function_division[i] = id.readShort() ;
		}
		cellCheckMap.put("iaux_function_division", Arrays.toString(iaux_function_division)) ;
		cellCheckMap.put("cAux_compare_type", getBytes(id, b10, charSet));
		cellCheckMap.put("cAux_data_type", getBytes(id, b10, charSet));

		float[] faux_Value = new float[10];
		for (int i = 0; i < faux_Value.length; i++) {
			faux_Value[i] = id.readFloat() ;
		}
		cellCheckMap.put("faux_Value", Arrays.toString(faux_Value)) ;
		mapper.setInsertSchCellCheck(cellCheckMap);
		// Cell Check Info ~

		// PS_RECORD_FILE_HEADER ~
		int nColumnCount = id.readInt() ;
		recordMap.put("nColumnCount", nColumnCount) ;

		short[] awColumnItem = new short[36];
		for(int i = 0 ; i < awColumnItem.length ; i++) {
			id.readShort();
		}
		recordMap.put("awColumnItem", "미사용");
		// ~ PS_RECORD_FILE_HEADER

		for (int z = 0; z < nColumnCount; z++) {
			Map<String, Object> map = new HashMap<>();

			map.put("chStepNo", getReadByte(id)) ;
			map.put("nProcType", getReadInt(id)) ;
			map.put("chType", getReadByte(id)) ;
			map.put("chMode", getReadByte(id)) ;
			map.put("fVref_Charge", getReadFloat(id)) ;
			map.put("fVref_DisCharge", getReadFloat(id)) ;
			map.put("fIref", getReadFloat(id)) ;
			map.put("fPref", getReadFloat(id)) ;
			map.put("fRref", getReadFloat(id)) ;
			map.put("fEndTime", getReadFloat(id)) ;
			map.put("fEndV", getReadFloat(id)) ;
			map.put("fEndI", getReadFloat(id)) ;
			map.put("fEndC", getReadFloat(id)) ;
			map.put("fEndDV", getReadFloat(id)) ;
			map.put("fEndDI", getReadFloat(id)) ;
			map.put("fCVTime", getReadFloat(id)) ;
			map.put("nLoopInfoCycle", getReadUnInt(id)) ;
			map.put("nLoopInfoGotoStep", getReadUnInt(id)) ;
			map.put("nAccLoopInfoCycle", getReadUnInt(id)) ;
			map.put("nAccLoopGroupID", getReadUnInt(id)) ;
			map.put("fCapacitanceHigh", getReadFloat(id)) ;
			map.put("fCapacitanceLow", getReadFloat(id)) ;
			map.put("fVLimitHigh", getReadFloat(id)) ;
			map.put("fVLimitLow", getReadFloat(id)) ;
			map.put("fILimitHigh", getReadFloat(id)) ;
			map.put("fILimitLow", getReadFloat(id)) ;
			map.put("fCLimitHigh", getReadFloat(id)) ;
			map.put("fCLimitLow", getReadFloat(id)) ;
			map.put("fImpLimitHigh", getReadFloat(id)) ;
			map.put("fImpLimitLow", getReadFloat(id)) ;
			map.put("fDeltaTime", getReadFloat(id)) ;
			map.put("fDeltaTime1", getReadFloat(id)) ;
			map.put("fDeltaV", getReadFloat(id)) ;
			map.put("fDeltaI", getReadFloat(id)) ;
			map.put("bGrade", getReadByte(id)) ;

			Map<String, Object> fileGrade = new HashMap<>();
			fileGrade.put("lGradeItem", getReadInt(id)) ;
			fileGrade.put("chTotalGrade", getReadByte(id)) ;

			float[] fValue1 = new float[10];
			for (int i = 0; i < fValue1.length; i++) {
				fValue1[i] = id.readFloat() ;
			}
			fileGrade.put("fValue1", Arrays.toString(fValue1)) ;

			float[] fValue2 = new float[10];
			for (int i = 0; i < fValue2.length; i++) {
				fValue2[i] = id.readFloat() ;
			}
			fileGrade.put("fValue2", Arrays.toString(fValue2)) ;
			fileGrade.put("aszGradeCode", getBytes(id, b10, charSet)) ;
			map.put("FILE_GRADE", fileGrade) ;

			float[] fCompVLow = new float[3];
			for (int i = 0; i < fCompVLow.length; i++) {
				fCompVLow[i] = id.readFloat() ;
			}
			map.put("fCompVLow", Arrays.toString(fCompVLow)) ;

			float[] fCompVHigh = new float[3];
			for (int i = 0; i < fCompVHigh.length; i++) {
				fCompVHigh[i] = id.readFloat() ;
			}
			map.put("fCompVHigh", Arrays.toString(fCompVHigh)) ;

			float[] fCompTimeV = new float[3];
			for (int i = 0; i < fCompTimeV.length; i++) {
				fCompTimeV[i] = id.readFloat() ;
			}
			map.put("fCompTimeV", Arrays.toString(fCompTimeV)) ;

			float[] fCompILow = new float[3];
			for (int i = 0; i < fCompILow.length; i++) {
				fCompILow[i] = id.readFloat() ;
			}
			map.put("fCompILow", Arrays.toString(fCompILow)) ;

			float[] fCompIHigh = new float[3];
			for (int i = 0; i < fCompIHigh.length; i++) {
				fCompIHigh[i] = id.readFloat() ;
			}
			map.put("fCompIHigh", Arrays.toString(fCompIHigh)) ;

			float[] fCompTimeI = new float[3];
			for (int i = 0; i < fCompTimeI.length; i++) {
				fCompTimeI[i] = id.readFloat() ;
			}
			map.put("fCompTimeI", Arrays.toString(fCompTimeI)) ;
			map.put("fIEndHigh", getReadFloat(id)) ;
			map.put("fIEndLow", getReadFloat(id)) ;
			map.put("fVEndHigh", getReadFloat(id)) ;
			map.put("fVEndLow", getReadFloat(id)) ;
			map.put("fReportV", getReadFloat(id)) ;
			map.put("fReportI", getReadFloat(id)) ;
			map.put("fReportTime", getReadFloat(id)) ;
			map.put("fCapaVoltage1", getReadFloat(id)) ;
			map.put("fCapaVoltage2", getReadFloat(id)) ;
			map.put("fDCRStartTime", getReadFloat(id)) ;
			map.put("fDCREndTime", getReadFloat(id)) ;
			map.put("fLCStartTime", getReadFloat(id)) ;
			map.put("fLCEndTime", getReadFloat(id)) ;
			map.put("lRange", getReadInt(id)) ;
			map.put("fReportTemp", getReadFloat(id)) ;
			map.put("fValueRate", getReadFloat(id)) ;
			map.put("fEndW", getReadFloat(id)) ;
			map.put("fEndWh", getReadFloat(id)) ;
			map.put("fHighLimitTemp", getReadFloat(id)) ;
			map.put("fLowLimitTemp", getReadFloat(id)) ;
			map.put("fTref", getReadFloat(id)) ;
			map.put("fHref", getReadFloat(id)) ;
			map.put("fTrate", getReadFloat(id)) ;
			map.put("fHrate", getReadFloat(id)) ;
			map.put("fStartT", getReadFloat(id)) ;
			map.put("fEndT", getReadFloat(id)) ;

			float[] fReserved = new float[20];
			for (int i = 0; i < fReserved.length; i++) {
				fReserved[i] = id.readFloat() ;
			}
			map.put("fReserved", Arrays.toString(fReserved)) ;
			map.put("bValueItem", getReadByte(id)) ;
			map.put("bValueStepNo", getReadByte(id)) ;
			map.put("wReserved", getReadShort(id)) ;
			map.put("fEndV_L", getReadFloat(id)) ;
			map.put("nAccLoopInfoGotoStep", getReadUnInt(id)) ;
			map.put("nMultiLoopGroupID", getReadUnInt(id)) ;
			map.put("nMultiLoopInfoCycle", getReadUnInt(id)) ;
			map.put("nMultiLoopInfoGotoStep", getReadUnInt(id)) ;
			map.put("nGotoStepEndV_H", getReadUnInt(id)) ;
			map.put("nGotoStepEndV_L", getReadUnInt(id)) ;
			map.put("nGotoStepEndTime", getReadUnInt(id)) ;
			map.put("nGotoStepEndCVTime", getReadUnInt(id)) ;
			map.put("nGotoStepEndC", getReadUnInt(id)) ;
			map.put("nGotoStepEndWh", getReadUnInt(id)) ;
			map.put("nGotoStepEndValue", getReadUnInt(id)) ;
			map.put("bUseCyclePause", getReadByte(id)) ;
			map.put("bUseLinkStep", getReadByte(id)) ;
			map.put("bUseChamberProg", getReadByte(id)) ;
			map.put("cReserved", getReadByte(id)) ;

			short[] iBranchCanDivision = new short[10];
			for (int i = 0; i < iBranchCanDivision.length; i++) {
				iBranchCanDivision[i] = id.readShort() ;
			}
			map.put("iBranchCanDivision", Arrays.toString(iBranchCanDivision)) ;

			float[] fBranchCanValue = new float[10];
			for (int i = 0; i < fBranchCanValue.length; i++) {
				fBranchCanValue[i] = id.readFloat() ;
			}
			map.put("fBranchCanValue", Arrays.toString(fBranchCanValue)) ;
			map.put("cBranchCanDataType", getBytes(id, b10, charSet));
			map.put("cBranchCanCompareType", getBytes(id, b10, charSet));

			short[] wBranchCanStep = new short[10];
			for (int i = 0; i < wBranchCanStep.length; i++) {
				wBranchCanStep[i] = id.readShort() ;
			}
			map.put("wBranchCanStep", Arrays.toString(wBranchCanStep)) ;

			short[] iBranchAuxDivision = new short[10];
			for (int i = 0; i < iBranchAuxDivision.length; i++) {
				iBranchAuxDivision[i] = id.readShort() ;
			}
			map.put("iBranchAuxDivision", Arrays.toString(iBranchAuxDivision)) ;

			float[] fBranchAuxValue = new float[10];
			for (int i = 0; i < fBranchAuxValue.length; i++) {
				fBranchAuxValue[i] = id.readFloat() ;
			}
			map.put("fBranchAuxValue", Arrays.toString(fBranchAuxValue)) ;
			map.put("cBranchAuxDataType", getBytes(id, b10, charSet));
			map.put("cBranchAuxCompareType", getBytes(id, b10, charSet));

			short[] wBranchAuxStep = new short[10];
			for (int i = 0; i < wBranchAuxStep.length; i++) {
				wBranchAuxStep[i] = id.readShort() ;
			}
			map.put("wBranchAuxStep", Arrays.toString(wBranchAuxStep)) ;

			long[] uaux_conti_time = new long[10];
			for (int i = 0; i < uaux_conti_time.length; i++) {
				uaux_conti_time[i] = id.readUnsignedInt() ;
			}
			map.put("uaux_conti_time", Arrays.toString(uaux_conti_time)) ;

			int[] lReserved = new int[20];
			for (int i = 0; i < lReserved.length; i++) {
				lReserved[i] = id.readInt() ;
			}
			map.put("lReserved", Arrays.toString(lReserved)) ;
			map.put("lEndTimeDay", getReadInt(id));
			map.put("lCVTimeDay", getReadInt(id));
			map.put("nValueLoaderItem", getReadInt(id));
			map.put("nValueLoaderMode", getReadInt(id));
			map.put("fValueLoaderSet", getReadInt(id));
			map.put("nWaitTimeInit", getReadInt(id));
			map.put("nWaitTimeDay", getReadInt(id));
			map.put("nWaitTimeHour", getReadInt(id));
			map.put("nWaitTimeMin", getReadInt(id));
			map.put("nWaitTimeSec", getReadInt(id));
			map.put("fValueMax", getReadFloat(id));
			map.put("fValueMin", getReadFloat(id));
			map.put("nNoCheckMode", getReadInt(id));
			map.put("nCanTxOffMode", getReadInt(id));
			map.put("nCanTxOffMode", getReadInt(id));
			map.put("nCanCheckMode", getReadInt(id));
			map.put("bStepChamberStop", getReadInt(id));
			map.put("szSimulationFile", getBytes(id, b256, charSet));
			map.put("fCellDeltaVStep", getReadFloat(id));
			map.put("fStepDeltaAuxTemp", getReadFloat(id));
			map.put("fStepDeltaAuxTh", getReadFloat(id));
			map.put("fStepDeltaAuxT", getReadFloat(id));
			map.put("fStepDeltaAuxVTime", getReadFloat(id));
			map.put("fStepAuxV", getReadFloat(id));
			map.put("bStepDeltaVentAuxV", getReadByte(id));
			map.put("bStepDeltaVentAuxTemp", getReadByte(id));
			map.put("bStepDeltaVentAuxTH", getReadByte(id));
			map.put("bStepDeltaVentAuxT", getReadByte(id));
			map.put("bStepVentAuxV", getReadByte(id));

			list.add(map) ;
		}
		// TODO: sch_record(list) insert
	}

	/**
	 * 프로세스 정보 조회
	 */
	public Map<String, Object> getProcessInfo(Map<String, Object> param){
		Map<String, Object> pInfo = mapper.getProcessInfo(param);

		if(StringUtils.isEmpty(pInfo)) {
			mapper.setInsertProcessInfo(param);
			pInfo = mapper.getProcessInfo(param);
		}
		param.put("processNo", pInfo.get("process_no"));

		return pInfo ;
	}

	/**
	 * 프로세스 채널  정보 조회
	 */
	public Map<String, Object> getProcessChannelInfo(FileInfo fileInfo, Map<String, Object> param) {
		Map<String, Object> pcInfo = mapper.getProcessChannelInfo(param);

		param.put("fileName", fileInfo.getOriginal().replace(("." + fileInfo.getExtend()), "")) ;
		if(StringUtils.isEmpty(pcInfo)) {
			mapper.setInsertProcessChannelInfo(param);
			pcInfo = mapper.getProcessChannelInfo(param);
		} else {
			param.put("process_channel_no", pcInfo.get("process_channel_no"));
			mapper.setUpdateProcessChannelInfo(param);
		}

		// TODO: 가동률 관련 처리
		return pcInfo ;
	}

	/**
	 * Module_info.json
	 */
	private void setModuleFileData(AgentVO agentInfo, JSONObject jsonObject) throws Exception {
		agentInfo.setModuleCount(String.valueOf(jsonObject.get("Module_Count")));
		mapper.setUpdateAgentInfo(agentInfo);

		JSONArray moduleList = (JSONArray) jsonObject.get("Module");
		if (! StringUtils.isEmpty(moduleList)) {
			ObjectMapper objectMapper = new ObjectMapper();

			for (Object module : moduleList) {
				Map<String, Object> moduleMap = objectMapper.convertValue(module, Map.class);

				moduleMap.put("agentNo", agentInfo.getAgentNo());
				moduleMap.put("voltRange", String.valueOf(moduleMap.get("Volt_Range")));
				moduleMap.put("currentRange", String.valueOf(moduleMap.get("Current_Range")));

				mapper.setInsertModuleInfo(moduleMap);
			}
		}
	}

	/**
	 * Module_x_channel_info.json
	 */
	private void setChannelFileData(ModuleVO mVO, JSONObject jsonObject) throws Exception {
		String moduleNo = mVO.getModuleNo();
		ObjectMapper objectMapper = new ObjectMapper();

		if (StringUtils.isEmpty(moduleNo)) {
			throw new ServiceException(- 1, "모듈정보 입력오류");
		}

		JSONArray channelList = (JSONArray) jsonObject.get("Channel");
		if (! StringUtils.isEmpty(channelList)) {
			for (Object channel : channelList) {
				Map<String, Object> channelMap = objectMapper.convertValue(channel, Map.class);

				channelMap.put("moduleNo", moduleNo);
				channelMap.put("eqpCode", jsonObject.get("Eqp_Code"));
				mapper.setInsertChannelInfo(channelMap);

				String channelNo = null;
				if (StringUtils.isEmpty(channelMap.get("channelNo"))) {
					ChannelVO cVO = new ChannelVO();
					cVO.setModuleNo(moduleNo);
					cVO.setChNo(String.valueOf(channelMap.get("Ch_No")));
					cVO = mapper.getChannelInfo(cVO);
					channelNo = cVO.getChannelNo();
				} else {
					channelNo = String.valueOf(channelMap.get("channelNo"));
				}

				if (StringUtils.isEmpty(channelNo)) {
					throw new ServiceException(- 1, "채널정보 입력오류");
				}

				JSONArray auxList = (JSONArray) channelMap.get("Aux");
				if (! StringUtils.isEmpty(auxList)) {
					for (Object aux : auxList) {
						Map<String, Object> auxMap = objectMapper.convertValue(aux, Map.class);

						auxMap.put("channelNo", channelNo);
						mapper.setInsertAuxInfo(auxMap);
					}
				}

				JSONArray canList = (JSONArray) channelMap.get("Can");
				if (! StringUtils.isEmpty(canList)) {
					for (Object can : canList) {
						Map<String, Object> canMap = objectMapper.convertValue(can, Map.class);

						canMap.put("channelNo", channelNo);
						mapper.setInsertCanInfo(canMap);
					}
				}
			}
		}

		JSONArray chillerList = (JSONArray) jsonObject.get("Chiller");
		if (! StringUtils.isEmpty(chillerList)) {
			for (Object chiller : chillerList) {
				Map<String, Object> chillerMap = objectMapper.convertValue(chiller, Map.class);

				chillerMap.put("moduleNo", moduleNo);
				mapper.setInsertChillerInfo(chillerMap);
			}
		}

		JSONArray ovenList = (JSONArray) jsonObject.get("Oven");
		if (! StringUtils.isEmpty(ovenList)) {
			for (Object oven : ovenList) {
				Map<String, Object> ovenMap = objectMapper.convertValue(oven, Map.class);

				ovenMap.put("moduleNo", moduleNo);
				mapper.setInsertOvenInfo(ovenMap);
			}
		}
	}

	/**
	 * Module_emg_code_yyyy_mm_dd_HH_MM_SS_sss
	 */
	private void setEmgFileData(ModuleVO mVO, FileInfo fileInfo, JSONObject jsonObject) throws Exception {
		String moduleNo = mVO.getModuleNo();
		ObjectMapper objectMapper = new ObjectMapper();
		Map<String, Object> errorMap = objectMapper.convertValue(jsonObject, Map.class);

		if (StringUtils.isEmpty(moduleNo)) {
			throw new ServiceException(- 1, "모듈정보 입력오류");
		}

//		errorMap = {"Module_Id":"1","Emg_Value1":"0","Eqp_Code":"wonikpne_220113_yc","Emg_Date_Time":"2022-01-14 10:45:40.007","Emg_Description":"鍮꾩젙�긽 醫낅즺","Emg_Code":"47"}
		errorMap.put("fileName", fileInfo.getOriginal());
		errorMap.put("alarmDiv", "EMG");
		errorMap.put("moduleNo", moduleNo);
		if (StringUtils.isEmpty(mapper.getEmgCodeHistory(errorMap))) {
			mapper.setInsertEmgCode(errorMap);
			AlarmVO aVO = mapper.getAlarmSetInfo(errorMap);
			List<UserVO> managerList = mapper.getUserInfoForModuleManager(errorMap);

			for (UserVO uVO : managerList) {
				String smsMessage = aVO.getSmsContent()
						.replace("#{담당자}", String.valueOf(uVO.getUserName()))
						.replace("#{사업장}", String.valueOf(uVO.getSiteName()))
						.replace("#{공장}", String.valueOf(uVO.getFactoryName()))
						.replace("#{구역}", String.valueOf(uVO.getAreaName()))
						.replace("#{설비}", String.valueOf(uVO.getModuleName()))
						.replace("#{알람코드}", String.valueOf(errorMap.get("Emg_Code")))
						.replace("#{알람메세지}", String.valueOf(errorMap.get("Emg_Description")));

				if ("Y".equals(aVO.getSmsYn())) {
					SmsUtil.toSendSMS(uVO.getDecryptedPhoneNo(), smsMessage);
					SmsUtil.toSendKakao(uVO.getDecryptedPhoneNo(), "emgalarm", smsMessage);
				}
				if ("Y".equals(aVO.getEmailYn())) {
//					mailService.sendMail(getSendMsgManger.get(i).get("EMAIL").toString(),email_title, email_content);
				}
			}
		}
	}

	public static String getAwColumnItem(short item) {
		String result = "";
		switch (item) {
			case 0x00:
				result = "PS_STATE";
			case 0x01:
				result = "PS_VOLTAGE";
				break;
			case 0x02:
				result = "PS_CURRENT";
				break;
			case 0x03:
				result = "PS_CAPACITY";
				break;
			case 0x04:
				result = "PS_IMPEDANCE";
				break;
			case 0x05:
				result = "PS_CODE";
				break;
			case 0x06:
				result = "PS_STEP_TIME";
				break;
			case 0x07:
				result = "PS_TOT_TIME";
				break;
			case 0x08:
				result = "PS_GRADE_CODE";
				break;
			case 0x09:
				result = "PS_STEP_NO";
				break;
			case 0x0A:
				result = "PS_WATT";
				break;
			case 0x0B:
				result = "PS_WATT_HOUR";
				break;
			case 0x0C:
				result = "PS_OVEN_TEMPERATURE";
				break;
			case 0x0D:
				result = "PS_OVEN_HUMIDITY";
				break;
			case 0x0E:
				result = "PS_STEP_TYPE";
				break;
			case 0x0F:
				result = "PS_CUR_CYCLE";
				break;
			case 0x10:
				result = "PS_TOT_CYCLE";
				break;
			case 0x11:
				result = "PS_TEST_NAME";
				break;
			case 0x12:
				result = "PS_SCHEDULE_NAME";
				break;
			case 0x13:
				result = "PS_CHANNEL_NO";
				break;
			case 0x14:
				result = "PS_MODULE_NO";
				break;
			case 0x15:
				result = "PS_LOT_NO";
				break;
			case 0x16:
				result = "PS_DATA_SEQ";
				break;
			case 0x17:
				result = "PS_AVG_CURRENT";
				break;
			case 0x18:
				result = "PS_AVG_VOLTAGE";
				break;
			case 0x19:
				result = "PS_CAPACITY_SUM";
				break;
			case 0x1A:
				result = "PS_CHARGE_CAP";
				break;
			case 0x1B:
				result = "PS_DISCHARGE_CAP";
				break;
			case 0x1C:
				result = "PS_METER_DATA";
				break;
			case 0x1D:
				result = "PS_START_TIME";
				break;
			case 0x1E:
				result = "PS_END_TIME";
				break;
			case 0x1F:
				result = "PS_CAN_DATA";
				break;
			case 0x20:
				result = "PS_CV_TIME";
				break;
			case 0x21:
				result = "PS_AUX_TEMPERATURE";
				break;
			case 0x22:
				result = "PS_AUX_VOLTAGE";
				break;
			case 0x23:
				result = "PS_AUX_TEMPERATURE_TH";
				break;
			case 0x24:
				result = "PS_CAPACITANCE";
				break;
			case 0x25:
				result = "PS_CHARGE_WH";
				break;
			case 0x26:
				result = "PS_DISCHARGE_WH";
				break;
			case 0x27:
				result = "PS_SYNC_DATE";
				break;
			case 0x28:
				result = "PS_SYNC_TIME";
				break;
			case 0x29:
				result = "PS_ACC_CYCLE1";
				break;
			case 0x2A:
				result = "PS_ACC_CYCLE2";
				break;
			case 0x2B:
				result = "PS_ACC_CYCLE3";
				break;
			case 0x2C:
				result = "PS_ACC_CYCLE4";
				break;
			case 0x2D:
				result = "PS_ACC_CYCLE5";
				break;
			case 0x2E:
				result = "PS_MULTI_CYCLE1";
				break;
			case 0x30:
				result = "PS_MULTI_CYCLE2";
				break;
			case 0x31:
				result = "PS_MULTI_CYCLE3";
				break;
			case 0x32:
				result = "PS_MULTI_CYCLE4";
				break;
			case 0x33:
				result = "PS_MULTI_CYCLE5";
				break;
			case 0x34:
				result = "PS_AUX_HUMIDITY";
				break;
			case 0x3F:
				result = "PS_AUX_DATA_COUNT";
				break;
			case 0x40:
				result = "PS_AUX_COMM_TEMP";
				break;
			case 0x41:
				result = "PS_AUX_COMM_VOLT";
				break;
			case 0x42:
				result = "PS_CAN_COMM_MASTER";
				break;
			case 0x43:
				result = "PS_CAN_COMM_SLAVE";
				break;
			case 0x44:
				result = "PS_COMM_STATE";
				break;
			case 0x45:
				result = "PS_CHAMBER_USING";
				break;
			case 0x46:
				result = "PS_RECORD_TIME_NO";
				break;
			case 0x48:
				result = "PS_VOLTAGE_INPUT";
				break;
			case 0x49:
				result = "PS_VOLTAGE_POWER";
				break;
			case 0x4A:
				result = "PS_VOLTAGE_BUS";
				break;
			case 0x4B:
				result = "PS_CAN_OUTPUT_STATE";
				break;
			case 0x4C:
				result = "PS_CAN_INPUT_STATE";
				break;
			case 0x4D:
				result = "PS_MUX_USE_STATE";
				break;
			case 0x4E:
				result = "PS_MUX_BACKUP_INFO";
				break;
			case 0x50:
				result = "PS_TOT_TIME_DAY";
				break;
			case 0x51:
				result = "PS_STEP_TIME_DAY";
				break;
			case 0x52:
				result = "PS_CV_TIME_DAY";
				break;
			case 0x53:
				result = "PS_LOAD_VOLT";
				break;
			case 0x54:
				result = "PS_LOAD_CURR";
				break;
			case 0x55:
				result = "PS_CHILLER_REF_TEMP";
				break;
			case 0x56:
				result = "PS_CHILLER_CUR_TEMP";
				break;
			case 0x57:
				result = "PS_CHILLER_REF_PUMP";
				break;
			case 0x58:
				result = "PS_CHILLER_CUR_PUMP";
				break;
			case 0x59:
				result = "PS_CELLBAL_CH_DATA";
				break;
			case 0x5A:
				result = "PS_CELLBAL_RES";
				break;
			case 0x5B:
				result = "PS_PWRSUPPLY_SETVOLT";
				break;
			case 0x5C:
				result = "PS_PWRSUPPLY_CURVOLT";
				break;
			case 0x5D:
				result = "PS_CBANK";
				break;
			case 0x5F:
				result = "PS_STEP_MODE";
				break;
			default:
				result = "";
				break;
		}
		return getItemKey(result);
	}

	public static String getItemKey(String aw) {
		String key = "" ;

		switch (aw) {
			case "PS_STATE":
				key = "chState" ;
				break ;
			case "PS_VOLTAGE":
				key = "fVoltage" ;
				break ;
			case "PS_VOLTAGE_INPUT":
				key = "fVoltageInput" ;
				break ;
			case "PS_VOLTAGE_POWER":
				key = "fVoltagePower" ;
				break ;
			case "PS_VOLTAGE_BUS":
				key = "fVoltageBus" ;
				break ;
			case "PS_CURRENT":
				key = "fCurrent" ;
				break ;
			case "PS_CAPACITY":
				key = "fCapacity" ;
				break ;
			case "PS_IMPEDANCE":
				key = "fImpedance" ;
				break ;
			case "PS_CODE":
				key = "chCode" ;
				break ;
			case "PS_STEP_TIME_DAY":
				key = "fStepTime_Day" ;
				break ;
			case "PS_STEP_TIME":
				key = "fStepTime" ;
				break ;
			case "PS_TOT_TIME_DAY":
				key = "fTotalTime_Day" ;
				break ;
			case "PS_TOT_TIME":
				key = "fTotalTime" ;
				break ;
			case "PS_GRADE_CODE":
				key = "chGradeCode" ;
				break ;
			case "PS_COMM_STATE":
				key = "chCommState" ;
				break ;
			case "PS_CAN_OUTPUT_STATE":
				key = "chOutputState" ;
				break ;
			case "PS_CAN_INPUT_STATE":
				key = "chInputState" ;
				break ;
			case "PS_STEP_NO":
				key = "chStepNo" ;
				break ;
			case "PS_WATT":
				key = "fWatt" ;
				break ;
			case "PS_WATT_HOUR":
				key = "fWattHour" ;
				break ;
			case "PS_OVEN_TEMPERATURE":
				key = "fOvenTemperature" ;
				break ;
			case "PS_OVEN_HUMIDITY":
				key = "fOvenHumidity" ;
				break ;
			case "PS_CHILLER_REF_TEMP":
				key = "fChillerRefTemp" ;
				break ;
			case "PS_CHILLER_CUR_TEMP":
				key = "fChillerCurTemp" ;
				break ;
			case "PS_CHILLER_REF_PUMP":
				key = "iChillerRefPump" ;
				break ;
			case "PS_CHILLER_CUR_PUMP":
				key = "iChillerCurPump" ;
				break ;
			case "PS_CELLBAL_CH_DATA":
				key = "dCellBALChData" ;
				break ;
			case "PS_PWRSUPPLY_SETVOLT":
				key = "fPwrSetVoltage" ;
				break ;
			case "PS_PWRSUPPLY_CURVOLT":
				key = "fPwrCurVoltage" ;
				break ;
			case "PS_LOAD_VOLT":
				key = "fLoadVoltage" ;
				break ;
			case "PS_LOAD_CURR":
				key = "fLoadCurrent" ;
				break ;
			case "PS_STEP_TYPE":
				key = "chStepType" ;
				break ;
			case "PS_CUR_CYCLE":
				key = "nCurrentCycleNum" ;
				break ;
			case "PS_TOT_CYCLE":
				key = "nTotalCycleNum" ;
				break ;
			case "PS_CHANNEL_NO":
				key = "chNo" ;
				break ;
			case "PS_DATA_SEQ":
				key = "lSaveSequence" ;
				break ;
			case "PS_AVG_CURRENT":
				key = "fAvgCurrent" ;
				break ;
			case "PS_AVG_VOLTAGE":
				key = "fAvgVoltage" ;
				break ;
			case "PS_CHARGE_CAP":
				key = "fChargeAh" ;
				break ;
			case "PS_DISCHARGE_CAP":
				key = "fDisChargeAh" ;
				break ;
			case "PS_CAPACITANCE":
				key = "fCapacitance" ;
				break ;
			case "PS_CHARGE_WH":
				key = "fChargeWh" ;
				break ;
			case "PS_DISCHARGE_WH":
				key = "fDisChargeWh" ;
				break ;
			case "PS_CV_TIME_DAY":
				key = "fCVTime_Day" ;
				break ;
			case "PS_CV_TIME":
				key = "fCVTime" ;
				break ;
			case "PS_SYNC_DATE":
				key = "lSyncDate" ;
				break ;
			case "PS_SYNC_TIME":
				key = "fSyncTime" ;
				break ;
			case "PS_ACC_CYCLE1":
				key = "nAccCycleGroupNum1" ;
				break ;
			case "PS_ACC_CYCLE2":
				key = "nAccCycleGroupNum2" ;
				break ;
			case "PS_ACC_CYCLE3":
				key = "nAccCycleGroupNum3" ;
				break ;
			case "PS_ACC_CYCLE4":
				key = "nAccCycleGroupNum4" ;
				break ;
			case "PS_ACC_CYCLE5":
				key = "nAccCycleGroupNum5" ;
				break ;
			case "PS_MULTI_CYCLE1":
				key = "nMultiCycleGroupNum1" ;
				break ;
			case "PS_MULTI_CYCLE2":
				key = "nMultiCycleGroupNum2" ;
				break ;
			case "PS_MULTI_CYCLE3":
				key = "nMultiCycleGroupNum3" ;
				break ;
			case "PS_MULTI_CYCLE4":
				key = "nMultiCycleGroupNum4" ;
				break ;
			case "PS_MULTI_CYCLE5":
				key = "nMultiCycleGroupNum5" ;
				break ;
			default:
				key = "";
				break;
		}

		return key ;
	}

	public static String getBytes(LEDataInputStream id, byte[] b, String ch) throws Exception {
		id.read(b, 0, b.length);
		return new String(b, ch).trim() ;
	}

	public static String getReadShort(LEDataInputStream id) throws Exception {
		return String.valueOf(id.readShort()) ;
	}

	public static String getReadFloat(LEDataInputStream id) throws  Exception {
		return String.valueOf(id.readFloat()) ;
	}

	public static String getReadInt(LEDataInputStream id) throws Exception {
		return String.valueOf(id.readInt()) ;
	}

	public static String getReadUnInt(LEDataInputStream id) throws Exception {
		return String.valueOf(id.readUnsignedInt()) ;
	}

	public static String getReadByte(LEDataInputStream id) throws Exception {
		return String.valueOf(id.readByte()) ;
	}

	public static String getReadUnByte(LEDataInputStream id) throws Exception {
		return String.valueOf(id.readUnsignedByte()) ;
	}

}
