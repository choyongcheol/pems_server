package pems.api.agent;

import io.swagger.annotations.ApiOperation;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pems.vo.AgentVO;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
//@RequestMapping (value = "/api")
public class AgentController {

	@Resource
	private AgentService service;


	/**
	 * 사업장 목록
	 */
	@ResponseBody
	@RequestMapping(value = "/api/site/getSiteListJson", method = RequestMethod.POST)
	public Map<String, Object> getSiteListJson(@ApiIgnore HttpServletRequest req) {
		Map<String, Object> map = new HashMap<>();

		try {
			map.put("list", service.getSiteList()) ;
			map.put("success", true) ;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return map ;
	}

	@ResponseBody
	@RequestMapping(value = "/api/area/getFactoryList", produces = "application/json; charset=utf-8", method = {RequestMethod.GET})
	public JSONObject getApiFactoryList(@ApiIgnore HttpServletRequest req) {
		JSONObject json = new JSONObject();
		Map map = new HashMap();
		map.put("site_seq",req.getParameter("site_seq"));

		json.put("list", service.getFactoryList(map));
		return json;
	}


	@ResponseBody
	@RequestMapping(value = "/api/area/getAreaCombo", produces = "application/json; charset=utf-8", method = {RequestMethod.GET})
	public JSONObject getAreaList(@ApiIgnore HttpServletRequest request) {
		JSONObject json = new JSONObject();
		Map param = new HashMap();
		param.put("factory_seq",request.getParameter("factory_seq"));
		json.put("list", service.getAreaList(param));
		return json;
	}


	/**
	 * Agent 등록
	 */
	@ResponseBody
	@RequestMapping(value = "/api/agent/resAgent", produces = "application/json; charset=utf-8", method = {RequestMethod.POST})
	public Map<String, Object> setInsertAgent( @RequestBody HashMap<String, Object> param) {
		Map<String, Object> map = new HashMap<>();

		try {
		//	param {agent_version=, module_type=, area_seq=1, json_file_dir=C:\Program Files (x86)\PNE CTSPack\State, pc_ip=192.168.178.1}
			service.setInsertAgentInfo(param);

			map.put("agent_seq", String.valueOf(param.get("agent_no")));
			map.put("result", "success");

		} catch (Exception e) {
			e.printStackTrace();
		}

		return map ;
	}

	/**
	 * Agent 파일 업로드
	 */
	@ResponseBody
	@ApiOperation (value = "JSON 및 바이너리 파일 수신 - (인수인계 문서)", notes = "JSON, CYC, CTS, SCH 파일 수신")
	@RequestMapping (value = "/api/responseData", method = RequestMethod.POST)
	public Map<String, Object> setResultBinaryData(HttpServletRequest request, AgentVO vo, @RequestParam Map<String, Object> param) {
		Map<String, Object> map = new HashMap<>();

		try {
			param.put("agentNo", param.get("agent_seq")) ;

			vo.setAgentNo(String.valueOf(param.get("agentNo"))) ;

			service.setAgentFileUpload(request, vo, param) ;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return map;
	}


}
