package pems.api.agent;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;
import pems.vo.AgentVO;
import pems.vo.AlarmVO;
import pems.vo.ChannelVO;
import pems.vo.UserVO;
import pems.vo.CommonVO;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class AgentMapper {

	private static final String namespace = "AgentMap";

	@Resource
	private SqlSession sqlSession;

	public List<Map<String, Object>> getSiteList() {
		return sqlSession.selectList(namespace + ".getSiteList");
	}

	public List<Map<String, Object>> getFactoryList(Map<String, Object> param) {
		return sqlSession.selectList(namespace + ".getFactoryList", param);
	}

	public List<Map<String, Object>> getAreaList(Map<String, Object> param) {
		return sqlSession.selectList(namespace + ".getAreaList", param);
	}

	public int setInsertAgentInfo(HashMap<String, Object> param) {
		return sqlSession.insert(namespace + ".setInsertAgentInfo", param) ;
	}

	public List<AgentVO> getAgentList(CommonVO vo) throws Exception {
		return sqlSession.selectList(namespace + ".getAgentList", vo);
	}

	public AgentVO getAgentInfo(AgentVO vo) throws Exception {
		return sqlSession.selectOne(namespace + ".getAgentInfo", vo);
	}

	public int setUpdateAgentInfo(AgentVO vo) {
		return sqlSession.update(namespace + ".setUpdateAgentInfo", vo);
	}

	public int setInsertModuleInfo(Map<String, Object> map) throws Exception {
		return sqlSession.insert(namespace + ".setInsertModuleInfo", map);
	}

	public int setInsertChannelInfo(Map<String, Object> map) {
		return sqlSession.insert(namespace + ".setInsertChannelInfo", map);
	}

	public ChannelVO getChannelInfo(ChannelVO vo) throws Exception {
		return sqlSession.selectOne(namespace + ".getChannelInfo", vo);
	}

	public int setInsertAuxInfo(Map<String, Object> map) {
		return sqlSession.insert(namespace + ".setInsertAuxInfo", map);
	}

	public int setInsertCanInfo(Map<String, Object> map) {
		return sqlSession.insert(namespace + ".setInsertCanInfo", map);
	}

	public int setInsertChillerInfo(Map<String, Object> map) {
		return sqlSession.insert(namespace + ".setInsertChillerInfo", map);
	}

	public int setInsertOvenInfo(Map<String, Object> map) {
		return sqlSession.insert(namespace + ".setInsertOvenInfo", map);
	}

	public int setInsertEmgCode(Map<String, Object> map) {
		return sqlSession.insert(namespace + ".setInsertEmgCode", map);
	}

	public Map<String, Object> getEmgCodeHistory(Map<String, Object> map) {
		return sqlSession.selectOne(namespace + ".getEmgCodeHistory", map);
	}

	public AlarmVO getAlarmSetInfo(Map<String, Object> map) {
		return sqlSession.selectOne(namespace + ".getAlarmSetInfo", map);
	}

	public List<UserVO> getUserInfoForModuleManager(Map<String, Object> map) {
		return sqlSession.selectList(namespace + ".getUserInfoForModuleManager", map);
	}

	public int setInsertProcessInfo(Map<String, Object> param) {
		return sqlSession.insert(namespace + ".setInsertProcessInfo", param);
	}

	public Map<String, Object> getProcessInfo(Map<String, Object> param) {
		return sqlSession.selectOne(namespace + ".getProcessInfo", param);
	}

	public int setInsertProcessChannelInfo(Map<String, Object> param) {
		return sqlSession.insert(namespace + ".setInsertProcessChannelInfo", param);
	}

	public Map<String, Object> getProcessChannelInfo(Map<String, Object> param) {
		return sqlSession.selectOne(namespace + ".getProcessChannelInfo", param);
	}

	public int setUpdateProcessChannelInfo(Map<String, Object> param) {
		return sqlSession.update(namespace + ".setUpdateProcessChannelInfo", param);
	}

	public int setInsertSchHeader(Map<String, Object> headerMap) {
		return sqlSession.insert(namespace + ".setInsertSchHeader", headerMap);
	}

	public Map<String, Object> getSchHeader(Map<String, Object> map) {
		return sqlSession.selectOne(namespace + ".getSchHeader", map);
	}

	public int setInsertSchModel(Map<String, Object> modelMap) {
		return sqlSession.insert(namespace + ".setInsertSchModel", modelMap);
	}

	public int setInsertSchProcess(Map<String, Object> scheduleMap) {
		return sqlSession.insert(namespace + ".setInsertSchProcess", scheduleMap);
	}

	public int setInsertSchCellCheck(Map<String, Object> cellCheckMap) {
		return sqlSession.insert(namespace + ".setInsertSchCellCheck", cellCheckMap);
	}

	public int setInsertCtsRecord(Map<String, Object> map) {
		return sqlSession.insert(namespace + ".setInsertCtsRecord", map);
	}

	public Map<String, Object> getCtsHeader(Map<String, Object> headerMap) {
		return sqlSession.selectOne(namespace + ".getCtsHeader", headerMap);
	}

	public int setInsertCtsHeader(Map<String, Object> headerMap) {
		return sqlSession.insert(namespace + ".setInsertCtsHeader", headerMap);
	}

	public Integer getLatestCtsNo(Map<String, Object> ctsHeaderInfo) {
		return sqlSession.selectOne(namespace + ".getLatestCtsNo", ctsHeaderInfo);
	}

	public int setInsertCycHeader(Map<String, Object> headerMap) {
		return sqlSession.insert(namespace + ".setInsertCycHeader", headerMap);
	}

	public Map<String, Object> getCycHeader(Map<String, Object> headerMap) {
		return sqlSession.selectOne(namespace + ".getCycHeader", headerMap);
	}

	public Integer getLatestCycSeq(Map<String, Object> cycHeaderInfo) {
		return sqlSession.selectOne(namespace + ".getLatestCycSeq", cycHeaderInfo);
	}

	public int setInsertCycRecord(List<Map<String, Object>> list) {
		return sqlSession.insert(namespace + ".setInsertCycRecord", list);
	}

	public int setUpdateReceiveDate(Map<String, Object> param) throws Exception {
		return sqlSession.update(namespace + ".setUpdateReceiveDate", param) ;
	}
}
