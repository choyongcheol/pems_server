package pems.web.setting;

import framework.exception.ServiceException;
import org.springframework.util.StringUtils;
import org.springframework.stereotype.Service;
import pems.vo.AlarmVO;
import pems.vo.Paging;
import pems.vo.CommonVO;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class SettingService {

	@Resource
	private SettingMapper mapper;

	/**
	 * 지역 목록
	 */
	public List<CommonVO> getCityList(Paging paging, CommonVO vo) throws Exception {
		List<CommonVO> list = new ArrayList<>() ;

		int count = mapper.getCityCount(vo) ;
		if (count > 0) {
			paging.setTotalCount(count) ;
			list = mapper.getCityList(paging, vo) ;
		}

		return list ;
	}

	/**
	 * 사업장 목록
	 */
	public List<CommonVO> getSiteList(Paging paging, CommonVO vo) throws Exception {
		List<CommonVO> list = new ArrayList<>() ;

		int count = mapper.getSiteCount(vo) ;
		if (count > 0) {
			paging.setTotalCount(count) ;
			list = mapper.getSiteList(paging, vo) ;
		}

		return list ;
	}

	/**
	 * 공장 목록
	 */
	public List<CommonVO> getFactoryList(Paging paging, CommonVO vo) throws Exception {
		List<CommonVO> list = new ArrayList<>() ;

		int count = mapper.getFactoryCount(vo) ;
		if (count > 0) {
			paging.setTotalCount(count) ;
			list = mapper.getFactoryList(paging, vo) ;
		}

		return list ;
	}

	/**
	 * 구역 목록
	 */
	public List<CommonVO> getAreaList(Paging paging, CommonVO vo) throws Exception {
		List<CommonVO> list = new ArrayList<>() ;

		int count = mapper.getAreaCount(vo) ;
		if (count > 0) {
			paging.setTotalCount(count) ;
			list = mapper.getAreaList(paging, vo) ;
		}

		return list ;
	}


	/**
	 * 지역 등록
	 */
	public void setInsertCity(CommonVO vo) throws Exception {
		CommonVO cityInfo = mapper.getCityInfo(vo) ;

		if (! StringUtils.isEmpty(cityInfo)) {
			throw new ServiceException(- 1, "이미 등록된 지역입니다.");
		}

		mapper.setInsertCity(vo) ;
	}

	/**
	 * 지역 수정
	 */
	public void setUpdateCity(CommonVO vo) throws Exception {
		CommonVO cityInfo = mapper.getCityInfo(vo) ;

		if (! StringUtils.isEmpty(cityInfo)) {
			throw new ServiceException(- 1, "이미 등록된 지역입니다.");
		}

		mapper.setUpdateCity(vo) ;
	}

	/**
	 * 지역 삭제
	 */
	public void setDeleteCity(CommonVO vo) throws Exception {
		mapper.setDeleteCity(vo) ;
	}


	/**
	 * 사업장 등록
	 */
	public void setInsertSite(CommonVO vo) throws Exception {
		CommonVO siteInfo = mapper.getSiteInfo(vo) ;

		if (! StringUtils.isEmpty(siteInfo)) {
			throw new ServiceException(- 1, "이미 등록된 사업장입니다.");
		}

		mapper.setInsertSite(vo) ;
	}

	/**
	 * 사업장 수정
	 */
	public void setUpdateSite(CommonVO vo) throws Exception {
//		CommonVO siteInfo = mapper.getSiteInfo(vo) ;

//		if (! StringUtils.isEmpty(siteInfo)) {
//			throw new ServiceException(- 1, "이미 등록된 사업장입니다.");
//		}

		mapper.setUpdateSite(vo) ;
	}

	/**
	 * 사업장 삭제
	 */
	public void setDeleteSite(CommonVO vo) throws Exception {
		mapper.setDeleteSite(vo) ;
	}


	// @@@@@@@@@@@@

	/**
	 * 공장 등록
	 */
	public void setInsertFactory(CommonVO vo) throws Exception {
		CommonVO factoryInfo = mapper.getFactoryInfo(vo) ;

		if (! StringUtils.isEmpty(factoryInfo)) {
			throw new ServiceException(- 1, "이미 등록된 공장입니다.");
		}

		mapper.setInsertFactory(vo) ;
	}

	/**
	 * 공장 수정
	 */
	public void setUpdateFactory(CommonVO vo) throws Exception {
		CommonVO factoryInfo = mapper.getFactoryInfo(vo) ;

		if (! StringUtils.isEmpty(factoryInfo)) {
			throw new ServiceException(- 1, "이미 등록된 공장입니다.");
		}

		mapper.setUpdateFactory(vo) ;
	}

	/**
	 * 공장 삭제
	 */
	public void setDeleteFactory(CommonVO vo) throws Exception {
		mapper.setDeleteFactory(vo) ;
	}

	// @@@@@@@@@@@@@@@@@

	/**
	 * 구역 등록
	 */
	public void setInsertArea(CommonVO vo) throws Exception {
		CommonVO areaInfo = mapper.getAreaInfo(vo) ;

		if (! StringUtils.isEmpty(areaInfo)) {
			throw new ServiceException(- 1, "이미 등록된 구역입니다.");
		}

		mapper.setInsertArea(vo) ;
	}

	/**
	 * 구역 수정
	 */
	public void setUpdateArea(CommonVO vo) throws Exception {
//		CommonVO areaInfo = mapper.getAreaInfo(vo) ;
//
//		if (! StringUtils.isEmpty(areaInfo)) {
//			throw new ServiceException(- 1, "이미 등록된 구역입니다.");
//		}

		mapper.setUpdateArea(vo) ;
	}

	/**
	 * 구역 삭제
	 */
	public void setDeleteArea(CommonVO vo) throws Exception {
		mapper.setDeleteArea(vo) ;
	}

}
