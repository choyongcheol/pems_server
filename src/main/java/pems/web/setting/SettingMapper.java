package pems.web.setting;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;
import pems.vo.AlarmVO;
import pems.vo.Paging;
import pems.vo.CommonVO;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class SettingMapper {

	@Resource
	private SqlSession sqlSession ;

	private static final String namespace = "SettingMap" ;


	// 지역 ~
	public int setInsertCity(CommonVO vo) throws Exception {
		return sqlSession.insert(namespace + ".setInsertCity", vo) ;
	}

	public int setUpdateCity(CommonVO vo) throws Exception {
		return sqlSession.update(namespace + ".setUpdateCity", vo) ;
	}

	public int setDeleteCity(CommonVO vo) throws Exception {
		return sqlSession.delete(namespace + ".setDeleteCity", vo) ;
	}

	public CommonVO getCityInfo(CommonVO vo) throws Exception {
		return sqlSession.selectOne(namespace + ".getCityInfo", vo) ;
	}

	public int getCityCount(CommonVO vo) throws Exception {
		Map<String, Object> param = new HashMap<>() ;
		param.put("vo", vo) ;
		return sqlSession.selectOne(namespace + ".getCityCount", param) ;
	}

	public List<CommonVO> getCityList(Paging paging, CommonVO vo) throws Exception {
		Map<String, Object> param = new HashMap<>() ;
		param.put("vo", vo) ;
		param.put("paging", paging) ;
		return sqlSession.selectList(namespace + ".getCityList", param) ;
	}

	// 지역 ~

	// 사업장 ~
	public int setInsertSite(CommonVO vo) throws Exception {
		return sqlSession.insert(namespace + ".setInsertSite", vo) ;
	}

	public int setUpdateSite(CommonVO vo) throws Exception {
		return sqlSession.update(namespace + ".setUpdateSite", vo) ;
	}

	public int setDeleteSite(CommonVO vo) throws Exception {
		return sqlSession.delete(namespace + ".setDeleteSite", vo) ;
	}

	public CommonVO getSiteInfo(CommonVO vo) throws Exception {
		Map<String, Object> param = new HashMap<>() ;
		param.put("vo", vo) ;
		return sqlSession.selectOne(namespace + ".getSiteInfo", param) ;
	}

	public int getSiteCount(CommonVO vo) throws Exception {
		Map<String, Object> param = new HashMap<>() ;
		param.put("vo", vo) ;
		return sqlSession.selectOne(namespace + ".getSiteCount", param) ;
	}

	public List<CommonVO> getSiteList(Paging paging, CommonVO vo) throws Exception {
		Map<String, Object> param = new HashMap<>() ;
		param.put("vo", vo) ;
		param.put("paging", paging) ;
		return sqlSession.selectList(namespace + ".getSiteList", param) ;
	}

	 // 사업장 ~

	// 공장 ~
	public int setInsertFactory(CommonVO vo) throws Exception {
		return sqlSession.insert(namespace + ".setInsertFactory", vo) ;
	}

	public int setUpdateFactory(CommonVO vo) throws Exception {
		return sqlSession.update(namespace + ".setUpdateFactory", vo) ;
	}

	public int setDeleteFactory(CommonVO vo) throws Exception {
		return sqlSession.delete(namespace + ".setDeleteFactory", vo) ;
	}

	public CommonVO getFactoryInfo(CommonVO vo) throws Exception {
		Map<String, Object> param = new HashMap<>() ;
		param.put("vo", vo) ;
		return sqlSession.selectOne(namespace + ".getFactoryInfo", param) ;
	}

	public int getFactoryCount(CommonVO vo) throws Exception {
		Map<String, Object> param = new HashMap<>() ;
		param.put("vo", vo) ;
		return sqlSession.selectOne(namespace + ".getFactoryCount", param) ;
	}

	public List<CommonVO> getFactoryList(Paging paging, CommonVO vo) throws Exception {
		Map<String, Object> param = new HashMap<>() ;
		param.put("vo", vo) ;
		param.put("paging", paging) ;
		return sqlSession.selectList(namespace + ".getFactoryList", param) ;
	}

	// 공장 ~

	// 구역 ~

	public int setInsertArea(CommonVO vo) throws Exception {
		return sqlSession.insert(namespace + ".setInsertArea", vo) ;
	}

	public int setUpdateArea(CommonVO vo) throws Exception {
		return sqlSession.update(namespace + ".setUpdateArea", vo) ;
	}

	public int setDeleteArea(CommonVO vo) throws Exception {
		return sqlSession.delete(namespace + ".setDeleteArea", vo) ;
	}

	public CommonVO getAreaInfo(CommonVO vo) throws Exception {
		Map<String, Object> param = new HashMap<>() ;
		param.put("vo", vo) ;
		return sqlSession.selectOne(namespace + ".getAreaInfo", param) ;
	}

	public int getAreaCount(CommonVO vo) throws Exception {
		Map<String, Object> param = new HashMap<>() ;
		param.put("vo", vo) ;
		return sqlSession.selectOne(namespace + ".getAreaCount", param) ;
	}

	public List<CommonVO> getAreaList(Paging paging, CommonVO vo) throws Exception {
		Map<String, Object> param = new HashMap<>() ;
		param.put("vo", vo) ;
		param.put("paging", paging) ;
		return sqlSession.selectList(namespace + ".getAreaList", param) ;
	}
	// 구역 ~




}
