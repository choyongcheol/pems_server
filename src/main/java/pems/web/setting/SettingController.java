package pems.web.setting;

import framework.exception.ServiceException;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pems.vo.AlarmVO;
import pems.vo.Paging;
import pems.vo.CommonVO;
import pems.vo.WebSessionVO;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping(value = "/setting")
public class SettingController {

	@Resource
	private ObjectFactory<WebSessionVO> webSessionFactory ;

	@Resource
	private SettingService service ;

	/**
	 * 지역 목록
	 */
	@RequestMapping (value = "/cityList", method = RequestMethod.GET)
	public String getCityList(Paging paging, CommonVO vo, Model model) {
		String viewPath = "/web/setting/cityList" ;

		try {
			model.addAttribute("list", service.getCityList(paging, vo)) ;
		} catch (Exception e) {
			viewPath = "redirect:/" ;
			e.printStackTrace();
		}

		return viewPath;
	}

	/**
	 * 사업장 목록
	 */
	@RequestMapping (value = "/siteList", method = RequestMethod.GET)
	public String getSiteList(Paging paging, CommonVO vo, Model model) {
		String viewPath = "/web/setting/siteList" ;

		try {
			paging.setRowPerPage(1000);
			model.addAttribute("cityList", service.getCityList(paging, vo)) ;

			paging.setRowPerPage(30);
			model.addAttribute("list", service.getSiteList(paging, vo)) ;
		} catch (Exception e) {
			viewPath = "redirect:/" ;
			e.printStackTrace();
		}

		return viewPath;
	}

	/**
	 * 공장 목록
	 */
	@RequestMapping (value = "/factoryList", method = RequestMethod.GET)
	public String getFactoryList(Paging paging, CommonVO vo, Model model) {
		String viewPath = "/web/setting/factoryList" ;

		try {
			paging.setRowPerPage(1000);
			model.addAttribute("siteList", service.getSiteList(paging, vo)) ;

			paging.setRowPerPage(30);
			model.addAttribute("list", service.getFactoryList(paging, vo)) ;
		} catch (Exception e) {
			viewPath = "redirect:/" ;
			e.printStackTrace();
		}

		return viewPath;
	}

	/**
	 * 공장 목록
	 */
	@ResponseBody
	@RequestMapping (value = "/factoryList", method = RequestMethod.POST)
	public Map<String, Object> getFactoryList(Paging paging, CommonVO vo) {
		Map<String, Object> map = new HashMap<>() ;

		try {
			paging.setRowPerPage(1000);
			map.put("vo", vo) ;
			map.put("list", service.getFactoryList(paging, vo)) ;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return map;
	}

	/**
	 * 구역 목록
	 */
	@RequestMapping (value = "/areaList", method = RequestMethod.GET)
	public String getAreaList(Paging paging, CommonVO vo, Model model) {
		String viewPath = "/web/setting/areaList" ;

		try {
			paging.setRowPerPage(1000);
			model.addAttribute("siteList", service.getSiteList(paging, vo)) ;

			paging.setRowPerPage(30);
			model.addAttribute("list", service.getAreaList(paging, vo)) ;
		} catch (Exception e) {
			viewPath = "redirect:/" ;
			e.printStackTrace();
		}

		return viewPath;
	}

	/**
	 * 구역 목록
	 */
	@ResponseBody
	@RequestMapping (value = "/areaList", method = RequestMethod.POST)
	public Map<String, Object> getAreaList(Paging paging, CommonVO vo) {
		Map<String, Object> map = new HashMap<>() ;

		try {
			paging.setRowPerPage(1000);
			map.put("vo", vo) ;
			map.put("list", service.getAreaList(paging, vo)) ;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return map;
	}

	/**
	 * 지역 등록
	 */
	@ResponseBody
	@RequestMapping (value = "/insertCity", method = RequestMethod.POST)
	public Map<String, Object> setInsertCity(CommonVO vo) {
		Map<String, Object> map = new HashMap<String, Object>();

		try {
			WebSessionVO session = webSessionFactory.getObject() ;
			vo.setRegUserNo(session.getUserNo());
			service.setInsertCity(vo) ;

			map.put("returnCode", 1);
			map.put("returnMessage", "등록되었습니다.");
		} catch (ServiceException se) {
			map.put("returnCode", se.getErrorCode());
			map.put("returnMessage", se.getErrorMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return map;
	}

	/**
	 * 지역 수정
	 */
	@ResponseBody
	@RequestMapping (value = "/setUpdateCity", method = RequestMethod.POST)
	public Map<String, Object> setUpdateCity(CommonVO vo) {
		Map<String, Object> map = new HashMap<String, Object>();

		try {
			service.setUpdateCity(vo) ;

			map.put("returnCode", 1);
			map.put("returnMessage", "수정되었습니다.");
		} catch (ServiceException se) {
			map.put("returnCode", se.getErrorCode());
			map.put("returnMessage", se.getErrorMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return map;
	}

	/**
	 * 지역 삭제
	 */
	@ResponseBody
	@RequestMapping (value = "/setDeleteCity", method = RequestMethod.POST)
	public Map<String, Object> setDeleteCity(CommonVO vo) {
		Map<String, Object> map = new HashMap<String, Object>();

		try {
			service.setDeleteCity(vo) ;

			map.put("returnCode", 1);
			map.put("returnMessage", "삭제되었습니다.");
		} catch (ServiceException se) {
			map.put("returnCode", se.getErrorCode());
			map.put("returnMessage", se.getErrorMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return map;
	}

	/**
	 * 사업장 등록
	 */
	@ResponseBody
	@RequestMapping (value = "/insertSite", method = RequestMethod.POST)
	public Map<String, Object> setInsertSite(CommonVO vo) {
		Map<String, Object> map = new HashMap<String, Object>();

		try {
			WebSessionVO session = webSessionFactory.getObject() ;
			vo.setRegUserNo(session.getUserNo());
			service.setInsertSite(vo) ;

			map.put("returnCode", 1);
			map.put("returnMessage", "등록되었습니다.");
		} catch (ServiceException se) {
			map.put("returnCode", se.getErrorCode());
			map.put("returnMessage", se.getErrorMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return map;
	}

	/**
	 * 사업장 수정
	 */
	@ResponseBody
	@RequestMapping (value = "/setUpdateSite", method = RequestMethod.POST)
	public Map<String, Object> setUpdateSite(CommonVO vo) {
		Map<String, Object> map = new HashMap<String, Object>();

		try {
			service.setUpdateSite(vo) ;

			map.put("returnCode", 1);
			map.put("returnMessage", "수정되었습니다.");
		} catch (ServiceException se) {
			map.put("returnCode", se.getErrorCode());
			map.put("returnMessage", se.getErrorMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return map;
	}

	/**
	 * 사업장 삭제
	 */
	@ResponseBody
	@RequestMapping (value = "/setDeleteSite", method = RequestMethod.POST)
	public Map<String, Object> setDeleteSite(CommonVO vo) {
		Map<String, Object> map = new HashMap<String, Object>();

		try {
			service.setDeleteSite(vo) ;

			map.put("returnCode", 1);
			map.put("returnMessage", "삭제되었습니다.");
		} catch (ServiceException se) {
			map.put("returnCode", se.getErrorCode());
			map.put("returnMessage", se.getErrorMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return map;
	}


	/**
	 * 공장 등록
	 */
	@ResponseBody
	@RequestMapping (value = "/insertFactory", method = RequestMethod.POST)
	public Map<String, Object> setInsertFactory(CommonVO vo) {
		Map<String, Object> map = new HashMap<String, Object>();

		try {
			WebSessionVO session = webSessionFactory.getObject() ;
			vo.setRegUserNo(session.getUserNo());
			service.setInsertFactory(vo) ;

			map.put("returnCode", 1);
			map.put("returnMessage", "등록되었습니다.");
		} catch (ServiceException se) {
			map.put("returnCode", se.getErrorCode());
			map.put("returnMessage", se.getErrorMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return map;
	}

	/**
	 * 공장 수정
	 */
	@ResponseBody
	@RequestMapping (value = "/setUpdateFactory", method = RequestMethod.POST)
	public Map<String, Object> setUpdateFactory(CommonVO vo) {
		Map<String, Object> map = new HashMap<String, Object>();

		try {
			service.setUpdateFactory(vo) ;

			map.put("returnCode", 1);
			map.put("returnMessage", "수정되었습니다.");
		} catch (ServiceException se) {
			map.put("returnCode", se.getErrorCode());
			map.put("returnMessage", se.getErrorMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return map;
	}

	/**
	 * 공장 삭제
	 */
	@ResponseBody
	@RequestMapping (value = "/setDeleteFactory", method = RequestMethod.POST)
	public Map<String, Object> setDeleteFactory(CommonVO vo) {
		Map<String, Object> map = new HashMap<String, Object>();

		try {
			service.setDeleteFactory(vo) ;

			map.put("returnCode", 1);
			map.put("returnMessage", "삭제되었습니다.");
		} catch (ServiceException se) {
			map.put("returnCode", se.getErrorCode());
			map.put("returnMessage", se.getErrorMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return map;
	}

	/**
	 * 구역 등록
	 */
	@ResponseBody
	@RequestMapping (value = "/insertArea", method = RequestMethod.POST)
	public Map<String, Object> setInsertArea(CommonVO vo) {
		Map<String, Object> map = new HashMap<String, Object>();

		try {
			WebSessionVO session = webSessionFactory.getObject() ;
			vo.setRegUserNo(session.getUserNo());
			service.setInsertArea(vo) ;

			map.put("returnCode", 1);
			map.put("returnMessage", "등록되었습니다.");
		} catch (ServiceException se) {
			map.put("returnCode", se.getErrorCode());
			map.put("returnMessage", se.getErrorMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return map;
	}

	/**
	 * 구역 수정
	 */
	@ResponseBody
	@RequestMapping (value = "/setUpdateArea", method = RequestMethod.POST)
	public Map<String, Object> Area(CommonVO vo) {
		Map<String, Object> map = new HashMap<String, Object>();

		try {
			service.setUpdateArea(vo) ;

			map.put("returnCode", 1);
			map.put("returnMessage", "수정되었습니다.");
		} catch (ServiceException se) {
			map.put("returnCode", se.getErrorCode());
			map.put("returnMessage", se.getErrorMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return map;
	}

	/**
	 * 구역 삭제
	 */
	@ResponseBody
	@RequestMapping (value = "/setDeleteArea", method = RequestMethod.POST)
	public Map<String, Object> setDeleteArea(CommonVO vo) {
		Map<String, Object> map = new HashMap<String, Object>();

		try {
			service.setDeleteArea(vo) ;

			map.put("returnCode", 1);
			map.put("returnMessage", "삭제되었습니다.");
		} catch (ServiceException se) {
			map.put("returnCode", se.getErrorCode());
			map.put("returnMessage", se.getErrorMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return map;
	}

}
