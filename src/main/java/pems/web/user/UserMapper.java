package pems.web.user ;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;
import pems.vo.Paging;
import pems.vo.UserVO;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class UserMapper {

	@Resource
	private SqlSession sqlSession ;

	private static final String namespace = "UserMap" ;

	public List<UserVO> getUserList(Paging paging, UserVO vo) {
		Map<String, Object> param = new HashMap<>();
		param.put("vo", vo);
		param.put("paging", paging);
		return sqlSession.selectList(namespace + ".getUserList", param);
	}

	public void setUpdateUserInfo(UserVO vo) {
		sqlSession.update(namespace + ".setUpdateUserInfo", vo) ;
	}

	public void setUpdateUserPw(UserVO vo) {
		sqlSession.update(namespace + ".setUpdateUserPw", vo) ;
	}


}
