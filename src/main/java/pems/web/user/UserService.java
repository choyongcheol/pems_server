package pems.web.user;

import org.springframework.stereotype.Service;
import pems.utils.RSA;
import pems.vo.Paging;
import pems.vo.UserVO;
import pems.web.login.LoginMapper;
import pems.vo.WebSessionVO;

import javax.annotation.Resource;
import java.security.PrivateKey;
import java.util.List;

@Service
public class UserService {

	@Resource
	private UserMapper mapper;

	@Resource
	private LoginMapper loginMapper;


	/**
	 * 회원 정보변경
	 */
	public void setUpdateUserInfo(UserVO vo) throws Exception {
		mapper.setUpdateUserInfo(vo) ;
	}

	/**
	 * 회원 비밀번호 변경
	 */
	public void setUpdateUserPw(UserVO vo, PrivateKey privateKey) throws Exception {
		vo.setSignPassword(RSA.decryptRsa(privateKey, vo.getSignPassword())) ;
		mapper.setUpdateUserPw(vo) ;
	}

	/**
	 * 세션 등록용 회원 정보조회
	 */
	public WebSessionVO getWebSessionUserInfo(UserVO vo) {
		return loginMapper.getWebSessionUserInfo(vo) ;
	}

	public List<UserVO> getUserList(Paging paging, UserVO vo) {
		paging.setRowPerPage(1000);

		return mapper.getUserList(paging, vo) ;
	}
}
