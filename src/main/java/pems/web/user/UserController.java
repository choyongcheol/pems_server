package pems.web.user ;

import framework.exception.ServiceException;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pems.utils.RSA;
import pems.vo.Paging;
import pems.vo.UserVO;
import pems.vo.WebSessionVO;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.security.PrivateKey;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping(value = "/user")
public class UserController {

	@Resource
	private ObjectFactory<WebSessionVO> webSessionFactory ;

	@Resource
	private UserService service;


	/**
	 * 비밀번호 암호화 키 발행
	 */
	@ResponseBody
	@RequestMapping (value = "/getRsaKey", method = RequestMethod.POST)
	public Map<String, Object> getRsaKey(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>() ;

		try {
			// 암호화 키생성
			RSA.getRsaKey(request);

			HttpSession session = request.getSession();
			map.put("publicKeyModulus", session.getAttribute("publicKeyModulus")) ;
			map.put("publicKeyExponent", session.getAttribute("publicKeyExponent")) ;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return map ;
	}

	/**
	 * 유저 목록
	 */
	@ResponseBody
	@RequestMapping (value = "/getUserList", method = RequestMethod.POST)
	public Map<String, Object> getUserList(Paging paging, UserVO vo) {
		Map<String, Object> map = new HashMap<>() ;

		try {
			map.put("vo", vo) ;
			map.put("list", service.getUserList(paging, vo)) ;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return map ;
	}

	/**
	 * 회원정보 수정
	 */
	@ResponseBody
	@RequestMapping (value = "/setUpdateUserInfo", method = RequestMethod.POST)
	public Map<String, Object> setUpdateUserInfo(HttpServletRequest request, UserVO vo) {
		HttpSession session = request.getSession();
		Map<String, Object> map = new HashMap<String, Object>();

		try {

			service.setUpdateUserInfo(vo);

			map.put("returnCode", 1);
			map.put("returnMessage", "회원정보가  수정되었습니다.");
			session.setAttribute("webSession", service.getWebSessionUserInfo(vo));
		} catch (ServiceException e) {
			map.put("returnCode", - 1);
			map.put("returnMessage", e.getErrorMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return map;
	}

	/**
	 * 회원비밀번호 수정
	 */
	@ResponseBody
	@RequestMapping (value = "/setUpdateUserPw", method = RequestMethod.POST)
	public Map<String, Object> setUpdateUserPw(HttpServletRequest request, UserVO vo) {
		HttpSession session = request.getSession();
		Map<String, Object> map = new HashMap<String, Object>();

		try {
			WebSessionVO loginSession = webSessionFactory.getObject() ;
			vo.setUserNo(loginSession.getUserNo());

			PrivateKey privateKey = (PrivateKey) session.getAttribute("privateKey");
			service.setUpdateUserPw(vo, privateKey);

			map.put("returnCode", 1);
			map.put("returnMessage", "비밀번호가 수정되었습니다.");
		} catch (ServiceException se) {
			map.put("returnCode", - 1);
			map.put("returnMessage", se.getErrorMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return map;
	}

}
