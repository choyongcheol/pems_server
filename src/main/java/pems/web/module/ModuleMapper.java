package pems.web.module;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;
import pems.vo.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ModuleMapper {

	private static final String namespace = "ModuleMap";
	@Resource
	private SqlSession sqlSession;

	public int getModuleCount(ModuleVO vo) throws Exception {
		Map<String, Object> param = new HashMap<>();
		param.put("vo", vo);
		return sqlSession.selectOne(namespace + ".getModuleCount", param);
	}

	public List<ModuleVO> getModuleList(Paging paging, ModuleVO vo) throws Exception {
		Map<String, Object> param = new HashMap<>();
		param.put("vo", vo);
		param.put("paging", paging);
		return sqlSession.selectList(namespace + ".getModuleList", param);
	}

	public int getChannelCount(ChannelVO vo) throws Exception {
		Map<String, Object> param = new HashMap<>();
		param.put("vo", vo);
		return sqlSession.selectOne(namespace + ".getChannelCount", param);
	}

	public List<ChannelVO> getChannelList(Paging paging, ChannelVO vo) throws Exception {
		Map<String, Object> param = new HashMap<>();
		param.put("vo", vo);
		param.put("paging", paging);
		return sqlSession.selectList(namespace + ".getChannelList", param);
	}

	public int getModuleManagerCount(ModuleVO vo) throws Exception {
		Map<String, Object> param = new HashMap<>();
		param.put("vo", vo);
		return sqlSession.selectOne(namespace + ".getModuleManagerCount", param);
	}

	public List<UserVO> getModuleManagerList(Paging paging, ModuleVO vo) throws Exception {
		Map<String, Object> param = new HashMap<>();
		param.put("vo", vo);
		param.put("paging", paging);
		return sqlSession.selectList(namespace + ".getModuleManagerList", param);
	}

	public ModuleVO getModuleInfo(ModuleVO vo) throws Exception {
		Map<String, Object> param = new HashMap<>();
		param.put("vo", vo);
		return sqlSession.selectOne(namespace + ".getModuleInfo", param);
	}

	public List<OvenVO> getOvenList(OvenVO vo) {
		return sqlSession.selectList(namespace + ".getOvenList", vo);
	}

	public List<ChillerVO> getChillerList(ChillerVO vo) {
		return sqlSession.selectList(namespace + ".getChillerList", vo);
	}

	public List<AuxVO> getAuxList(AuxVO vo) {
		return sqlSession.selectList(namespace + ".getAuxList", vo);
	}

	public List<CanVO> getCanList(CanVO vo) {
		return sqlSession.selectList(namespace + ".getCanList", vo);
	}

	public int setInsertModuleManager(List<Map<String, Object>> list) throws Exception {
		return sqlSession.insert(namespace + ".setInsertModuleManager", list);
	}

	public int resetModuleManager(Map<String, String> param) {
		return sqlSession.delete(namespace + ".resetModuleManager", param);
	}
}
