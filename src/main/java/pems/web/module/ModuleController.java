package pems.web.module;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pems.vo.*;
import pems.web.setting.SettingService;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping (value = "/module")
public class ModuleController {

	@Resource
	private ObjectFactory<WebSessionVO> webSessionFactory ;

	@Resource
	private ModuleService service;

	@Resource
	private SettingService settingService;

	/**
	 * 모듈 리스트
	 */
	@RequestMapping (value = "/moduleList", method = RequestMethod.GET)
	public String getModuleList(Paging paging, ModuleVO vo, Model model) {
		String viewPath = "/web/module/moduleList";

		try {
			paging.setRowPerPage(1000);
			model.addAttribute("siteList", settingService.getSiteList(paging, vo));
			model.addAttribute("factoryList", settingService.getFactoryList(paging, vo));
			model.addAttribute("areaList", settingService.getAreaList(paging, vo));

			paging.setRowPerPage(30);
			model.addAttribute("list", service.getModuleList(paging, vo));
		} catch (Exception e) {
			viewPath = "redirect:/";
			e.printStackTrace();
		}

		return viewPath;
	}

	/**
	 * 채널 리스트
	 */
	@ResponseBody
	@RequestMapping (value = "/channelList", method = RequestMethod.POST)
	public Map<String, Object> channelList(Paging paging, ChannelVO vo) {
		Map<String, Object> map = new HashMap<>() ;

		try {

			map.put("vo", vo) ;
			map.put("list", service.getChannelList(paging, vo)) ;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return map ;
	}

	/**
	 * 챔버 리스트
	 */
	@ResponseBody
	@RequestMapping (value = "/getOvenList", method = RequestMethod.POST)
	public Map<String, Object> getOvenList(Paging paging, OvenVO vo) {
		Map<String, Object> map = new HashMap<>() ;

		try {
			map.put("vo", vo) ;
			map.put("list", service.getOvenList(paging, vo)) ;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return map ;
	}

	/**
	 * 칠러 리스트
	 */
	@ResponseBody
	@RequestMapping (value = "/getChillerList", method = RequestMethod.POST)
	public Map<String, Object> getChillerList(Paging paging, ChillerVO vo) {
		Map<String, Object> map = new HashMap<>() ;

		try {
			map.put("vo", vo) ;
			map.put("list", service.getChillerList(paging, vo)) ;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return map ;
	}

	/**
	 * Aux 리스트
	 */
	@ResponseBody
	@RequestMapping (value = "/getAuxList", method = RequestMethod.POST)
	public Map<String, Object> getAuxList(Paging paging, AuxVO vo) {
		Map<String, Object> map = new HashMap<>() ;

		try {
			map.put("vo", vo) ;
			map.put("list", service.getAuxList(paging, vo)) ;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return map ;
	}

	/**
	 * Can 리스트
	 */
	@ResponseBody
	@RequestMapping (value = "/getCanList", method = RequestMethod.POST)
	public Map<String, Object> getCanList(Paging paging, CanVO vo) {
		Map<String, Object> map = new HashMap<>() ;

		try {
			map.put("vo", vo) ;
			map.put("list", service.getCanList(paging, vo)) ;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return map ;
	}

	/**
	 * 모듈 담당자 목록
	 */
	@RequestMapping (value = "/manager", method = RequestMethod.GET)
	public String getModuleManagerList(Paging paging, ModuleVO vo, Model model) {
		String viewPath = "/web/module/manager";

		try {
			paging.setRowPerPage(1000);
			model.addAttribute("siteList", settingService.getSiteList(paging, vo));
			model.addAttribute("factoryList", settingService.getFactoryList(paging, vo));
			model.addAttribute("areaList", settingService.getAreaList(paging, vo));

			paging.setRowPerPage(30);
			model.addAttribute("list", service.getModuleManagerList(paging, vo));
		} catch (Exception e) {
			viewPath = "redirect:/";
			e.printStackTrace();
		}

		return viewPath;
	}


	@ResponseBody
	@RequestMapping (value = "/setInsertModuleManager", method = RequestMethod.POST, consumes = "application/json;")
	public Map<String, Object> setInsertModuleManager(@RequestBody Map<String, String> param) {
		Map<String, Object> map = new HashMap<>() ;

		try {
			WebSessionVO session = webSessionFactory.getObject() ;
			param.put("regUserNo", session.getUserNo()) ;

			service.setInsertModuleManager(param) ;

			map.put("returnCode", "1") ;
			map.put("returnMessage", "저장되었습니다.") ;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return map ;
	}



}
