package pems.web.module;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import pems.vo.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ModuleService {

	@Resource
	private ModuleMapper mapper;

	/**
	 * 모듈 리스트
	 */
	public List<ModuleVO> getModuleList(Paging paging, ModuleVO vo) throws Exception {
		List<ModuleVO> list = new ArrayList<>() ;

		int count = mapper.getModuleCount(vo) ;
		if (count > 0) {
			paging.setTotalCount(count) ;
			list = mapper.getModuleList(paging, vo) ;
		}

		return list ;
	}

	/**
	 * 채널 리스트
	 */
	public List<ChannelVO> getChannelList(Paging paging, ChannelVO vo) throws Exception {
		List<ChannelVO> list = new ArrayList<>() ;

		int count = mapper.getChannelCount(vo) ;
		if (count > 0) {
			paging.setTotalCount(count) ;
			list = mapper.getChannelList(paging, vo) ;
		}

		return list ;
	}

	/**
	 * 모듈 담당자 목록
	 */
	public List<UserVO> getModuleManagerList(Paging paging, ModuleVO vo) throws Exception {
		List<UserVO> list = new ArrayList<>() ;

		int count = mapper.getModuleManagerCount(vo) ;
		if (count > 0) {
			paging.setTotalCount(count) ;
			list = mapper.getModuleManagerList(paging, vo) ;
		}

		return list ;
	}

	/**
	 * 모듈 정보
	 */
	public ModuleVO getModuleInfo(ModuleVO vo) throws Exception {
		return mapper.getModuleInfo(vo) ;
	}

	/**
	 * 챔버 리스트
	 */
	public List<OvenVO> getOvenList(Paging paging, OvenVO vo) throws Exception {
		return mapper.getOvenList(vo) ;
	}

	/**
	 * 칠러 리스트
	 */
	public List<ChillerVO> getChillerList(Paging paging, ChillerVO vo) throws Exception {
		return mapper.getChillerList(vo);
	}

	public List<AuxVO> getAuxList(Paging paging, AuxVO vo) throws Exception {
		return mapper.getAuxList(vo);
	}

	public List<CanVO> getCanList(Paging paging, CanVO vo) throws Exception {
		return mapper.getCanList(vo);
	}

	public void setInsertModuleManager(Map<String, String> param) throws Exception {
		List<Map<String, Object>> list = new ArrayList<>();
		String userNoLsit = param.get("userNoList") ;

//		if (StringUtils.isEmpty(userNoLsit)) {
//			throw new ServiceException(-1, "담당자를 선택해주세요") ;
//		}

		mapper.resetModuleManager(param) ;

		if (!StringUtils.isEmpty(userNoLsit)) {
			for (String userNo : userNoLsit.split(",")) {
				System.out.println("userNo = " + userNo);
				Map<String, Object> map = new HashMap<>();
				map.put("moduleNo", param.get("moduleNo"));
				map.put("regUserNo", param.get("regUserNo"));
				map.put("userNo", userNo);

				list.add(map);
			}
			mapper.setInsertModuleManager(list) ;
		}
	}

}


