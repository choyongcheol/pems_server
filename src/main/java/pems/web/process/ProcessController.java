package pems.web.process;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pems.vo.AgentVO;
import pems.vo.Paging;
import pems.vo.ProcessVO;
import pems.web.etc.EtcService;
import pems.web.setting.SettingService;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping (value = "/process")
public class ProcessController {

	@Resource
	private ProcessService service;

	@Resource
	private SettingService settingService;

	@Resource
	private EtcService etcService;

	/**
	 * 공정 목록
	 */
	@RequestMapping (value = "/processList", method = RequestMethod.GET)
	public String getProcessList(Paging paging, ProcessVO vo, AgentVO aVO, Model model) {
		String viewPath = "/web/process/processList";

		try {
			paging.setRowPerPage(1000);
			model.addAttribute("siteList", settingService.getSiteList(paging, vo));
			model.addAttribute("factoryList", settingService.getFactoryList(paging, vo));
			model.addAttribute("areaList", settingService.getAreaList(paging, vo));
			model.addAttribute("agentList", etcService.getAgentList(paging, aVO));

			paging.setRowPerPage(30);
			model.addAttribute("list", service.getProcessList(paging, vo));
		} catch (Exception e) {
			viewPath = "redirect:/";
			e.printStackTrace();
		}

		return viewPath;
	}

	/**
	 * 공정 채널 리스트
	 */
	@ResponseBody
	@RequestMapping (value = "/processChannelList", method = RequestMethod.POST)
	public Map<String, Object> getProcessChannelList(Paging paging, ProcessVO vo) {
		Map<String, Object> map = new HashMap<>();

		try {
			map.put("vo", vo);
			map.put("list", service.getProcessChannelList(paging, vo));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return map;
	}

	/**
	 * CTS 정보 (header,  record)
	 */
	@ResponseBody
	@RequestMapping (value = "/getCtsData", method = RequestMethod.POST)
	public Map<String, Object> getCtsData(ProcessVO vo) {
		Map<String, Object> map = new HashMap<>();

		try {
			map.put("ctsHeader", service.getCtsHeader(vo));
			map.put("ctsList", service.getCtsRecordList(vo));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}

	/**
	 * CYC 정보(record)
	 */
	@ResponseBody
	@RequestMapping (value = "/getCycRecordList", method = RequestMethod.POST)
	public Map<String, Object> getCycRecordList(Paging paging, ProcessVO vo) {
		Map<String, Object> map = new HashMap<>();

		try {
			map.put("paging", paging);
			map.put("list", service.getCycRecordList(paging, vo));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}


}
