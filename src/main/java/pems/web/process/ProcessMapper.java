package pems.web.process;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;
import pems.vo.Paging;
import pems.vo.ProcessVO;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ProcessMapper {

	private static final String namespace = "ProcessMap";

	@Resource
	private SqlSession sqlSession;

	public ProcessVO getProcessInfo(ProcessVO vo) throws Exception {
		Map<String, Object> param = new HashMap<>();
		param.put("vo", vo);
		return sqlSession.selectOne(namespace + ".getProcessInfo", param);
	}

	public int getProcessCount(ProcessVO vo) throws Exception {
		Map<String, Object> param = new HashMap<>();
		param.put("vo", vo);
		return sqlSession.selectOne(namespace + ".getProcessCount", param);
	}

	public List<ProcessVO> getProcessList(Paging paging, ProcessVO vo) throws Exception {
		Map<String, Object> param = new HashMap<>();
		param.put("vo", vo);
		param.put("paging", paging);
		return sqlSession.selectList(namespace + ".getProcessList", param);
	}

	public List<ProcessVO> getProcessChannelList(ProcessVO vo) {
		return sqlSession.selectList(namespace + ".getProcessChannelList", vo) ;
	}

	public Map<String, Object> getCtsHeader(ProcessVO vo) throws Exception {
		return sqlSession.selectOne(namespace + ".getCtsHeader", vo);
	}

	public List<Map<String, Object>> getCtsRecordList(ProcessVO vo) {
		return sqlSession.selectList(namespace + ".getCtsRecordList", vo) ;
	}

	public int getCycRecordCount(ProcessVO vo) throws Exception {
		Map<String, Object> param = new HashMap<>();
		param.put("vo", vo);
		return sqlSession.selectOne(namespace + ".getCycRecordCount", param);
	}

	public List<Map<String, Object>> getCycRecordList(Paging paging, ProcessVO vo) throws Exception {
		Map<String, Object> param = new HashMap<>();
		param.put("vo", vo);
		param.put("paging", paging);
		return sqlSession.selectList(namespace + ".getCycRecordList", param);
	}

}
