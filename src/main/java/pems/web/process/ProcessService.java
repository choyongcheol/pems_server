package pems.web.process;

import org.springframework.stereotype.Service;
import pems.vo.Paging;
import pems.vo.ProcessVO;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class ProcessService {

	@Resource
	private ProcessMapper mapper;


	/**
	 * 채널 리스트
	 */
	public List<ProcessVO> getProcessList(Paging paging, ProcessVO vo) throws Exception {
		List<ProcessVO> list = new ArrayList<>() ;

		int count = mapper.getProcessCount(vo) ;
		if (count > 0) {
			paging.setTotalCount(count) ;
			list = mapper.getProcessList(paging, vo) ;
		}

		return list ;
	}

	/**
	 * 공정 채널 리스트
	 */
	public List<ProcessVO> getProcessChannelList(Paging paging, ProcessVO vo) throws Exception {
		return mapper.getProcessChannelList(vo) ;
	}


	/**
	 * CTS 헤더
	 */
	public Map<String, Object> getCtsHeader(ProcessVO vo) throws Exception {
		return mapper.getCtsHeader(vo) ;
	}

	/**
	 * CTS 리스트 조회
	 */
	public List<Map<String, Object>> getCtsRecordList(ProcessVO vo) throws Exception {
		return mapper.getCtsRecordList(vo) ;
	}


	public List<Map<String, Object>> getCycRecordList(Paging paging, ProcessVO vo) throws Exception {
		List<Map<String, Object>> list = new ArrayList<>() ;

		int count = mapper.getCycRecordCount(vo) ;
		if (count > 0) {
			paging.setTotalCount(count) ;
			list = mapper.getCycRecordList(paging, vo) ;
		}

		return list ;
	}


}
