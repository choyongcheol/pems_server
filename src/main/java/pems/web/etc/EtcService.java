package pems.web.etc;

import org.springframework.stereotype.Service;
import pems.vo.AgentVO;
import pems.vo.AlarmVO;
import pems.vo.HistoryVO;
import pems.vo.Paging;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class EtcService {

	@Resource
	private EtcMapper mapper;

	/**
	 * 알림 발송이력
	 */
	public List<HistoryVO> getSendAlarmList(Paging paging, HistoryVO vo) throws Exception {
		List<HistoryVO> list = new ArrayList<>();

		int count = mapper.getSendAlarmCount(vo);
		if (count > 0) {
			paging.setTotalCount(count);
			list = mapper.getSendAlarmList(paging, vo);
		}

		return list;
	}

	/**
	 * 에이전트 리스트
	 */
	public List<AgentVO> getAgentList(Paging paging, AgentVO vo) throws Exception {
		List<AgentVO> list = new ArrayList<>();

		int count = mapper.getAgentCount(vo);
		if (count > 0) {
			paging.setTotalCount(count);
			list = mapper.getAgentList(paging, vo);
		}

		return list;
	}

	/**
	 * 에이전트 수정
	 */
	public void setUpdateAgent(AgentVO vo) throws Exception {
		mapper.setUpdateAgent(vo);
	}

	/**
	 * 알람셋 리스트
	 */
	public List<AlarmVO> getAlarmSetList(Paging paging, AlarmVO vo) throws Exception {
		List<AlarmVO> list = new ArrayList<>();

		int count = mapper.getAlarmSetCount(vo);
		if (count > 0) {
			paging.setTotalCount(count);
			list = mapper.getAlarmSetList(paging, vo);
		}

		return list;
	}


}
