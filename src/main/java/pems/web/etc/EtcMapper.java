package pems.web.etc;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;
import pems.vo.AgentVO;
import pems.vo.AlarmVO;
import pems.vo.HistoryVO;
import pems.vo.Paging;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class EtcMapper {

	private static final String namespace = "EtcMap";

	@Resource
	private SqlSession sqlSession;

	public int getSendAlarmCount(HistoryVO vo) throws Exception {
		Map<String, Object> param = new HashMap<>();
		param.put("vo", vo);
		return sqlSession.selectOne(namespace + ".getSendAlarmCount", param);
	}

	public List<HistoryVO> getSendAlarmList(Paging paging, HistoryVO vo) throws Exception {
		Map<String, Object> param = new HashMap<>();
		param.put("vo", vo);
		param.put("paging", paging);
		return sqlSession.selectList(namespace + ".getSendAlarmList", param);
	}

	public int getAgentCount(AgentVO vo) throws Exception {
		Map<String, Object> param = new HashMap<>();
		param.put("vo", vo);
		return sqlSession.selectOne(namespace + ".getAgentCount", param);
	}

	public List<AgentVO> getAgentList(Paging paging, AgentVO vo) throws Exception {
		Map<String, Object> param = new HashMap<>();
		param.put("vo", vo);
		param.put("paging", paging);
		return sqlSession.selectList(namespace + ".getAgentList", param);
	}

	public int setUpdateAgent(AgentVO vo) throws Exception {
		return sqlSession.update(namespace + ".setUpdateAgent", vo);
	}

	public int getAlarmSetCount(AlarmVO vo) throws Exception {
		Map<String, Object> param = new HashMap<>();
		param.put("vo", vo);
		return sqlSession.selectOne(namespace + ".getAlarmSetCount", param);
	}

	public List<AlarmVO> getAlarmSetList(Paging paging, AlarmVO vo) throws Exception {
		Map<String, Object> param = new HashMap<>();
		param.put("vo", vo);
		param.put("paging", paging);
		return sqlSession.selectList(namespace + ".getAlarmSetList", param);
	}


}
