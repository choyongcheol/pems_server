package pems.web.etc;

import framework.exception.ServiceException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pems.vo.AgentVO;
import pems.vo.AlarmVO;
import pems.vo.HistoryVO;
import pems.vo.Paging;
import pems.web.setting.SettingService;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping (value = "/etc")
public class EtcController {

	@Resource
	private EtcService service;

	@Resource
	private SettingService settingService;

	/**
	 * 알람 발송이력
	 */
	@RequestMapping (value = "/sendAlarmList", method = RequestMethod.GET)
	public String getSendAlarmList(Paging paging, HistoryVO vo, AgentVO aVO, Model model) {
		String viewPath = "/web/etc/sendAlarmList";

		try {
			paging.setRowPerPage(1000);
			model.addAttribute("siteList", settingService.getSiteList(paging, vo));
			model.addAttribute("factoryList", settingService.getFactoryList(paging, vo));
			model.addAttribute("areaList", settingService.getAreaList(paging, vo));
			model.addAttribute("agentList", service.getAgentList(paging, aVO));

			paging.setRowPerPage(30);
			model.addAttribute("list", service.getSendAlarmList(paging, vo));

		} catch (Exception e) {
			viewPath = "redirect:/";
			e.printStackTrace();
		}

		return viewPath;
	}

	/**
	 * 에이전트 리스트
	 */
	@RequestMapping (value = "/agentList", method = RequestMethod.GET)
	public String getCityList(Paging paging, AgentVO vo, Model model) {
		String viewPath = "/web/etc/agentList";

		try {
			model.addAttribute("list", service.getAgentList(paging, vo));
		} catch (Exception e) {
			viewPath = "redirect:/";
			e.printStackTrace();
		}

		return viewPath;
	}

	/**
	 * 에이전트 수정
	 */
	@ResponseBody
	@RequestMapping (value = "/setUpdateAgent", method = RequestMethod.POST)
	public Map<String, Object> setUpdateAgent(AgentVO vo) {
		Map<String, Object> map = new HashMap<String, Object>();

		try {
			service.setUpdateAgent(vo);

			map.put("returnCode", 1);
			map.put("returnMessage", "수정되었습니다.");
		} catch (ServiceException se) {
			map.put("returnCode", se.getErrorCode());
			map.put("returnMessage", se.getErrorMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return map;
	}

	/**
	 * 알람셋 리스트
	 */
	@RequestMapping (value = "/alarmSetList", method = RequestMethod.GET)
	public String getAlarmSetList(Paging paging, AlarmVO vo, Model model) {
		String viewPath = "/web/etc/alarmSetList";

		try {
			model.addAttribute("list", service.getAlarmSetList(paging, vo));
		} catch (Exception e) {
			viewPath = "redirect:/";
			e.printStackTrace();
		}

		return viewPath;
	}

	/**
	 * 알람셋 리스트 (selectbox)
	 */
	@ResponseBody
	@RequestMapping (value = "/alarmSetList", method = RequestMethod.POST)
	public Map<String, Object> getAlarmSetList(Paging paging, AlarmVO vo) {
		Map<String, Object> map = new HashMap<>();

		try {

			map.put("vo", vo);
			map.put("list", service.getAlarmSetList(paging, vo));

		} catch (Exception e) {
			e.printStackTrace();
		}

		return map;
	}

}
