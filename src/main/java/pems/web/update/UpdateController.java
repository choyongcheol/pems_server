package pems.web.update;

import framework.exception.ServiceException;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pems.vo.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping (value = "/update")
public class UpdateController {

	@Resource
	private ObjectFactory<WebSessionVO> webSessionFactory ;

	@Resource
	private UpdateService service;


	/**
	 * 업데이트 파일리스트
	 */
	@RequestMapping (value = "/updateFileList", method = RequestMethod.GET)
	public String getSbcFileList(Paging paging, UpdateFileVO vo, Model model) {
		String viewPath = "/web/update/updateFileList";

		try {
			model.addAttribute("vo", vo);
			model.addAttribute("list", service.getUpdateFileList(paging, vo));
		} catch (Exception e) {
			viewPath = "redirect:/";
			e.printStackTrace();
		}

		return viewPath;
	}

	/**
	 * 업데이트 파일 업로드
	 */
	@ResponseBody
	@RequestMapping(value = "/fileUpload", method = RequestMethod.POST)
	public Map<String, Object> setUpdateFileUpload(HttpServletRequest request, UpdateFileVO vo) {
		Map<String, Object> map = new HashMap<>() ;

		try {
			WebSessionVO session = webSessionFactory.getObject() ;
			vo.setUserNo(session.getUserNo());

			service.setUpdateFileUpload(request, vo) ;

			map.put("returnCode", 1) ;
			map.put("returnMessage", "업로드 되었습니다.") ;
		} catch (ServiceException e) {
			map.put("returnCode", e.getErrorCode()) ;
			map.put("returnMessage", e.getErrorMessage()) ;
		} catch (Exception e) {
			e.printStackTrace() ;
		}

		return map ;
	}


}
