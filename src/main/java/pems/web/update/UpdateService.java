package pems.web.update;

import framework.exception.ServiceException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import pems.Constants;
import pems.common.CommonMapper;
import pems.utils.PemsFileUtils;
import pems.vo.FileInfo;
import pems.vo.ModuleVO;
import pems.vo.Paging;
import pems.vo.UpdateFileVO;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class UpdateService {

	@Resource
	private UpdateMapper mapper;

	@Resource
	private CommonMapper commonMapper;


	public List<UpdateFileVO> getUpdateFileList(Paging paging, UpdateFileVO vo) throws Exception {
		List<UpdateFileVO> list = new ArrayList<>() ;

		if (StringUtils.isEmpty(vo.getFileType())) {
			throw new ServiceException(-1, "필수 파라미터 누락(FileType)") ;
		}

		int count = mapper.getUpdateFileCount(vo) ;
		if (count > 0) {
			paging.setTotalCount(count) ;
			list = mapper.getUpdateFileList(paging, vo) ;
		}

		return list ;
	}


	public void setUpdateFileUpload(HttpServletRequest request, UpdateFileVO vo) throws Exception {
		String path = Constants.UPDATE_FILE_PATH ;

		if (StringUtils.isEmpty(vo.getFileType()) || StringUtils.isEmpty(vo.getUserNo())) {
			System.out.println("UpdateFileVO = " + vo);
			throw new ServiceException(-1, "필수 파라미터 누락") ;
		}

		FileInfo fi = PemsFileUtils.createTempFile(request, path) ;

		mapper.setInsertUpdateFileInfo(vo, fi) ;

	}


}
