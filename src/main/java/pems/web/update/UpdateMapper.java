package pems.web.update;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;
import pems.vo.FileInfo;
import pems.vo.ModuleVO;
import pems.vo.Paging;
import pems.vo.UpdateFileVO;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class UpdateMapper {

	private static final String namespace = "UpdateMap";
	@Resource
	private SqlSession sqlSession;


	public int getUpdateFileCount(UpdateFileVO vo) throws Exception {
		Map<String, Object> param = new HashMap<>();
		param.put("vo", vo);
		return sqlSession.selectOne(namespace + ".getUpdateFileCount", param);
	}

	public List<UpdateFileVO> getUpdateFileList(Paging paging, UpdateFileVO vo) throws Exception {
		Map<String, Object> param = new HashMap<>();
		param.put("vo", vo);
		param.put("paging", paging);
		return sqlSession.selectList(namespace + ".getUpdateFileList", param);
	}

	public int setInsertUpdateFileInfo(UpdateFileVO vo , FileInfo fi) throws Exception {
		Map<String, Object> param = new HashMap<>();
		param.put("fv", vo);
		param.put("fi", fi);
		return sqlSession.insert(namespace + ".setInsertUpdateFileInfo", param) ;
	}


}
