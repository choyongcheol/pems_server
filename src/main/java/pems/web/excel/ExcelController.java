package pems.web.excel;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import pems.Constants;
import pems.web.process.ProcessService;
import pems.vo.ProcessVO;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping (value = "/excel")
public class ExcelController {

	@Resource
	private ExcelService service;

	@Resource
	private ProcessService processService;


	@RequestMapping (value = "/ctsDownload", method = RequestMethod.GET)
	public ModelAndView excelCtsDownload(Model model, ProcessVO vo) {
		Map<String, Object> map = new HashMap<>() ;

		try {
			Map<String, Object> header = processService.getCtsHeader(vo) ;

			model.addAttribute("excelFileName", "CTS_RECORD" + header.get("file_name")) ;
			model.addAttribute("colName", Arrays.asList(Constants.EXCEL_HEADER_NAME_CTS_RECORD)) ;
			model.addAttribute("colValue", processService.getCtsRecordList(vo)) ;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return new ModelAndView("excelDownloadView");
	}


}
