package pems.web.main;

import framework.exception.ServiceException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pems.vo.Paging;
import pems.vo.MainVO;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
//@RequestMapping(value = "/web")
public class MainController {

	@Resource
	private MainService service ;

	// 메인화면
	@RequestMapping(value = { "/dashboard" }, method = RequestMethod.GET)
	public String main(HttpServletRequest request, Paging paging, Model model, MainVO vo) {
		try {

			Map<String, Object> map = service.getMainData(paging, vo) ;

			model.addAttribute("bestPartnerList", "안녕 ? ") ;

		} catch (ServiceException se) {
			se.printStackTrace() ;
		} catch (Exception e) {
			e.printStackTrace() ;
		}

		return "/web/dashboard" ;
	}


}
