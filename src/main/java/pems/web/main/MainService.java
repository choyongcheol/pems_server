package pems.web.main;

import org.springframework.stereotype.Service;
import pems.common.CommonMapper;
import pems.vo.Paging;
import pems.vo.MainVO;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Service
public class MainService {

	@Resource
	private MainMapper mapper;

	@Resource
	private CommonMapper commonMapper;


	public Map<String, Object> getMainData(Paging paging, MainVO vo) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();

		// 추천상품
		map.put("1", "1");
		map.put("2", "2");

		return map;
	}

}
