package pems.web.login;

import framework.exception.ServiceException;
import framework.session.EgovHttpSessionBindingListener;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import pems.utils.RSA;
import pems.vo.UserVO;
import pems.vo.SignVO;
import pems.vo.WebSessionVO;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.security.InvalidKeyException;
import java.security.PrivateKey;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping (value = "/login")
public class LoginController {

	@Resource
	private LoginService service;

	/**
	 * 로그인 화면
	 */
	@RequestMapping (value = "/login", method = RequestMethod.GET)
	public String login(HttpServletRequest request) {
		HttpSession session = request.getSession();

		try {
			session.setAttribute("rurl", request.getAttribute("rurl"));

			// 암호화 키생성
			RSA.getRsaKey(request);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "/login";
	}

	/**
	 * 로그인
	 */
	@ResponseBody
	@RequestMapping (value = "/login", method = RequestMethod.POST)
	public Map<String, Object> login(HttpServletRequest request, UserVO vo) {
		HttpSession session = request.getSession();
		Map<String, Object> map = new HashMap<String, Object>();
		EgovHttpSessionBindingListener listener = new EgovHttpSessionBindingListener();

		try {
			String rurl = StringUtils.isEmpty(session.getAttribute("rurl")) ? "/" : String.valueOf(session.getAttribute("rurl"));
			PrivateKey privateKey = (PrivateKey) session.getAttribute("privateKey");
			WebSessionVO webSession = service.getWebSessionUserInfo(vo, privateKey);

			if (! StringUtils.isEmpty(webSession)) {
				session.setAttribute("webSession", webSession);
				session.setAttribute(webSession.getUserId() + webSession.getUserNo(), listener);

				map.put("rurl", rurl);
				map.put("returnCode", 1);
				map.put("returnMessage", "로그인 성공");
			}
		} catch (InvalidKeyException ie) {
			map.put("returnCode", - 1000);
			map.put("returnMessage", "");
		} catch (ServiceException e) {
			map.put("returnCode", - 1);
			map.put("returnMessage", e.getErrorMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return map;
	}

	/**
	 * 로그아웃
	 */
	@ResponseBody
	@RequestMapping (value = "/logout", method = RequestMethod.POST)
	public Map<String, Object> logout(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<>();

		try {
			map.put("returnCode", 1);
			WebSessionVO session = (WebSessionVO) request.getSession().getAttribute("webSession");
			RequestContextHolder.getRequestAttributes().removeAttribute(session.getUserId(), RequestAttributes.SCOPE_SESSION);
			request.getSession().invalidate();
		} catch (Exception e) {
			e.printStackTrace();
		}


		return map;
	}

	/**
	 * 인증번호 발송
	 */
	@ResponseBody
	@RequestMapping (value = "/sendValidationCode", method = RequestMethod.POST)
	public Map<String, Object> searchPwd(UserVO vo) {
		Map<String, Object> map = new HashMap<String, Object>();

		try {
			map.put("returnCode", 1);
			map.put("returnMessage", "인증번호를 전송하였습니다.");
			map.put("validationCode", service.sendValidationCode(vo));
		} catch (ServiceException se) {
			map.put("returnCode", se.getErrorCode());
			map.put("returnMessage", se.getErrorMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return map;
	}

	/**
	 * 인증번호 확인
	 */
	@ResponseBody
	@RequestMapping (value = "/confirmVerificationCode", method = RequestMethod.POST)
	public Map<String, Object> confirmVerificationCode(UserVO vo) {
		Map<String, Object> map = new HashMap<String, Object>();

		try {
			map.put("returnCode", 1);
			map.put("returnMessage", "인증되었습니다.");
			map.put("vo", service.confirmVerificationCode(vo));
		} catch (ServiceException e) {
			map.put("returnCode", - 1);
			map.put("returnMessage", e.getErrorMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return map;
	}

	/**
	 * 비밀번호 재설정
	 */
	@ResponseBody
	@RequestMapping (value = "/resetPassword", method = RequestMethod.POST)
	public Map<String, Object> resetPassword(UserVO vo) {
		Map<String, Object> map = new HashMap<String, Object>();

		try {

			service.resetPassword(vo) ;

			map.put("returnCode", 1);
			map.put("returnMessage", "비밀번호를 재설정 했습니다.");
		} catch (ServiceException e) {
			map.put("returnCode", - 1);
			map.put("returnMessage", e.getErrorMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return map;
	}

	/**
	 * 아이디 중복체크
	 */
	@ResponseBody
	@RequestMapping (value = "/duplicateId", method = RequestMethod.POST)
	public Map<String, Object> duplicateId(SignVO vo) {
		Map<String, Object> map = new HashMap<String, Object>();

		try {
			service.checkUserDuplicateSignId(vo);

			map.put("returnCode", 1);
			map.put("returnMessage", "사용 가능한 아이디 입니다.");
		} catch (ServiceException e) {
			map.put("returnCode", - 1);
			map.put("returnMessage", e.getErrorMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return map;
	}













	/**
	 * 회원 가입
	 */
	@ResponseBody
	@RequestMapping (value = "/signup", method = RequestMethod.POST)
	public Map<String, Object> signup(HttpServletRequest request, UserVO uVO) {
		HttpSession session = request.getSession();
		Map<String, Object> map = new HashMap<String, Object>();

		try {
			PrivateKey privateKey = (PrivateKey) session.getAttribute("privateKey");

			service.setSignup(uVO, privateKey);

			map.put("returnCode", 1);
			map.put("returnMessage", "가입되었습니다.");
			map.put("url", "/login/login");
		} catch (ServiceException e) {
			map.put("returnCode", - 1);
			map.put("returnMessage", e.getErrorMessage());
			map.put("url", "/");
		} catch (Exception e) {
			e.printStackTrace();
		}

		return map;
	}


}
