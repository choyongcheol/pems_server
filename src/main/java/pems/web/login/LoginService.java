package pems.web.login;

import framework.exception.ServiceException;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import pems.utils.RSA;
import pems.utils.SmsUtil;
import pems.utils.Utils;
import pems.vo.UserVO;
import pems.vo.SignVO;
import pems.vo.WebSessionVO;

import javax.annotation.Resource;
import java.security.PrivateKey;
import java.util.Random;

@Service
public class LoginService {

	@Resource
	private LoginMapper dao;

	/**
	 * 인증번호 발송
	 */
	public String sendValidationCode(UserVO vo) throws Exception {
		Random ran = new Random();
		UserVO userInfo = dao.getUserInfo(vo) ;

		if (StringUtils.isEmpty(userInfo)) {
			throw new ServiceException(-1, "회원정보가 없습니다.") ;
		}

		int number = ran.nextInt(999999);
		String digital = String.format("%06d", number);
		String validationCode = Utils.getRandomHex(10) ;
		String message = "[" + digital + "] 사용자 인증을 위한 인증번호입니다.";

		SmsUtil.toSendSMS(userInfo.getDecryptedPhoneNo(), message);
		userInfo.setVerificationCode(digital);
		userInfo.setValidationCode(validationCode);

		dao.setVerificationCode(userInfo) ;

		return validationCode ;
	}

	/**
	 * 인증번호 확인
	 */
	public UserVO confirmVerificationCode(UserVO vo) throws Exception {
		UserVO userInfo = dao.getUserInfo(vo) ;

		if (StringUtils.isEmpty(userInfo)) {
			throw new ServiceException(-1, "올바른 인증번호를 입력해주세요.") ;
		}

		return userInfo ;
	}

	/**
	 * 비밀번호 재설정
	 */
	public void resetPassword(UserVO vo) throws Exception {
		UserVO userInfo = dao.getUserInfo(vo) ;

		if (StringUtils.isEmpty(userInfo)) {
			throw new ServiceException(-1, "올바른 인증번호를 입력해주세요.") ;
		}

		Random ran = new Random();
		int number = ran.nextInt(99999999);
		String tempPw = String.format("%08d", number);
		String message = "재설정된 임시 비밀번호는 [" + tempPw + "] 입니다.";
		SmsUtil.toSendSMS(userInfo.getDecryptedPhoneNo(), message);

		vo.setSignPassword(tempPw);
		dao.setUpdateTempPassword(vo) ;
	}


	/**
	 * 유저 회원가입
	 */
	public void setSignup(UserVO vo, PrivateKey privateKey) throws Exception {
		UserVO userVO = dao.getUserInfo(vo);

		if (! StringUtils.isEmpty(userVO)) {
			throw new ServiceException(- 1, "이미 사용중인 아이디 입니다.");
		}

		vo.setSignPassword(RSA.decryptRsa(privateKey, vo.getSignPassword()));

		dao.setInsertUserInfo(vo);

		// throw new ServiceException(-1, "유저 회원가입") ;
	}


	/**
	 * 회원 로그인
	 */
	public WebSessionVO getWebSessionUserInfo(UserVO vo, PrivateKey privateKey) throws Exception {
		WebSessionVO sessionInfo = dao.getWebSessionUserInfo(vo);

		if (StringUtils.isEmpty(sessionInfo)) {
			throw new ServiceException(- 1, "등록되지 않은 아이디입니다.");
		}

		if (! BCrypt.checkpw(RSA.decryptRsa(privateKey, vo.getSignPassword()), sessionInfo.getPassword())) {
			throw new ServiceException("아이디 또는 비밀번호가 틀렸습니다.");
		}

		return sessionInfo;
	}

	/**
	 * 사용자 이름 중복체크
	 */
	public void checkUserDuplicateSignId(SignVO vo) throws Exception {
		UserVO uVO = dao.checkUserDuplicateSignId(vo);

		if (! StringUtils.isEmpty(uVO)) {
			throw new ServiceException(- 1, "이미 사용중인 아이디 입니다.");
		}
	}


	/**
	 * 세션아이디 세팅
	 private AdminVO setUniqueSessionId(AdminVO adminVO) {
	 String uniqueSessionId = adminVO.getUniqueSessionId() ;

	 if (!Utils.isEmpty(uniqueSessionId)) {
	 uniqueSessionId = uniqueSessionId.substring(0, 19) ;
	 adminVO.setUniqueSessionId(uniqueSessionId) ;
	 }

	 return adminVO ;
	 }
	 */

}
