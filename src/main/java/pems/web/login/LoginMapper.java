package pems.web.login;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;
import pems.vo.UserVO;
import pems.vo.SignVO;
import pems.vo.WebSessionVO;

import javax.annotation.Resource;

@Repository
public class LoginMapper {

	private static final String namespace = "LoginMap";

	@Resource
	private SqlSession sqlSession;

	/**
	 * 회원정보 조회
	 */
	public WebSessionVO getWebSessionUserInfo(UserVO vo) {
		return sqlSession.selectOne(namespace + ".getWebSessionUserInfo", vo);
	}

	public int setVerificationCode(UserVO vo) {
		return sqlSession.update(namespace + ".setVerificationCode", vo) ;
	}

	// 회원정보 조회
	public UserVO getUserInfo(UserVO vo) {
		return sqlSession.selectOne(namespace + ".getUserInfo", vo);
	}



	/**
	 * 회원가입
	 */
	public int setInsertUserInfo(UserVO vo) {
		return sqlSession.insert(namespace + ".setInsertUserInfo", vo);
	}

	/**
	 * 회원 임시비밀번호 적용
	 */
	public int setUpdateTempPassword(UserVO vo) {
		return sqlSession.update(namespace + ".setUpdateTempPassword", vo);
	}

	/**
	 * 회원 아이디 중복체크
	 */
	public UserVO checkUserDuplicateSignId(SignVO vo) {
		return sqlSession.selectOne(namespace + ".checkUserDuplicateSignId", vo);
	}

}
