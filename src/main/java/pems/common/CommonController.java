package pems.common;

import framework.exception.ServiceException;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pems.vo.ColumnVO;
import pems.vo.FileInfo;
import pems.vo.Paging;
import pems.vo.WebSessionVO;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping(value = "/common")
public class CommonController {

	@Resource
	private ObjectFactory<WebSessionVO> webSessionFactory ;

	@Resource
	private CommonService service ;


	/**
	 * 유저별 테이블 필드설정 조회
	 */
	@ResponseBody
	@RequestMapping (value = "/getUserColumnList", method = RequestMethod.POST)
	public Map<String, Object> getUserColumnList(Paging paging, ColumnVO vo) {
		Map<String, Object> map = new HashMap<>() ;

		try {
			WebSessionVO session = webSessionFactory.getObject() ;
			if (!StringUtils.isEmpty(session) || !StringUtils.isEmpty(session.getUserNo())) {
				vo.setUserNo(session.getUserNo());

				map.put("vo", vo) ;
				map.put("column", service.getUserColumnList(vo)) ;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return map ;
	}

	/**
	 * 유저별 테이블 필드설정 저장
	 */
	@ResponseBody
	@RequestMapping (value = "/setUpdateUserColumns", method = RequestMethod.POST)
	public Map<String, Object> setUpdateUserColumns(ColumnVO vo) {
		Map<String, Object> map = new HashMap<>() ;

		try {
			WebSessionVO session = webSessionFactory.getObject() ;
			vo.setUserNo(session.getUserNo());
			service.setUpdateUserColumns(vo) ;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return map ;
	}













	@ResponseBody
	@RequestMapping(value = "/fileUpload", method = RequestMethod.POST)
	public Map<String, Object> fileUpload(HttpServletRequest request) throws Exception {
		Map<String, Object> map = new HashMap<>() ;
		try {

			map.put("returnCode", 1) ;
			map.put("returnMessage", "성공") ;
			map.put("fileList", service.fileUploads(request)) ;

		} catch (ServiceException e) {
			map.put("returnCode", e.getErrorCode()) ;
			map.put("returnMessage", e.getErrorMessage()) ;
		} catch (Exception e) {
			e.printStackTrace() ;
		}

		return map ;
	}

	/**
	 * 파일 다운로드
	 *
	 * @param res
	 * @param vo
	 */
	@RequestMapping(value = "/fileDownload", method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public void fileDownload(HttpServletRequest req, HttpServletResponse res, FileInfo vo) {
		FileInputStream fis = null ;
		ServletOutputStream sos = null ;

		try {
			String agent = req.getHeader("User-Agent") ;
			FileInfo fi = service.getFileInfo(vo) ;

			String filePath = fi.getPath() + fi.getUploaded() ;
			File file = new File(filePath) ;
			fis = new FileInputStream(file) ;
			String originFileName = URLDecoder.decode(fi.getOriginal(), "UTF-8") ;

			if (agent.indexOf("MSIE") > -1 || agent.indexOf("Trident") > -1 || agent.indexOf("Edge") > -1) {
				originFileName = URLEncoder.encode(originFileName, "UTF-8").replaceAll("\\+", "%20") ;
			} else if (agent.indexOf("Chrome") > -1) {
				originFileName = new String(originFileName.getBytes("UTF-8"), "ISO-8859-1") ;
			} else if (agent.indexOf("Opera") > -1) {} else if (agent.indexOf("Safari") > -1 || agent.indexOf("Firefox") > -1) {} else {
				originFileName = null ;
			}

			res.setContentType("application/octet-stream") ;
			res.setContentLength((int) file.length()) ;
			res.setHeader("Content-Disposition", "attachment; filename=" + originFileName) ;

			sos = res.getOutputStream() ;
			byte[] buffer = new byte[4096] ;
			int bytesRead = -1 ;

			while((bytesRead = fis.read(buffer)) != -1) {
				sos.write(buffer, 0, bytesRead) ;
			}

		} catch (Exception e) {
			e.printStackTrace() ;
		} finally {
			try {
				if (!StringUtils.isEmpty(fis)) fis.close() ;
				if (!StringUtils.isEmpty(sos)) sos.close() ;
			} catch (IOException e) {
				e.printStackTrace() ;
			}
		}

	}

}
