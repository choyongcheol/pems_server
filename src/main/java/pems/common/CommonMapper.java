package pems.common;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;
import pems.vo.ColumnVO;
import pems.vo.FileInfo;

import javax.annotation.Resource;
import java.util.List;

@Repository
public class CommonMapper {

	@Resource
	private SqlSession sqlSession ;

	private static final String namespace = "CommonMap" ;

	public ColumnVO getUserColumnList(ColumnVO vo) throws Exception {
		return sqlSession.selectOne(namespace + ".getUserColumnList", vo) ;
	}

	public int setInsertUserColumn(ColumnVO vo) throws Exception {
		return sqlSession.insert(namespace + ".setInsertUserColumn", vo) ;
	}

	public int setUpdateUserColumns(ColumnVO vo) throws Exception {
		return sqlSession.update(namespace + ".setUpdateUserColumns", vo) ;
	}




	public int setInsertEstimateFileInfo(FileInfo vo) throws Exception {
		return sqlSession.insert(namespace + ".setInsertEstimateFileInfo", vo) ;
	}

	public FileInfo getFileInfo(FileInfo vo) throws Exception {
		return sqlSession.selectOne(namespace + ".getFileInfo", vo) ;
	}


}
