package pems.common;

import org.springframework.util.StringUtils;
import pems.Constants;
import org.springframework.stereotype.Service;
import pems.utils.PemsFileUtils;
import pems.vo.ColumnVO;
import pems.vo.FileInfo;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
public class CommonService {

	@Resource
	private CommonMapper mapper;


	/**
	 * 유저별 테이블 필드설정 조회
	 */
	public ColumnVO getUserColumnList(ColumnVO vo) throws Exception {
		ColumnVO retVO = mapper.getUserColumnList(vo) ;

		if (StringUtils.isEmpty(retVO)) {
			if ("channelInfo".equals(vo.getTableName())) {
				vo.setColumnId(Constants.USER_COLUMN_CHANNEL_ID);
				vo.setColumnName(Constants.USER_COLUMN_CHANNEL_NAME);
				vo.setColumnVisible(Constants.USER_COLUMN_CHANNEL_VISIBLE);
			} else if ("cycrecord".equals(vo.getTableName())) {
				vo.setColumnId(Constants.USER_COLUMN_CYC_ID);
				vo.setColumnName(Constants.USER_COLUMN_CYC_NAME);
				vo.setColumnVisible(Constants.USER_COLUMN_CYC_VISIBLE);
			}
			mapper.setInsertUserColumn(vo) ;
			retVO = mapper.getUserColumnList(vo) ;
		}

		return retVO ;
	}











	public List<FileInfo> fileUploads(HttpServletRequest request) throws Exception {
		String path = Constants.TEMP_FILE_PATH ;
		List<FileInfo> list = PemsFileUtils.createTempFiles(request, path) ;

		for (FileInfo fi : list) {
			mapper.setInsertEstimateFileInfo(fi) ;
		}

		return list ;
	}

	public FileInfo getFileInfo(FileInfo vo) throws Exception {
		return mapper.getFileInfo(vo) ;
	}


	public void setUpdateUserColumns(ColumnVO vo) throws Exception {
		mapper.setUpdateUserColumns(vo) ;
	}
}
