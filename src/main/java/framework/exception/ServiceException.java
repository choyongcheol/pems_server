package framework.exception ;

import lombok.Data ;
import lombok.EqualsAndHashCode ;

@Data
@EqualsAndHashCode(callSuper = false)
public class ServiceException extends Exception {

	private static final long serialVersionUID = -3701106564447708629L ;

	/* errorCode */
	private Integer errorCode ;

	/* errorMessage */
	private String errorMessage ;

	public ServiceException() {
		super() ;
	}

	public ServiceException(Integer errorCode) {
		super() ;
		this.errorCode = errorCode ;
	}

	public ServiceException(String errorMessage) {
		super() ;
		this.errorMessage = errorMessage ;
	}

	public ServiceException(Integer errorCode, String errorMessage) {
		super() ;
		this.errorCode = errorCode ;
		this.errorMessage = errorMessage ;
	}
}
