package framework.interceptor;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import pems.utils.RSA;
import pems.vo.WebSessionVO;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class WebSessionInterceptor implements HandlerInterceptor {

	@Resource
	private ObjectFactory<WebSessionVO> sessionFactory;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		System.out.println("WebSessionInterceptor");
		WebSessionVO session = sessionFactory.getObject();

		if (StringUtils.isEmpty(session) || StringUtils.isEmpty(session.getUserNo())) {
			System.out.println(" ! isEmpty(session) ! ");
			request.setAttribute("rurl", request.getRequestURI() + "?" + request.getQueryString());
			RequestDispatcher dispatcher = request.getRequestDispatcher("/login/login");
			dispatcher.forward(request, response);
			return false;
		}

		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
//		RSA.getRsaKey(request);
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
	}

}
