package framework.session ;

import javax.servlet.http.HttpServletRequest ;
import javax.servlet.http.HttpSession ;
import javax.servlet.http.HttpSessionEvent ;
import javax.servlet.http.HttpSessionListener ;

import org.apache.ibatis.session.SqlSession ;
import org.springframework.util.StringUtils ;
import org.springframework.web.context.WebApplicationContext ;
import org.springframework.web.context.request.RequestContextHolder ;
import org.springframework.web.context.request.ServletRequestAttributes ;
import org.springframework.web.context.support.WebApplicationContextUtils ;
import pems.vo.LogVO;

public class SessionListener implements HttpSessionListener {

	@Override
	public void sessionCreated(HttpSessionEvent se) {

		HttpSession session = se.getSession() ;

		if (session.isNew()) {
			WebApplicationContext wac = WebApplicationContextUtils.getRequiredWebApplicationContext(session.getServletContext()) ;
			HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest() ;
			SqlSession sqlSession = (SqlSession) wac.getBean("sqlSession") ;
			String userAgent = req.getHeader("User-Agent") ;
			String referer = req.getHeader("referer") ;
			String refererStr = String.valueOf(referer) ;

			if (!StringUtils.isEmpty(referer) && !(refererStr.indexOf("localhost") > -1) && !(refererStr.indexOf("null") > -1) && !(userAgent.indexOf("Java") > -1)) {
				LogVO log = new LogVO() ;

				log.setUserAgent(userAgent);
				log.setReferer(referer) ;
				log.setOs(getUserOs(userAgent)) ;
				log.setBrowser(getBrowserInfo(userAgent)) ;
				log.setIp(getUserIp(req));
				log.setDisplayLanguage(req.getLocale().getDisplayLanguage());

				sqlSession.insert("CommonMap.setInsertAccessUserLog", log) ;
			}
		}

	}

	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
	}

	// 사용자 브라우저 구하기
	protected String getBrowserInfo(String userAgent) {
		String browser = "" ;

		try {
			if (userAgent != null) {
				if (userAgent.indexOf("Trident") > -1) {
					browser = "MSIE" ;
				} else if (userAgent.indexOf("Chrome") > -1) {
					browser = "Chrome" ;
				} else if (userAgent.indexOf("Opera") > -1) {
					browser = "Opera" ;
				} else if (userAgent.indexOf("iPhone") > -1 && userAgent.indexOf("Mobile") > -1) {
					browser = "iPhone" ;
				} else if (userAgent.indexOf("Android") > -1 && userAgent.indexOf("Mobile") > -1) {
					browser = "Android" ;
				}
			}
		} catch (Exception e) {
			e.printStackTrace() ;
		}

		return browser ;
	}

	private String getUserIp(HttpServletRequest request) {
		
        String ip = request.getHeader("X-Forwarded-For");
 
        if (ip == null) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null) {
            ip = request.getHeader("WL-Proxy-Client-IP"); // 웹로직
        }
        if (ip == null) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }
        if (ip == null) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (ip == null) {
            ip = request.getRemoteAddr();
        }
        
        return ip;
 
	}
	
	// 사용자 IP 구하기
	/*
	protected String getUserIp(HttpServletRequest req) {
		String retIp = "" ;

		try {
			String clientIp = req.getHeader("HTTP_X_FORWARDED_FOR") ;
			if (null == clientIp || clientIp.length() == 0 || clientIp.toLowerCase().equals("unknown")) {
				retIp = req.getHeader("REMOTE_ADDR") ;
			}
			if (null == clientIp || clientIp.length() == 0 || clientIp.toLowerCase().equals("unknown")) {
				retIp = req.getRemoteAddr() ;
			}
		} catch (Exception e) {
			e.printStackTrace() ;
		}

		return retIp ;
	}
	*/
	
	protected String getUserOs(String userAgent) {
		String os ;
		if(userAgent.indexOf("NT 6.0") != -1) os = "Windows Vista/Server 2008";
		else if(userAgent.indexOf("NT 5.2") != -1) os = "Windows Server 2003";
		else if(userAgent.indexOf("NT 5.1") != -1) os = "Windows XP";
		else if(userAgent.indexOf("NT 5.0") != -1) os = "Windows 2000";
		else if(userAgent.indexOf("NT 10.0") != -1) os = "Windows 10";
		else if(userAgent.indexOf("NT") != -1) os = "Windows NT";
		else if(userAgent.indexOf("9x 4.90") != -1) os = "Windows Me";
		else if(userAgent.indexOf("98") != -1) os = "Windows 98";
		else if(userAgent.indexOf("95") != -1) os = "Windows 95";
		else if(userAgent.indexOf("Win16") != -1) os = "Windows 3.x";
		else if(userAgent.indexOf("Windows") != -1) os = "Windows";
		else if(userAgent.indexOf("Linux") != -1) os = "Linux";
		else if(userAgent.indexOf("Macintosh") != -1) os = "Macintosh";
		else os = ""; 

		return os ;
	}

}
