<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>

<div class="table-btn">
	<div class="paging">
		<c:if test="${paging.page > 1}">
			<a href="<my:replaceParam name='page' value='1' />" >처음</a>
		</c:if>

		<c:if test="${paging.prevBlock > 0}">
			<a href="<my:replaceParam name='page' value='${paging.prevBlock }' />">&lt;</a>
		</c:if>

		<c:forEach var="i" begin="${paging.startBlock}" end="${paging.endBlock }">
			<c:choose>
				<c:when test="${i eq paging.page}">
					<a href="javascript:void(0);" class="active">${i }</a>
				</c:when>
				<c:otherwise>
					<a href="<my:replaceParam name='page' value='${i }' />">${i }</a>
				</c:otherwise>
			</c:choose>
		</c:forEach>

		<c:if test="${paging.nextBlock > 0}">
			<a href="<my:replaceParam name='page' value='${paging.nextBlock }' />">&gt</a>
		</c:if>

		<c:if test="${paging.page < paging.totalPage}">
			<a href="<my:replaceParam name='page' value='${paging.totalPage }' />">끝</a>
		</c:if>

	</div>
</div>