<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<script>

    $(document).ready(function () {

        $("#searchForm select").change(function () {

            if ($(this).attr("id") == "siteNo") {
                if (localStorage[SEARCH_SITE_NO] != $(this).val()) {
                    localStorage[SEARCH_SITE_NO] = $(this).val();

                    $("#factoryNo").val("") ;
                    localStorage[SEARCH_FACTORY_NO] = "";

                    $("#areaNo").val("") ;
                    localStorage[SEARCH_AREA_NO] = "";

                    $("#agentNo").val("") ;
                    localStorage[SEARCH_AGENT_NO] = "";
                }
            } else if ($(this).attr("id") == "factoryNo") {
                if (localStorage[SEARCH_FACTORY_NO] != $(this).val()) {
                    localStorage[SEARCH_FACTORY_NO] = $(this).val();

                    $("#areaNo").val("") ;
                    localStorage[SEARCH_AREA_NO] = "";

                    $("#agentNo").val("") ;
                    localStorage[SEARCH_AGENT_NO] = "";
                }
            } else if ($(this).attr("id") == "areaNo") {
                if (localStorage[SEARCH_AREA_NO] != $(this).val()) {
                    localStorage[SEARCH_AREA_NO] = $(this).val();

                    $("#agentNo").val("") ;
                    localStorage[SEARCH_AGENT_NO] = "";
                }
            } else if ($(this).attr("id") == "agentNo") {
                localStorage[SEARCH_AGENT_NO] = $(this).val();
            }

            // location.href = "?siteNo=" + localStorage[SEARCH_SITE_NO] + "&factoryNo=" + localStorage[SEARCH_FACTORY_NO]
            //     + "&areaNo=" + localStorage[SEARCH_AREA_NO] + "&agentNo=" + localStorage[SEARCH_AGENT_NO] ;

            $("#searchForm").submit() ;

        })

    })

</script>

<div class="content-option">
    <div class="option-date" id="option-date"></div>
<%--    <form id="searchForm">--%>
        <dl class="option-data">
            <dt>사업장 구분</dt>
            <dd>
                <select id="siteNo" name="siteNo">
                    <option value="">사업장을 선택하세요</option>
                    <c:forEach var="site" items="${siteList}">
                        <option value="${site.siteNo}" ${site.siteNo eq param.siteNo ? 'selected' : ''}>${site.siteName}</option>
                    </c:forEach>
                </select>
            </dd>
            <dt>공장 구분</dt>
            <dd>
                <select id="factoryNo" name="factoryNo">
                    <option value="">공장을 선택하세요</option>
                    <c:forEach var="factory" items="${factoryList}">
                        <option value="${factory.factoryNo}" ${factory.factoryNo eq param.factoryNo ? 'selected' : ''}>${factory.factoryName}</option>
                    </c:forEach>
                </select>
            </dd>
            <dt>구역 구분</dt>
            <dd>
                <select id="areaNo" name="areaNo">
                    <option value="">구역을 선택하세요</option>
                    <c:forEach var="area" items="${areaList}">
                        <option value="${area.areaNo}" ${area.areaNo eq param.areaNo ? 'selected' : ''}>${area.areaName}</option>
                    </c:forEach>
                </select>
            </dd>

            <c:if test="${not empty agentList}">
                <dt>장비 구분</dt>
                <dd>
                    <select id="agentNo" name="agentNo">
                        <option value="">장비을 선택하세요</option>
                        <c:forEach var="agent" items="${agentList}">
                            <option value="${agent.agentNo}" ${agent.agentNo eq param.agentNo ? 'selected' : ''}>${agent.agentName}</option>
                        </c:forEach>
                    </select>
                </dd>
            </c:if>

        </dl>
<%--    </form>--%>
</div>