<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" >
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>PEMS</title>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link rel="icon" href="<c:url value="/resource/favicon.png"/>">

<link rel="stylesheet" href="<c:url value="/resource/css/snackbar.css"/>" type="text/css"/>
<link rel="stylesheet" href="<c:url value="/resource/css/common.css"/>" type="text/css"/>
<link rel="stylesheet" href="<c:url value="/resource/css/fonts.css"/>" type="text/css"/>
<link rel="stylesheet" href="<c:url value="/resource/css/reset.css"/>" type="text/css"/>
<link rel="stylesheet" href="<c:url value="/resource/jquery-ui-1.13.1/jquery-ui.min.css"/>" type="text/css"/>

<tiles:insertAttribute name="webJs"/>

</head>

<body id="wrap">
		<tiles:insertAttribute name="webHeader"/>
		<tiles:insertAttribute name="webContent"/>
		<tiles:insertAttribute name="webFooter"/>
</body>
</html>