<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%--
<script type="application/javascript" src="/assets/plugins/jquery/jquery-1.9.1.min.js"></script>
<script type="application/javascript" src="/assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
<script type="application/javascript" src="/assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
<script type="application/javascript" src="/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script type="application/javascript" src="/assets/plugins/pace/pace.min.js"></script>
<script type="application/javascript" src="/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script type="application/javascript" src="/assets/plugins/jquery-cookie/jquery.cookie.js"></script>
<script type="application/javascript" src="/assets/js/apps.js"></script>
<script type="application/javascript" src="/assets/js/pne.js"></script>
<script type="application/javascript" src="/assets/js/extension.js"></script>
--%>
<script src="<c:url value="/resource/js/jquery-3.4.1.min.js"/>"></script>
<script src="<c:url value="/resource/js/common.js"/>"></script>
<script src="<c:url value="/resource/js/cryptico.min.js"/>"></script>
<script src="<c:url value="/resource/js/jquery.form.min.js"/>"></script>
<script src="<c:url value="/resource/jquery-ui-1.13.1/jquery-ui.min.js"/>"></script>

<%--
<script src="/resource/web/js/jquery-ui.min.js"></script>
<script src="/resource/web/js/script.js"></script>
<script src="/resource/web/js/swiper.min.js"></script>
<script src="/resource/js/common.js"></script>
<script src="/resource/js/cryptico.min.js"></script>
--%>
