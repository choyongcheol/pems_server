<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<script type="text/javascript">

    let USER_TABLE_NAME_CHANNEL = "channelInfo" ;
    let USER_TABLE_NAME_CYC = "cycrecord" ;

    let SEARCH_SITE_NO = "SEARCH_SITE_NO" ;
    let SEARCH_FACTORY_NO = "SEARCH_FACTORY_NO" ;
    let SEARCH_AREA_NO = "SEARCH_AREA_NO" ;
    let SEARCH_AGENT_NO = "SEARCH_AGENT_NO" ;

    let CYC_COLUMN_ID_ARRAY = "CYC_COLUMN_ID_ARRAY";
    let CYC_COLUMN_NAME_ARRAY = "CYC_COLUMN_NAME_ARRAY";
    let CYC_VISIBLE_ARRAY = "CYC_VISIBLE_ARRAY";
    let CHANNEL_COLUMN_ID_ARRAY = "CHANNEL_COLUMN_ID_ARRAY";
    let CHANNEL_COLUMN_NAME_ARRAY = "CHANNEL_COLUMN_NAME_ARRAY";
    let CHANNEL_VISIBLE_ARRAY = "CHANNEL_VISIBLE_ARRAY";

    $(document).ready(function () {
        $("#option-date").text(getCommonNowDate(new Date()));

        if (location.protocol == "http:") {
            // location.href = document.location.href.replace("http:", "https:") ;
        }

        // 체크박스 전체 선택
        $(document).on("click", ".jCheck_All", function() {
            $(".jCheck_Each").prop("checked", this.checked);
        });

        // 체크박스 개별 선택
        $(document).on("click", ".jCheck_Each", function() {
            $(".jCheck_All").prop("checked", ($(".jCheck_Each:checked").length == $(".jCheck_Each").length)) ;
        });

        // 로그아웃
        $(".jLogout").click(function () {
            $.ajax({
                url: "/login/logout",
                type: "POST",
                dataType: "json",
                data: {},
                success: function (data) {
                    if (data.returnCode == 1) {
                        location.href = "/";
                    }
                }
            })
        })

        $(".jDatepicker").datepicker({
			dateFormat: 'yy-mm-dd',
            monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
            dayNames: ['일', '월', '화', '수', '목', '금', '토'],
            showMonthAfterYear: true,
            yearSuffix: '년'
		});

        // 엔터키 이벤트
        $("#keyword").on("keyup", function (key) {
            if (key.keyCode == 13) {
                $(".jSearchKeyword").trigger("click");
            }
        })

        $(document).on('mouseenter focus', '.header-menu > li > a', function () {
            $('#header').addClass('on');
            $('.gnb_subMenu li').removeClass('on');

            if ($('#header').hasClass('active') == false) {
                $(this).parent('li').addClass('on').siblings('li').removeClass('on');
            }
        }).on('mouseenter focus', '.header .submenu a', function () {
            if ($(this).parents('ul').hasClass('submenu')) {
                $(this).parents('.submenu').parent('li').addClass('on').siblings('li').removeClass('on');
            } else {
                $(this).parent('li').addClass('on').siblings('li').removeClass('on');
            }
        }).on('mouseenter focus', '.header-user a', function () {
            $('#header').removeClass('on');
            $('.header-menu li').removeClass('on');
        }).on('mouseleave', '.header-menu', function () {
            $('#header').removeClass('on');
            $('.header-menu li').removeClass('on');
        })

        // 회원정보 수정
        $(".jUpdateUserInfo").click(function () {

            if ("" == $("#input_username").val()) {
                alert("이름은 공백일 수 없습니다.");
                $("#input_username").focus();
                return false;
            }

            $.ajax({
                url: "/user/setUpdateUserInfo",
                type: "POST",
                dataType: "json",
                data: $("#userInfoForm").serialize(),
                success: function (data) {
                    alert(data.returnMessage);
                    if (data.returnCode == 1) {
                        location.reload();
                    }
                }
            })
        })

        $(".jUpdateUserPw").click(function () {

            if ("" == $("#input_username").val()) {
                alert("이름은 공백일 수 없습니다.");
                $("#input_username").focus();
                return false;
            }

            if ($("#input_chpw_1").val() == "") {
                alert("비밀번호를 입력하세요");
                $("#input_chpw_1").focus();
                return false;
            }

            if ($("#input_chpw_1").val() != $("#input_chpw_2").val()) {
                alert("비밀번호를 확인하세요");
                $("#input_chpw_2").focus();
                return false;
            }
            $("#sendpassword").val(getRAS($("#input_chpw_2").val()));

            $.ajax({
                url: "/user/setUpdateUserPw",
                type: "POST",
                dataType: "json",
                data: $("#userPwForm").serialize(),
                success: function (data) {
                    alert(data.returnMessage);
                    if (data.returnCode == 1) {
                        location.reload();
                    }
                }
            })

        })

    })

    function setUpdateUserPassword() {

        $.ajax({
            url: "/user/getRsaKey",
            type: "POST",
            dataType: "json",
            data: {},
            success: function (data) {
                $("#publicKeyModulus").val(data.publicKeyModulus) ;
                $("#publicKeyExponent").val(data.publicKeyExponent) ;
            }
        })

        openModal("modal-pw") ;
    }

    function openModal(modalId) {
        $("body").addClass("show-modal");
        $("#" + modalId).toggleClass("active");
        return false;
    }

    function closeModal(obj) {
        $("body").removeClass("show-modal");
        $("#" + $(obj).closest(".modal-wrap").attr("id")).removeClass("active");
        return false;
    }

    function closeModalId(modalId) {
        $("body").removeClass("show-modal");
        $("#" + modalId).removeClass("active");
        return false;
    }

    function pageLoad(url) {
        let searchSiteNo = localStorage[SEARCH_SITE_NO] ;
        let searchFactoryNo = localStorage[SEARCH_FACTORY_NO] ;
        let searchAreaNo = localStorage[SEARCH_AREA_NO] ;
        let searchAgentNo = localStorage[SEARCH_AGENT_NO] ;

        location.href = url  + "?siteNo=" + searchSiteNo + "&factoryNo=" + searchFactoryNo
            + "&areaNo=" + searchAreaNo + "&agentNo=" + searchAgentNo ;
    }

    // 유저 컬럼설정 조회
    function getUserTableColumns(tableName) {
        $.ajax({
            url: "/common/getUserColumnList",
            type: "post",
            dataType: "json",
            data: {tableName: tableName},
            success: function (data) {

                if (USER_TABLE_NAME_CYC == tableName) {
                    localStorage.setItem(CYC_COLUMN_ID_ARRAY, data.column.columnId) ;
                    localStorage.setItem(CYC_COLUMN_NAME_ARRAY, data.column.columnName) ;
                    localStorage.setItem(CYC_VISIBLE_ARRAY, data.column.columnVisible) ;
                } else if (USER_TABLE_NAME_CHANNEL == tableName) {
                    localStorage.setItem(CHANNEL_COLUMN_ID_ARRAY, data.column.columnId) ;
                    localStorage.setItem(CHANNEL_COLUMN_NAME_ARRAY, data.column.columnName) ;
                    localStorage.setItem(CHANNEL_VISIBLE_ARRAY, data.column.columnVisible) ;
                }

            }
        })
    }

    // 유저 컬럼설정 저장
    function setUserTableColumns(tableName, columnId, columnName, columnVisible) {
        let params = {
            tableName: tableName
            , columnId: columnId
            , columnName: columnName
            , columnVisible: columnVisible
        }

        $.ajax({
            url: "/common/setUpdateUserColumns",
            type: "post",
            dataType: "json",
            data: params,
            success: function (data) {
            }
        })
    }


</script>

<header>
    <header id="header">
        <div class="inner">
            <div class="header-total"><a href="#">전체메뉴</a></div>
            <h1 class="header-logo">
                <a href="/"><img src="<c:url value="/resource/images/common/header_logo@2x.png"/>"alt="wonik pne"></a>
            </h1>
            <div class="header-menu-wrap" style="display: block;">
                <ul class="header-menu">
<%--                    <li class="item1"><a href="/">홈</a></li>--%>
                    <li class="item2">
                        <a href="#">설비</a>
                        <ul class="submenu">
                            <li><a href="#" onclick="pageLoad('/module/moduleList')">설비 정보</a></li>
                            <li><a href="#" onclick="pageLoad('/module/manager')">설비 담당자 관리</a></li>
<%--                            <li><a href="/gear/rate">가동률</a></li>--%>
                        </ul>
                    </li>
                    <li class="item3">
                        <a href="#" onclick="pageLoad('/process/processList')">결과데이터</a>
                    </li>
                    <li class="item4">
                        <a href="#">사업장 관리</a>
                        <ul class="submenu">
                            <li><a href="<c:url value="/setting/cityList"/>">지역 관리</a></li>
                            <li><a href="<c:url value="/setting/siteList"/>">사업장 관리</a></li>
                            <li><a href="<c:url value="/setting/factoryList"/>">공장 관리</a></li>
                            <li><a href="<c:url value="/setting/areaList"/>">구역(룸) 관리</a></li>
                        </ul>
                    </li>
                    <li class="item5">
                        <a href="#">S/W 업데이트</a>
                        <ul class="submenu">
                            <li><a href="<c:url value="/update/updateFileList?fileType=SBC"/>">SBC 파일관리</a></li>
                            <li><a href="<c:url value="/update/updateFileList?fileType=CTS"/>">CTS 파일관리</a></li>
                            <li><a href="<c:url value="/update/updateFileList?fileType=DLL"/>">DLL 파일관리</a></li>
                        </ul>
                    </li>
                    <li class="item6">
                        <a href="#">기타</a>
                        <ul class="submenu">
                            <li><a href="#" onclick="pageLoad('/etc/agentList')">에이전트 관리</a></li>
                            <li><a href="<c:url value="/etc/alarmSetList"/>">알람셋 리스트</a></li>
                            <li><a href="#" onclick="pageLoad('/etc/sendAlarmList')">알림 발송이력</a></li>
                        </ul>
                    </li>
                </ul>
                <div class="header-myinfo">
                    <a onclick="openModal('modal-info')">회원 정보</a>
                </div>
                <div class="header-pw">
                    <a onclick="setUpdateUserPassword()">비밀번호 변경</a>
                </div>
                <div class="header-user">
                    <a href="#" class="jLogout">로그아웃</a>
                </div>
            </div>
        </div>

        <div class="modal-wrap" id="modal-info">
            <div class="modal-content">

                <div class="modal-head">
                    <h2 class="modal-head-title">회원정보</h2>
                    <button class="modal-close" onclick="closeModal(this)">닫기</button>
                </div><!-- // modal-head -->

                <div class="modal-body">
                    <div class="modal-scroll">
                        <div class="scroll-inner">
                            <form id="userInfoForm">
                                <input type="hidden" name="signId" value="${webInfo.userId }">
                                <input type="hidden" name="userNo" value="${webInfo.userNo }">
                                <ul class="form-list">
                                    <li>
                                        <label class="form-title">이름</label>
                                        <div class="form-box">
                                            <input type="text" id="input_username" name="userName"
                                                   value="${webInfo.userName }">
                                        </div>
                                    </li>
                                    <li>
                                        <label class="form-title">휴대폰 번호</label>
                                        <div class="form-box form-te">
                                            <div class="tel1">
                                                <input type="text" id="input_tel1" name="phoneNo"
                                                       value="${webInfo.decryptedPhoneNo }">
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <label class="form-title">부서</label>
                                        <div class="form-box">
                                            <input type="text" id="input_section" name="dept" value="${webInfo.dept }">
                                        </div>
                                    </li>
                                    <li>
                                        <label class="form-title">직급</label>
                                        <div class="form-box">
                                            <input type="text" id="input_position" name="position"
                                                   value="${webInfo.position }">
                                        </div>
                                    </li>
                                    <li>
                                        <label class="form-title">이메일</label>
                                        <div class="form-box form-email">
                                            <div class="email1">
                                                <input type="text" id="input_email" name="email"
                                                       value="${webInfo.email }">
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <hr class="sect-line">
                                <div class="form-btn">
                                    <a href="javascript:void(0);" class="line" onclick="closeModal(this)">취소</a>
                                    <a href="#" class="jUpdateUserInfo">수정</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal-wrap" id="modal-pw">
            <input type="hidden" id="publicKeyModulus" name="publicKeyModulus">
            <input type="hidden" id="publicKeyExponent" name="publicKeyExponent">
            <div class="modal-content">
                <div class="modal-head">
                    <h2 class="modal-head-title">비밀번호 변경</h2>
                    <button class="modal-close" onclick="closeModal(this)">닫기</button>
                </div>

                <div class="modal-body">
                    <div class="modal-scroll">
                        <div class="scroll-inner">
                            <form id="userPwForm">
                                <ul class="form-list">
                                    <li>
                                        <label class="form-title">아이디</label>
                                        <div class="form-box">
                                            <input type="text" value="${webInfo.userId }" readonly>
                                        </div>
                                    </li>
                                    <li>
                                        <label class="form-title">이름</label>
                                        <div class="form-box">
                                            <input type="text" name="userName" value="${webInfo.userName }" readonly>
                                        </div>
                                    </li>
                                    <li>
                                        <label class="form-title">비밀번호</label>
                                        <input type="hidden" id="sendpassword" name="signPassword">
                                        <div class="form-box">
                                            <input type="password" id="input_chpw_1">
                                        </div>
                                    </li>
                                    <li>
                                        <label class="form-title">비밀번호 확인</label>
                                        <div class="form-box">
                                            <input type="password" id="input_chpw_2">
                                        </div>
                                    </li>
                                </ul>
                                <hr class="sect-line">
                                <div class="form-btn">
                                    <a href="javascript:void(0);" class="line" onclick="closeModal(this)">취소</a>
                                    <a href="#" class="jUpdateUserPw">수정</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </header>
</header>
