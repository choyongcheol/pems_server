<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<script>

    $(document).ready(function () {

        $(".jUpdateAgent").click(function() {

            $.ajax({
                url: "/etc/setUpdateAgent",
                type: "post",
                dataType: "json",
                data: $("#agentForm").serialize(),
                success: function (data) {
                    alert(data.returnMessage);
                    if (data.returnCode == 1) {
                        location.reload() ;
                    }
                }
            })
        })

    })

    function setUpdateAgentData(agentNo, modalId) {
        $("#agentForm #agentNo").val(agentNo) ;
        $("#agentForm #agentName").val($("#agentName_" + agentNo).text()) ;
        $("#agentForm #comment").val($("#comment_" + agentNo).text()) ;

        openModal(modalId) ;
    }

</script>


<div id="wrap">
    <div id="container">
        <div class="content">
            <div class="content-head">
                <h2>- 에이전트 관리</h2>
            </div>

            <div class="content-option">
                <div class="option-date" id="option-date"></div>
            </div>

            <div class="content-option">
                <h3 class="option-title">에이전트 목록</h3>
            </div>

            <div class="table-wrap">
                <table class="base-table">
                    <thead>
                        <tr>
                            <th scope="col" style="width:10%">고유번호</th>
                            <th scope="col">PC IP</th>
                            <th scope="col">모듈 수</th>
                            <th scope="col">데이터 최초 전송 일자</th>
                            <th scope="col">데이터 최근 전송 일자</th>
                            <th scope="col">이름</th>
                            <th scope="col">메모</th>
                            <th scope="col">수정</th>
                            <th scope="col">등록일</th>
                        </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="agent" items="${list}">
                        <tr>
                            <td>${agent.agentNo}</td>
                            <td>${agent.pcIp}</td>
                            <td>${agent.moduleCount}</td>
                            <td>${agent.firstReceiveDate}</td>
                            <td>${agent.lastReceiveDate}</td>
                            <td id="agentName_${agent.agentNo}">${agent.agentName}</td>
                            <td id="comment_${agent.agentNo}">${agent.comment}</td>
                            <td>
                                <div class="option-btn-center">
                                    <a href="javascript:void(0);" onclick="setUpdateAgentData('${agent.agentNo}', 'modal-agent')">수정</a>
                                </div>
                            </td>
                            <td>${agent.regDate}</td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
            <%-- 페이징 --%>
            <%@include file="/WEB-INF/jsp/tiles_template/web/paging.jsp"%>
        </div>
    </div>
</div>

<div class="modal-wrap" id="modal-agent">
    <div class="modal-content">
        <div class="modal-head">
            <h2 class="modal-head-title">에이전트 수정</h2>
            <button class="modal-close" onclick="closeModal(this)">닫기</button>
        </div>

        <div class="modal-body">
            <div class="modal-scroll">
                <div class="scroll-inner">

                    <form id="agentForm">
                        <input type="hidden" id="agentNo" name="agentNo">
                        <ul class="form-list">
                            <li>
                                <label class="form-title">이름</label>
                                <div class="form-box">
                                    <div class="input">
                                        <input type="text" id="agentName" name="agentName">
                                    </div>
                                </div>
                            </li>
                            <li>
                                <label class="form-title">메모</label>
                                <div class="form-box">
                                    <div class="input">
                                        <input type="text" id="comment" name="comment">
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </form>
                    <hr class="sect-line">
                    <div class="form-btn">
                        <a href="javascript:void(0);" class="line" onclick="closeModal(this)">취소</a>
                        <a href="javascript:void(0);" id="updateBtn" class="jRegBtn jUpdateAgent">수정</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>