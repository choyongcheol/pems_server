<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<script>

    $(document).ready(function () {

        $("#searchForm #alarmDiv").change(function() {
            $("#searchForm").submit() ;
        })

    })

</script>


<body>
<div id="wrap">
    <div id="container">
        <div class="content">
            <div class="content-head">
                <h2>- 알람 설정</h2>
            </div>

            <form id="searchForm">
                <div class="content-option">
                    <div class="option-date" id="option-date"></div>
                </div>

                <div class="content-option">
                    <h3 class="option-title">알람설정 목록</h3>
                    <ul class="option-data">
                        <li class="title">검색</li>
                        <li class="auto triple">
                            <div class="option-select">
                                <select id="alarmDiv" name="alarmDiv">
                                    <option value="">알람 구분</option>
                                    <option value="EMG" ${param.alarmDiv eq "EMG" ? "selected" : ""}>EMG 알람</option>
                                    <option value="CH" ${param.alarmDiv eq "CH" ? "selected" : ""}>채널 알람</option>
                                </select>
                            </div>
                        </li>
                    </ul>
                </div>
            </form>

            <div class="table-wrap">
                <table class="base-table">
                    <thead>
                    <tr>
                        <th style="width: 10%;">알람구분</th>
                        <th style="width: 10%;">알람코드</th>
                        <th scope="col">알람 메시지</th>
                        <th scope="col">알람 원인</th>
                    </tr>
                    </thead>
                    <tbody id="alarmlist-tbody">
                        <c:forEach var="alarmSet" items="${list}">
                            <tr>
                                <td>${alarmSet.alarmDiv}</td>
                                <td>${alarmSet.alarmCode}</td>
                                <td>${alarmSet.alarmMessage}</td>
                                <td>${alarmSet.alarmDesc}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>

            <%-- 페이징 --%>
            <c:if test="${paging.startBlock ne paging.endBlock}">
                <%@include file="/WEB-INF/jsp/tiles_template/web/paging.jsp" %>
            </c:if>

        </div>
    </div>
</div>

<div class="modal-wrap" id="modal-17">
    <div class="modal-content">

        <div class="modal-head">
            <h2 class="modal-head-title">사용자 관리</h2>
            <button class="modal-close">닫기</button>
        </div>

        <div class="modal-body" style="height: 448px;">
            <div class="modal-scroll">
                <div class="scroll-inner">
                    <ul class="form-list">
                        <li>
                            <label class="form-title">알람구분<span class="req">*</span></label>
                            <div class="form-box form-half">
                                <div>
                                    <select id="alarm_div" name="" title="">
                                        <option value="">알람구분 선택</option>
                                        <option value="CH">채널(CH)</option>
                                        <option value="EMG">EMG</option>
                                    </select>
                                </div>
                            </div>
                        </li>
                        <li>
                            <label class="form-title">알람 코드<span class="req">*</span></label>
                            <div class="form-box">
                                <div class="input"><input type="text" id="alarm_code"></div>
                            </div>
                        </li>
                        <li>
                            <label class="form-title">알람 메시지<span class="req">*</span></label>
                            <div class="form-box">
                                <div class="input"><input type="text" id="alarm_message"></div>
                            </div>
                        </li>
                        <li>
                            <label class="form-title">알람 설명<span class="req">*</span></label>
                            <div class="form-box">
                                <div class="input"><input type="text" id="alarm_desc"></div>
                            </div>
                        </li>
                        <li>
                            <label class="form-title">알람 방법<span class="req">*</span></label>
                            <div class="form-box form-checkbox">
                                <div class="check">
                                    <input type="checkbox" id="check_m17_1" name="checkbox" value="check_m17_1"
                                           onclick="checkSms()">
                                    <label for="check_m17_1">SMS 알람</label>
                                </div>
                                <div class="check">
                                    <input type="checkbox" id="check_m17_2" name="checkbox" value="check_m17_2"
                                           onclick="checkEmail()">
                                    <label for="check_m17_2">이메일 알람</label>
                                </div>
                            </div>
                        </li>
                        <li>
                            <label class="form-title">변환가능 문자<span class="req">*</span></label>
                            <div class="form-text">{담당자} {설비} {채널} {사업장} {공장} {구역}</div>
                        </li>
                        <li>
                            <label class="form-title">SMS 메시지<span class="req">*</span></label>
                            <div class="form-box">
                                <input type="text" id="sms_content">
                            </div>
                        </li>
                        <li>
                            <label class="form-title">이메일 제목<span class="req">*</span></label>
                            <div class="form-box">
                                <input type="text" id="email_title">
                            </div>
                        </li>
                        <li>
                            <label class="form-title">이메일 내용<span class="req">*</span></label>
                            <div class="form-box">
                                <textarea id="email_content"></textarea>
                            </div>
                        </li>
                        <li>
                            <label class="form-title">상태</label>
                            <div class="form-box form-half">
                                <div>
                                    <select id="register-status" name="" title="">
                                        <option value="Y">사용</option>
                                        <option value="N">미사용</option>
                                    </select>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <hr class="sect-line">
                    <div class="form-btn">
                        <a href="#" class="line" onclick="togglePopup()">취소</a>
                        <a href="#" onclick="applyRegister()">등록</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>