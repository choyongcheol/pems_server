<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<script>
    $(document).ready(function () {

        $(".jSearchFormSubmit").click(function() {
            $("#searchForm").submit() ;
        })

        $("input[name='alarmDiv']").click(function() {
            setAlarmCodeList() ;
        })


        setAlarmCodeList() ;
    })

    function setAlarmCodeList() {
        let alarmCode = "${param.alarmCode}";

        $.ajax({
            url: "/etc/alarmSetList",
            type: "post",
            dataType: "json",
            data: {alarmDiv: $("input[name='alarmDiv']:checked").val()},
            success: function (data) {
                let html = '<option value="">알림 구분</option>' ;

                $.each(data.list, function (idx, obj) {
                    html += '<option value="' + obj.alarmCode + '" ' + (obj.alarmCode == alarmCode ? "selected" : "") + '>' + obj.alarmMessage + '</option>';
                })

                $("#alarm-code").html(html);
            }
        })
    }

</script>

<div id="wrap">
    <div id="container">
        <div class="content">
            <div class="content-head">
                <h2>- 알림 발송이력</h2>
            </div>

            <form id="searchForm">
                <%@include file="/WEB-INF/jsp/tiles_template/web/searchForm.jsp"%>

                <div class="content-option">
                    <ul class="option-data left">
                        <li class="title">발생 구분</li>
                        <li class="auto">
                            <div class="option-radio">
                                <input type="radio" id="emg_radio_button" name="alarmDiv" value="EMG" ${param.alarmDiv eq 'CH' ? "" : "checked"}>
                                <label for="emg_radio_button">EMG알림</label>
                                <input type="radio" id="ch_radio_button" name="alarmDiv" value="CH" ${param.alarmDiv eq 'CH' ? "checked" : ""}>
                                <label for="ch_radio_button">채널알림</label>
                            </div>
                        </li>
                        <li class="title">알림 구분</li>
                        <li class="auto">
                            <dl class="option-data">
                                <dd>
                                    <select id="alarm-code" name="alarmCode">
                                    </select>
                                </dd>
                            </dl>
                        </li>
                    </ul>
                    <ul class="option-data">
                        <li class="title">발송 일자</li>
                        <li class="auto type-date-btn">
                            <div class="date">
                                <input type="text" class="jDatepicker" name="sDate" value="${param.sDate}" autocomplete="off">
                            </div>
                            <div class="dash">~</div>
                            <div class="date">
                                <input type="text" class="jDatepicker" name="eDate" value="${param.eDate}" autocomplete="off">
                            </div>
                            <div class="button"><button type="button" class="jSearchFormSubmit">조회</button></div>
                        </li>
                    </ul>
                </div>
            </form>

            <div class="table-wrap">
                <table class="base-table">
                    <thead>
                        <tr>
                            <th scope="col">사업장명</th>
                            <th scope="col">공장명</th>
                            <th scope="col">구역명</th>
                            <th scope="col">발생 구분</th>
                            <th scope="col">코드</th>
                            <th scope="col">설명</th>
                            <th scope="col">발생일자</th>
                            <th scope="col">알림발송일자</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="alarm" items="${list}">
                            <tr>
                                <td>${alarm.siteName}</td>
                                <td>${alarm.factoryName}</td>
                                <td>${alarm.areaName}</td>
                                <td>${alarm.alarmDiv}</td>
                                <td>${alarm.emgCode}</td>
                                <td>${alarm.emgDescription}</td>
                                <td>${alarm.emgDateTime}</td>
                                <td>${alarm.regDate}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>

            <%-- 페이징 --%>
            <c:if test="${paging.startBlock ne paging.endBlock}">
                <%@include file="/WEB-INF/jsp/tiles_template/web/paging.jsp" %>
            </c:if>

        </div>
    </div>
</div>