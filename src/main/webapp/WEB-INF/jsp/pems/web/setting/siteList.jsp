<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<script>

	$(document).ready(function () {

        $(".jInsertSite").click(function() {
            if ($("#siteForm #siteName").val().trim() == "") {
                alert("사업장명을 입력하세요") ;
                $("#siteForm #siteName").focus() ;
                return false ;
            }

            $.ajax({
                url: "/setting/insertSite",
                type: "post",
                dataType: "json",
                data: $("#siteForm").serialize(),
                success: function (data) {
                    alert(data.returnMessage);
                    if (data.returnCode == 1) {
                        location.reload() ;
                    }
                }
            })
        })

        $(".jUpdateSite").click(function() {
            if ($("#siteForm #siteName").val().trim() == "") {
                alert("지역명을 입력하세요") ;
                $("#siteForm #siteName").focus() ;
                return false ;
            }

            $.ajax({
                url: "/setting/setUpdateSite",
                type: "post",
                dataType: "json",
                data: $("#siteForm").serialize(),
                success: function (data) {
                    alert(data.returnMessage);
                    if (data.returnCode == 1) {
                        location.reload() ;
                    }
                }
            })
        })

        $(".jDeleteSite").click(function() {
            if (confirm("정말 삭제하시겠습니까?")) {
                $.ajax({
					url: "/setting/setDeleteSite",
					type: "post",
					dataType: "json",
					data: { siteNo: $(this).attr("_no") }
					, success: function (data) {
						alert(data.returnMessage);
						if (data.returnCode == 1) {
							location.reload() ;
						}
					}
				})
            }
        })

	})

	function setInsertSiteData(modalId) {
        $(".jRegBtn#insertBtn").show() ;
        $(".jRegBtn#updateBtn").hide() ;
		openModal(modalId) ;
    }

    function setUpdateSiteData(siteNo, modalId) {
        $(".jRegBtn#insertBtn").hide() ;
        $(".jRegBtn#updateBtn").show() ;

		$("#siteForm #cityNo").val($("#cityNo_" + siteNo).val()) ;
		$("#siteForm #siteNo").val($("#siteNo_" + siteNo).text()) ;
		$("#siteForm #siteName").val($("#siteName_" + siteNo).text()) ;
		$("#siteForm #address").val($("#address_" + siteNo).text()) ;
		$("#siteForm #tel").val($("#tel_" + siteNo).text()) ;

		openModal(modalId) ;
    }

</script>


<div id="wrap">
	<div id="container">
		<div class="content">
			<div class="content-head">
				<h2>- 사업장 관리</h2>
			</div>

			<div class="content-option">
				<div class="option-date" id="option-date"></div>
			</div>

			<div class="content-option">
				<h3 class="option-title">사업장 목록</h3>
				<div class="option-btn">
					<a href="javascript:void(0);" onclick="setInsertSiteData('modal-site');">등록</a>
				</div>
			</div>

			<div class="table-wrap">
				<table class="base-table">
					<thead>
						<tr>
							<th scope="col" style="width:10%">사업장 고유번호</th>
							<th scope="col">지역명</th>
							<th scope="col">사업장이름</th>
							<th scope="col">상세주소</th>
							<th scope="col">대표 연락처</th>
							<th scope="col" style="width:10%">수정</th>
							<th scope="col" style="width:10%">삭제</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="site" items="${list}">
							<tr>
								<td id="siteNo_${site.siteNo}">${site.siteNo}</td>
								<td>
									${site.cityName}
									<input type="hidden" id="cityNo_${site.siteNo}" value="${site.cityNo}">
								</td>
								<td id="siteName_${site.siteNo}">${site.siteName}</td>
								<td id="address_${site.siteNo}">${site.address}</td>
								<td id="tel_${site.siteNo}">${site.tel}</td>
								<td>
									<div class="option-btn-center">
										<a href="javascript:void(0);" onclick="setUpdateSiteData('${site.siteNo}', 'modal-site')">수정</a>
									</div>
								</td>
								<td>
									<div class="option-btn-center">
										<a href="javascript:void(0);" class="jDeleteSite" _no="${site.siteNo}">삭제</a>
									</div>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<%-- 페이징 --%>
			<%@include file="/WEB-INF/jsp/tiles_template/web/paging.jsp"%>
		</div>
	</div>
</div>

<div class="modal-wrap" id="modal-site">
	<div class="modal-content">
		<div class="modal-head">
			<h2 class="modal-head-title">사업장 등록</h2>
			<button class="modal-close" onclick="closeModal(this)">닫기</button>
		</div>

		<div class="modal-body">
			<div class="modal-scroll">
				<div class="scroll-inner">

					<form id="siteForm">
						<input type="hidden" id="siteNo" name="siteNo">
						<ul class="form-list">
							<li>
								<label class="form-title">지역명<span class="req">*</span></label>
								<div class="form-box form-half">
									<div>
										<select id="cityNo" name="cityNo">
											<option value="">사업장을 선택하세요.</option>
											<c:forEach var="city" items="${cityList}">
												<option value="${city.cityNo}">${city.cityName}</option>
											</c:forEach>
										</select>
									</div>
								</div>
							</li>
							<li>
								<label class="form-title">이름</label>
								<div class="form-box">
									<div class="input">
										<input type="text" id="siteName" name="siteName">
									</div>
								</div>
							</li>
							<li>
								<label class="form-title">상세주소</label>
								<div class="form-box">
									<div class="input">
										<input type="text" id="address" name="address">
									</div>
								</div>
							</li>
							<li>
								<label class="form-title">연락처</label>
								<div class="form-box form-find-tel-btn">
									<div class="tel">
										<input type="text" id="tel" name="tel">
									</div>
								</div>
							</li>
						</ul>
					</form>

					<hr class="sect-line">
					<div class="form-btn">
						<a href="javascript:void(0);" class="line" onclick="closeModal(this)">취소</a>
						<a href="javascript:void(0);" id="updateBtn" class="jRegBtn jUpdateSite">수정</a>
						<a href="javascript:void(0);" id="insertBtn" class="jRegBtn jInsertSite">등록</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
