<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>

<script>
    $(document).ready(function () {

        $(".jInsertArea").click(function () {
            if ($("#areaForm #areaName").val().trim() == "") {
                alert("구역명을 입력하세요");
                $("#areaForm #areaName").focus();
                return false;
            }

            $.ajax({
                url: "/setting/insertArea",
                type: "post",
                dataType: "json",
                data: $("#areaForm").serialize(),
                success: function (data) {
                    alert(data.returnMessage);
                    if (data.returnCode == 1) {
                        location.reload();
                    }
                }
            })
        })

        $(".jUpdateArea").click(function () {
            if ($("#areaForm #areaName").val().trim() == "") {
                alert("지역명을 입력하세요");
                $("#areaForm #areaName").focus();
                return false;
            }

            $.ajax({
                url: "/setting/setUpdateArea",
                type: "post",
                dataType: "json",
                data: $("#areaForm").serialize(),
                success: function (data) {
                    alert(data.returnMessage);
                    if (data.returnCode == 1) {
                        location.reload();
                    }
                }
            })
        })

        $(".jDeleteArea").click(function () {
            if (confirm("정말 삭제하시겠습니까?")) {
                $.ajax({
                    url: "/setting/setDeleteArea",
                    type: "post",
                    dataType: "json",
                    data: {areaNo: $(this).attr("_no")}
                    , success: function (data) {
                        alert(data.returnMessage);
                        if (data.returnCode == 1) {
                            location.reload();
                        }
                    }
                })
            }
        })

        $("#areaForm #siteNo").change(function() {
            $.ajax({
                url: "/setting/factoryList",
                type: "post",
                dataType: "json",
                data: {siteNo: $(this).val()}
                , success: function (data) {
                    let factoryNo = $("#areaForm #factoryNo").attr("factoryNo") ;
                    let html = "<option>공장을 선택하세요.</option>" ;
                    data.list.forEach(function(fac, i) {
                        html += '<option value="' + fac.factoryNo + '" name="' + fac.factoryName + '" ' + (fac.factoryNo == factoryNo ? 'selected' : '') + '>' + fac.factoryName + '</option>' ;
                    }) ;
                    $("#areaForm #factoryNo").html(html) ;
                }
            })
        })

    })

    function setInsertAreaData(modalId) {
        $("#areaForm")[0].reset();
        $("#areaForm #factoryNo").html("<option>공장을 선택하세요.</option>") ;
        $(".jRegBtn#insertBtn").show();
        $(".jRegBtn#updateBtn").hide();

        openModal(modalId) ;
    }

    function setUpdateAreaData(areaNo, modalId) {
        $(".jRegBtn#insertBtn").hide();
        $(".jRegBtn#updateBtn").show();

        $("#areaForm #areaNo").val($("#areaNo_" + areaNo).val());
        $("#areaForm #factoryNo").attr("factoryNo", $("#factoryNo_" + areaNo).val()) ;
        $("#areaForm #siteNo").val($("#siteNo_" + areaNo).val());
        $("#areaForm #siteNo").trigger("change") ;
        $("#areaForm #floor").val($("#floor_" + areaNo).val());
        $("#areaForm #areaName").val($("#areaName_" + areaNo).val());
        $("#areaForm #comment").val($("#comment_" + areaNo).val());
        $("#areaForm #status").val($("#status_" + areaNo).val());

        openModal(modalId) ;
    }

</script>

<div id="wrap">
    <div id="container">
        <div class="content">
            <div class="content-head">
                <h2>- 구역(룸) 관리</h2>
            </div>

            <div class="content-option">
                <div class="option-date" id="option-date"></div>
                <ul class="option-data">
                    <li class="title">사업장 상태</li>
                    <li class="auto tabs">
                        <a href="<my:replaceParam name='status' value='' />" class="${empty param.status ? 'on' : ''}">전체</a>
                        <a href="<my:replaceParam name='status' value='Y' />"
                           class="${param.status eq 'Y' ? 'on' : ''}">운영중</a>
                        <a href="<my:replaceParam name='status' value='N' />"
                           class="${param.status eq 'N' ? 'on' : ''}">미사용</a>
                        <a href="<my:replaceParam name='status' value='X' />"
                           class="${param.status eq 'X' ? 'on' : ''}">정지</a>
                    </li>
                </ul>
            </div>

            <div class="content-option">
                <h3 class="option-title">구역(룸) 목록</h3>
                <div class="option-btn">
                    <a href="javascript:void(0);" onclick="setInsertAreaData('modal-area');">등록</a>
                </div>
            </div>
            <div class="table-wrap">
                <table class="base-table">
                    <thead>
                        <tr>
                            <th scope="col" style="width:10%">구역 고유번호</th>
                            <th scope="col">사업장명</th>
                            <th scope="col">공장명</th>
                            <th scope="col">층</th>
                            <th scope="col">구역명</th>
                            <th scope="col">설명</th>
                            <th scope="col">상태</th>
                            <th scope="col" style="width:10%">수정</th>
                            <th scope="col" style="width:10%">삭제</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="area" items="${list}">
                            <tr>
                                <td>
                                    ${area.areaNo}
                                    <input type="hidden" id="areaNo_${area.areaNo}" value="${area.areaNo}">
                                </td>
                                <td>
                                        ${area.siteName}
                                    <input type="hidden" id="siteNo_${area.areaNo}" value="${area.siteNo}">
                                </td>
                                <td>
                                    <span style="color:#009471">
                                        ${area.factoryName}
                                    </span>
                                    <input type="hidden" id="factoryNo_${area.areaNo}" value="${area.factoryNo}">
                                </td>
                                <td>
                                    ${area.floor}
                                    <input type="hidden" id="floor_${area.areaNo}" value="${area.floor}">
                                </td>
                                <td>
                                    <span style="color:#009471">
                                        ${area.areaName}
                                    </span>
                                    <input type="hidden" id="areaName_${area.areaNo}" value="${area.areaName}">
                                </td>
                                <td>
                                    ${area.comment}
                                    <input type="hidden" id="comment_${area.areaNo}" value="${area.comment}">
                                </td>
                                <td>
                                    <c:choose>
                                        <c:when test="${area.status eq 'Y'}">운영중</c:when>
                                        <c:when test="${area.status eq 'N'}">미사용</c:when>
                                        <c:when test="${area.status eq 'X'}">정지</c:when>
                                        <c:otherwise>
                                            ${area.status}
                                        </c:otherwise>
                                    </c:choose>
                                    <input type="hidden" id="status_${area.areaNo}" value="${area.status}">
                                </td>
                                <td>
                                    <div class="option-btn-center">
                                        <a href="javascript:void(0);" class="jOpenModal"
                                           onclick="setUpdateAreaData('${area.areaNo}', 'modal-area')">수정</a>
                                    </div>
                                </td>
                                <td>
                                    <div class="option-btn-center">
                                        <a href="javascript:void(0);" class="jDeleteArea" _no="${area.areaNo}">삭제</a>
                                    </div>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>

            <%-- 페이징 --%>
            <%@include file="/WEB-INF/jsp/tiles_template/web/paging.jsp" %>

        </div>
    </div>
</div>

<div class="modal-wrap" id="modal-area">
    <div class="modal-content">

        <div class="modal-head">
            <h2 class="modal-head-title">구역(룸) 정보 등록/수정</h2>
            <button class="modal-close" onclick="closeModal(this)">닫기</button>
        </div><!-- // modal-head -->

        <div class="modal-body">
            <div class="modal-scroll">
                <div class="scroll-inner">

                    <form id="areaForm">
                        <input type="hidden" id="areaNo" name="areaNo">
                        <ul class="form-list">
                            <li>
                                <label class="form-title">사업장명<span class="req">*</span></label>
                                <div class="form-box form-half">
                                    <div>
                                        <select id="siteNo" name="siteNo">
                                            <option value="">사업장을 선택하세요.</option>
                                            <c:forEach var="site" items="${siteList}">
                                                <option value="${site.siteNo}">${site.siteName}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <label class="form-title">공장<span class="req">*</span></label>
                                <div class="form-box form-half">
                                    <div>
                                        <select id="factoryNo" name="factoryNo"></select>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <label class="form-title">층수<span class="req">*</span></label>
                                <div class="form-box">
                                    <div class="input">
                                        <input type="text" id="floor" name="floor">
                                    </div>
                                </div>
                            </li>
                            <li>
                                <label class="form-title">구역(룸)명<span class="req">*</span></label>
                                <div class="form-box form-checkbtn">
                                    <div class="input">
                                        <input type="text" id="areaName" name="areaName">
                                    </div>
<%--                                    <div class="button">--%>
<%--                                        <button type="button" onclick="checkAreaNameAjax();">중복체크</button>--%>
<%--                                    </div>--%>
                                </div>
                            </li>
<%--                            <li>--%>
<%--                                <label class="form-title">Map 위치</label>--%>
<%--                                <div class="form-box">--%>
<%--                                    <div class="input"><input type="text" placeholder="0 0" id="area-map1"></div>--%>
<%--                                </div>--%>
<%--                            </li>--%>
<%--                            <li>--%>
<%--                                <label class="form-title">Map 사이즈</label>--%>
<%--                                <div class="form-box">--%>
<%--                                    <div class="input"><input type="text" placeholder="400 600" id="area-map2"></div>--%>
<%--                                </div>--%>
<%--                            </li>--%>
                            <li>
                                <label class="form-title">설명</label>
                                <div class="form-box">
                                    <div class="input">
                                        <input type="text" id="comment" name="comment">
                                    </div>
                                </div>
                            </li>
                            <li>
                                <label class="form-title">상태</label>
                                <div class="form-box form-half">
                                    <div>
                                        <select id="status" name="status">
                                            <option value="Y">운영중</option>
                                            <option value="X">미사용</option>
                                            <option value="N">중지</option>
                                        </select>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </form>

                    <hr class="sect-line">
                    <div class="form-btn">
                        <a href="javascript:void(0);" class="line" onclick="closeModal(this)">취소</a>
                        <a href="javascript:void(0);" id="updateBtn" class="jRegBtn jUpdateArea">수정</a>
                        <a href="javascript:void(0);" id="insertBtn" class="jRegBtn jInsertArea">등록</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
