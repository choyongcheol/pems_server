<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>

<script>
    $(document).ready(function () {

        $(".jInsertFactory").click(function () {
            if ($("#factoryForm #factoryName").val().trim() == "") {
                alert("사업장명을 입력하세요");
                $("#factoryForm #factoryName").focus();
                return false;
            }

            $.ajax({
                url: "/setting/insertFactory",
                type: "post",
                dataType: "json",
                data: $("#factoryForm").serialize(),
                success: function (data) {
                    alert(data.returnMessage);
                    if (data.returnCode == 1) {
                        location.reload();
                    }
                }
            })
        })

        $(".jUpdateFactory").click(function () {
            if ($("#factoryForm #factoryName").val().trim() == "") {
                alert("지역명을 입력하세요");
                $("#factoryForm #factoryName").focus();
                return false;
            }

            $.ajax({
                url: "/setting/setUpdateFactory",
                type: "post",
                dataType: "json",
                data: $("#factoryForm").serialize(),
                success: function (data) {
                    alert(data.returnMessage);
                    if (data.returnCode == 1) {
                        location.reload();
                    }
                }
            })
        })

        $(".jDeleteFactory").click(function () {
            if (confirm("정말 삭제하시겠습니까?")) {
                $.ajax({
                    url: "/setting/setDeleteFactory",
                    type: "post",
                    dataType: "json",
                    data: {factoryNo: $(this).attr("_no")}
                    , success: function (data) {
                        alert(data.returnMessage);
                        if (data.returnCode == 1) {
                            location.reload();
                        }
                    }
                })
            }
        })

    })

    function setInsertFactoryData(modalId) {
        $(".jRegBtn#insertBtn").show();
        $(".jRegBtn#updateBtn").hide();

        openModal(modalId) ;
    }

    function setUpdateFactoryData(factoryNo, modalId) {
        $(".jRegBtn#insertBtn").hide();
        $(".jRegBtn#updateBtn").show();

        $("#factoryForm #siteNo").val($("#siteNo_" + factoryNo).val());
        $("#factoryForm #factoryNo").val($("#factoryNo_" + factoryNo).val());
        $("#factoryForm #factoryName").val($("#factoryName_" + factoryNo).val());
        $("#factoryForm #tel").val($("#tel_" + factoryNo).val());
        $("#factoryForm #comment").val($("#comment_" + factoryNo).val());
        $("#factoryForm #status").val($("#status_" + factoryNo).val());

        openModal(modalId) ;
    }

</script>

<div id="wrap">
    <div id="container">
        <div class="content">
            <div class="content-head">
                <h2>- 공장 관리</h2>
            </div>
            <div class="content-option">
                <div class="option-date" id="option-date"></div>
                <ul class="option-data">
                    <li class="title">사업장 상태</li>
                    <li class="auto tabs">
                        <a href="<my:replaceParam name='status' value='' />" class="${empty param.status ? 'on' : ''}">전체</a>
                        <a href="<my:replaceParam name='status' value='Y' />"
                           class="${param.status eq 'Y' ? 'on' : ''}">운영중</a>
                        <a href="<my:replaceParam name='status' value='N' />"
                           class="${param.status eq 'N' ? 'on' : ''}">미사용</a>
                        <a href="<my:replaceParam name='status' value='X' />"
                           class="${param.status eq 'X' ? 'on' : ''}">정지</a>
                    </li>
                </ul>
            </div>

            <div class="content-option">
                <h3 class="option-title">공장 목록</h3>
                <div class="option-btn">
                    <a href="javascript:void(0);" onclick="setInsertFactoryData('modal-factory');">등록</a>
                </div>
            </div>
            <div class="table-wrap">
                <table class="base-table">
                    <thead>
                    <tr>
                        <th scope="col">공장 고유번호</th>
                        <th scope="col">사업장명</th>
                        <th scope="col">공장 이름</th>
                        <th scope="col">대표 연락처</th>
                        <th scope="col">설명</th>
                        <th scope="col">상태</th>
                        <th scope="col" style="width:10%">수정</th>
                        <th scope="col" style="width:10%">삭제</th>
                    </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="factory" items="${list}">
                            <tr>
                                <td>
                                    ${factory.factoryNo}
                                    <input type="hidden" id="factoryNo_${factory.factoryNo}" value="${factory.factoryNo}">
                                </td>
                                <td>
                                        ${factory.siteName}
                                    <input type="hidden" id="siteNo_${factory.factoryNo}" value="${factory.siteNo}">
                                </td>
                                <td>
                                    <span style="color:#009471">
                                        ${factory.factoryName}
                                    </span>
                                    <input type="hidden" id="factoryName_${factory.factoryNo}" value="${factory.factoryName}">
                                </td>
                                <td>
                                    ${factory.tel}
                                    <input type="hidden" id="tel_${factory.factoryNo}" value="${factory.tel}">
                                </td>
                                <td>
                                    ${factory.comment}
                                    <input type="hidden" id="comment_${factory.factoryNo}" value="${factory.comment}">
                                </td>
                                <td>
                                    <c:choose>
                                        <c:when test="${factory.status eq 'Y'}">운영중</c:when>
                                        <c:when test="${factory.status eq 'N'}">미사용</c:when>
                                        <c:when test="${factory.status eq 'X'}">정지</c:when>
                                        <c:otherwise>
                                            ${factory.status}
                                        </c:otherwise>
                                    </c:choose>
                                    <input type="hidden" id="status_${factory.factoryNo}" value="${factory.status}">
                                </td>
                                <td>
                                    <div class="option-btn-center">
                                        <a href="javascript:void(0);" class="jOpenModal"
                                           onclick="setUpdateFactoryData('${factory.factoryNo}', 'modal-factory')">수정</a>
                                    </div>
                                </td>
                                <td>
                                    <div class="option-btn-center">
                                        <a href="javascript:void(0);" class="jDeleteFactory" _no="${factory.factoryNo}">삭제</a>
                                    </div>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>

            <%-- 페이징 --%>
            <%@include file="/WEB-INF/jsp/tiles_template/web/paging.jsp" %>

        </div>
    </div>
</div>

<div class="modal-wrap" id="modal-factory">
    <div class="modal-content">
        <div class="modal-head">
            <h2 class="modal-head-title">공장정보 등록/수정</h2>
            <button class="modal-close" onclick="closeModal(this)">닫기</button>
        </div>

        <div class="modal-body">
            <div class="modal-scroll">
                <div class="scroll-inner">

                    <form id="factoryForm">
                        <input type="hidden" id="factoryNo" name="factoryNo">
                        <ul class="form-list">
                            <li>
                                <label class="form-title">사업장명<span class="req">*</span></label>
                                <div class="form-box form-half">
                                    <div>
                                        <select id="siteNo" name="siteNo" title="">
                                            <option value="">사업장을 선택하세요.</option>
                                            <c:forEach var="site" items="${siteList}">
                                                <option value="${site.siteNo}">${site.siteName}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>
                            </li>

                            <li>
                                <label class="form-title">공장명<span class="req">*</span></label>
                                <div class="form-box form-checkbtn">
                                    <div class="input">
                                        <input type="text" id="factoryName" name="factoryName">
                                    </div>
<%--                                    <div class="button">--%>
<%--                                        <button type="button" onclick="checkNameAjax();">중복체크</button>--%>
<%--                                    </div>--%>
                                </div>
                            </li>

                            <li>
                                <label class="form-title">전화번호</label>
                                <div class="form-box form-tel">
                                    <div class="tel">
                                        <input type="text" id="tel" name="tel">
                                    </div>
                                </div>
                            </li>

                            <%--                        <li>--%>
                            <%--                            <label class="form-title">Map 사이즈</label>--%>
                            <%--                            <div class="form-box">--%>
                            <%--                                <div class="input"><input type="text" placeholder="400 600" id="map-size"></div>--%>
                            <%--                            </div>--%>
                            <%--                        </li>--%>

                            <li>
                                <label class="form-title">설명</label>
                                <div class="form-box">
                                    <div class="input">
                                        <input type="text" id="comment" name="comment">
                                    </div>
                                </div>
                            </li>

                            <li>
                                <label class="form-title">상태</label>
                                <div class="form-box form-half">
                                    <div>
                                        <select id="status" name="status">
                                            <option value="Y">운영중</option>
                                            <option value="N">미사용</option>
                                            <option value="X">정지</option>
                                        </select>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </form>

                    <hr class="sect-line">
                    <div class="form-btn">
                        <a href="javascript:void(0);" class="line" onclick="closeModal(this)">취소</a>
                        <a href="javascript:void(0);" id="updateBtn" class="jRegBtn jUpdateFactory">수정</a>
                        <a href="javascript:void(0);" id="insertBtn" class="jRegBtn jInsertFactory">등록</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>