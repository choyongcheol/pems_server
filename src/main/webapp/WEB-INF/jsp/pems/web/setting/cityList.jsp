<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<script>

    $(document).ready(function () {

        $(".jInsertCity").click(function() {
            if ($("#cityForm #cityName").val().trim() == "") {
                alert("지역명을 입력하세요") ;
                $("#cityForm #cityName").focus() ;
                return false ;
            }

            $.ajax({
                url: "/setting/insertCity",
                type: "post",
                dataType: "json",
                data: $("#cityForm").serialize(),
                success: function (data) {
                    alert(data.returnMessage);
                    if (data.returnCode == 1) {
                        location.reload() ;
                    }
                }
            })
        })

        $(".jUpdateCity").click(function() {
            if ($("#cityForm #cityName").val().trim() == "") {
                alert("지역명을 입력하세요") ;
                $("#cityForm #cityName").focus() ;
                return false ;
            }

            $.ajax({
                url: "/setting/setUpdateCity",
                type: "post",
                dataType: "json",
                data: $("#cityForm").serialize(),
                success: function (data) {
                    alert(data.returnMessage);
                    if (data.returnCode == 1) {
                        location.reload() ;
                    }
                }
            })
        })

        $(".jDeleteCity").click(function() {
            if (confirm("정말 삭제하시겠습니까?")) {
                $.ajax({
                    url: "/setting/setDeleteCity",
                    type: "post",
                    dataType: "json",
                    data: { cityNo: $(this).attr("_no") }
                    , success: function (data) {
                        alert(data.returnMessage);
                        if (data.returnCode == 1) {
                            location.reload() ;
                        }
                    }
                })
            }

        })

    })

    function setInsertCityData(modalId) {
        $(".jRegBtn#insertBtn").show() ;
        $(".jRegBtn#updateBtn").hide() ;
        openModal(modalId) ;
    }

    function setUpdateCityData(cityNo, modalId) {
        $(".jRegBtn#insertBtn").hide() ;
        $(".jRegBtn#updateBtn").show() ;
        $("#cityForm #cityNo").val($("#cityNo_" + cityNo).text()) ;
        $("#cityForm #cityName").val($("#cityName_" + cityNo).text()) ;
        $("#cityForm #countryName").val($("#countryName_" + cityNo).text()) ;

        openModal(modalId) ;
    }

</script>


<div id="wrap">
    <div id="container">
        <div class="content">
            <div class="content-head">
                <h2>- 지역 관리</h2>
            </div>

            <div class="content-option">
                <div class="option-date" id="option-date"></div>
            </div>

            <div class="content-option">
                <h3 class="option-title">지역 목록</h3>
                <div class="option-btn">
                    <a href="javascript:void(0);" onclick="setInsertCityData('modal-city');">등록</a>
                </div>
            </div>

            <div class="table-wrap">
                <table class="base-table">
                    <thead>
                        <tr>
                            <th scope="col" style="width:10%">고유번호</th>
                            <th scope="col">국가</th>
                            <th scope="col">지역명</th>
                            <th scope="col" style="width:10%">수정</th>
                            <th scope="col" style="width:10%">삭제</th>
                        </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="city" items="${list}" varStatus="loop">
                        <tr>
                            <td id="cityNo_${city.cityNo}">${city.cityNo}</td>
                            <td id="countryName_${city.cityNo}">${city.countryName}</td>
                            <td id="cityName_${city.cityNo}">${city.cityName}</td>
                            <td>
                                <div class="option-btn-center">
                                    <a href="javascript:void(0);" onclick="setUpdateCityData('${city.cityNo}', 'modal-city')">수정</a>
                                </div>
                            </td>
                            <td>
                                <div class="option-btn-center">
                                    <a href="javascript:void(0);" class="jDeleteCity" _no="${city.cityNo}">삭제</a>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
            <%-- 페이징 --%>
            <%@include file="/WEB-INF/jsp/tiles_template/web/paging.jsp"%>
        </div>
    </div>
</div>

<div class="modal-wrap" id="modal-city">
    <div class="modal-content">
        <div class="modal-head">
            <h2 class="modal-head-title">지역 등록</h2>
            <button class="modal-close" onclick="closeModal(this)">닫기</button>
        </div>

        <div class="modal-body">
            <div class="modal-scroll">
                <div class="scroll-inner">

                    <form id="cityForm">
                        <input type="hidden" id="cityNo" name="cityNo">
                        <ul class="form-list">
                            <li>
                                <label class="form-title">나라명</label>
                                <div class="form-box">
                                    <div class="input">
                                        <input type="text" id="countryName" name="countryName">
                                    </div>
                                </div>
                            </li>
                            <li>
                                <label class="form-title">지역명</label>
                                <div class="form-box">
                                    <div class="input">
                                        <input type="text" id="cityName" name="cityName">
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </form>
                    <hr class="sect-line">
                    <div class="form-btn">
                        <a href="javascript:void(0);" class="line" onclick="closeModal(this)">취소</a>
                        <a href="javascript:void(0);" id="updateBtn" class="jRegBtn jUpdateCity">수정</a>
                        <a href="javascript:void(0);" id="insertBtn" class="jRegBtn jInsertCity">등록</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>