<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<script src="<c:url value="/resource/js/chartjs.js"/>"></script>


<%--    <script src="/assets/js/common.js" type="application/javascript"></script>--%>

<script>
    let showDefaultId;
    let showDefaultVisible;
    let result = null;

    let systemStausGraphV
    let channelGraphV

    let dayWorkGraphV
    let monthWorkGraphV
    let alarmGraphV

    const CHART_COLORS = {
        red: 'rgb(255, 99, 132)',
        orange: 'rgb(255, 159, 64)',
        yellow: 'rgb(255, 205, 86)',
        green: 'rgb(75, 192, 192)',
        blue: 'rgb(54, 162, 235)',
        purple: 'rgb(153, 102, 255)',
        grey: 'rgb(201, 203, 207)'
    };

    $(window).resize(function() {
        modalScroll();
    })

    $(document).ready(function () {

        $("#em_month").text(getCommonMonth(new Date()));
        $("#em_year").text(getCommonFullYear(new Date()));
        // search();
        // modalScroll();







    }) ;
/*
        onChangeSite();
        let startDay = new Date();
        startDay.setDate(startDay.getDate() - 7)
        let endDay = new Date();


        $("#default").change(function () {
            if ($("#default").is(":checked")) {
                for (let i = 0; i < result.default_count; i++) {
                    if (showDefaultVisible[i] == 'Y') {
                        $('#table_list').datagrid('hideColumn', showDefaultId[i]);
                    }
                }
            } else {
                for (let i = 0; i < result.default_count; i++) {
                    if (showDefaultVisible[i] == 'Y') {
                        $('#table_list').datagrid('showColumn', showDefaultId[i]);
                    }
                }
            }
        });

        $("#module").change(function () {
            let modField = Number(result.default_count) + Number(result.module_count);

            if ($("#module").is(":checked")) {
                for (let i = result.default_count; i < modField; i++) {
                    if (showDefaultVisible[i] == 'Y') {
                        $('#table_list').datagrid('hideColumn', showDefaultId[i]);
                    }
                }
            } else {
                for (let i = result.default_count; i < modField; i++) {
                    if (showDefaultVisible[i] == 'Y') {
                        $('#table_list').datagrid('showColumn', showDefaultId[i]);
                    }
                }
            }
        });
    });

    function onChangeNation() {
        let now = new Date()
        let params = {
            "nation_area_seq": $('#nation_area_select option:selected').val(),
            "month": convertDateYYYYMM(now),
            "year": now.getFullYear().toString()
        }
        console.log(params)
        ajaxCall('/module/getModuleListJson', params, function (data) {
            let running = 0
            let stop = 0
            data.list.forEach(function (value, i) {
                if (value.status == "Y") {
                    running += 1
                } else if (value.status == "N") {
                    stop += 1
                }
            })
            systemStausGraph(running, stop, data.list.length);
        });

        ajaxCall('/channel/getChannelStateForMain', params, function (data) {
            let running = 0
            let idle = 0
            let troble = 0
            data.list.forEach(function (value, i) {
                if (value.state == "RUN") {
                    running += 1
                } else if (value.state == "Idle") {
                    idle += 1
                } else {
                    troble += 1
                }
            })
            channelGraph(running, idle, troble);
        });

        let startDate = new Date()
        startDate.setDate(startDate.getDate() - 7)
        let endDate = new Date()
        endDate.setDate(endDate.getDate() + 1)


        ajaxCall('/operation/getOperationDayGraph', params, function (data) {
            if (data.success) {
                let dayMap = {
                    days: [],
                    workValues: [],
                    noWorkValues: []
                }
                let monthMap = {
                    month: [],
                    workValues: [],
                    noWorkValues: []
                }
                console.log(data)
                data.day_list.forEach(function (value) {
                    dayMap["days"].push(parseInt(value.day))
                    dayMap["workValues"].push(value.rate)
                    dayMap["noWorkValues"].push(100 - value.rate)
                })
                data.month_list.forEach(function (value) {
                    monthMap["month"].push(value.month)
                    monthMap["workValues"].push(value.rate)
                    monthMap["noWorkValues"].push(100 - value.rate)
                })
                workOnDayGraph(dayMap);
                workOnMonthGraph(monthMap)
            }
        });


        let params2 = {
            "startDay": startDate,
            "endDay": endDate,
            "nation_area_seq": $('#nation_area_select option:selected').val()
        }


        ajaxCall('/history/getAlarmHistoryListJson', params2, function (data) {
            let emg = 0
            let ch = 0
            $('#alarm_tbody').empty()
            data.list.forEach(function (value, i) {
                if (i < 5) {
                    addTbody(value)
                }
                if (value.alarm_div != "EMG") {
                    ch += 1
                } else {
                    emg += 1
                }
            })
            alarmGraph(ch, emg)
        });

    }

    function onChangeSite() {
        let now = new Date()
        let params = {
            "site_seq": $('#site_select option:selected').val(),
            "month": convertDateYYYYMM(now),
            "year": now.getFullYear().toString()
        }
        console.log(params)
        ajaxCall('/module/getModuleListJson', params, function (data) {
            let running = 0
            let stop = 0
            data.list.forEach(function (value, i) {
                if (value.status == "Y") {
                    running += 1
                } else if (value.status == "N") {
                    stop += 1
                }
            })
            systemStausGraph(running, stop, data.list.length);
        });


        ajaxCall('/channel/getChannelStateForMain', params, function (data) {
            let running = 0
            let idle = 0
            let troble = 0
            data.list.forEach(function (value, i) {
                if (value.state == "RUN") {
                    running += 1
                } else if (value.state == "Idle") {
                    idle += 1
                } else {
                    troble += 1
                }
            })
            channelGraph(running, idle, troble);
        });

        let startDate = new Date()
        startDate.setDate(startDate.getDate() - 7)
        let endDate = new Date()
        endDate.setDate(endDate.getDate() + 1)

        ajaxCall('/operation/getOperationDayGraph', params, function (data) {
            if (data.success) {
                let dayMap = {
                    days: [],
                    workValues: [],
                    noWorkValues: []
                }
                let monthMap = {
                    month: [],
                    workValues: [],
                    noWorkValues: []
                }

                data.day_list.forEach(function (value) {
                    dayMap["days"].push(parseInt(value.day))
                    dayMap["workValues"].push(value.rate)
                    dayMap["noWorkValues"].push(100 - value.rate)
                })
                data.month_list.forEach(function (value) {
                    monthMap["month"].push(value.month)
                    monthMap["workValues"].push(value.rate)
                    monthMap["noWorkValues"].push(100 - value.rate)
                })

                workOnDayGraph(dayMap);
                workOnMonthGraph(monthMap)
            }
        });


        let params2 = {
            "startDay": startDate,
            "endDay": endDate,
            "site_seq": $('#site_select option:selected').val()
        }

        ajaxCall('/history/getAlarmHistoryListJson', params2, function (data) {
            let emg = 0
            let ch = 0
            $('#alarm_tbody').empty()
            data.list.forEach(function (value, i) {
                if (i < 5) {
                    addTbody(value)
                }
                if (value.alarm_div != "EMG") {
                    ch += 1
                } else {
                    emg += 1
                }
            })
            alarmGraph(ch, emg)
        });


        //알림 현황

        //왼쪽하단
    }

    function addTbody(value) {
        let html = "<tr>"
        html += "<td>" + makeVeryShortWord(value.si_site_name) + "</td>"
        html += "<td>" + makeVeryShortWord(value.ai_area_name) + "</td>"
        html += "<td>" + makeVeryShortWord(value.module_name) + "</td>"
        html += "<td>" + makeVeryShortWord(value.send_title) + "</td>"
        html += "</tr>"

        $('#alarm_tbody').append(html)
    }

    function search() {
        $('.channel').removeClass('hide');
        let columnNameList;
        let columnIdList;
        let isVisible;

        $.ajax({
            url: "/module/getModuleColumList?1=1&type=channel",
            type: "GET",
            cache: false,
            success: function (data) {
                result = data.columInfo;
                if (result.get_columns_id != null) {
                    columnIdList = result.get_columns_id.split(',');
                    columnNameList = result.get_columns_name.split(',');
                    isVisible = result.is_visible.split(',');
                }

                showDefaultId = columnIdList;
                showDefaultVisible = isVisible;

                for (let i = 0; i < result.length; i++) {
                    columnNameList.push(result[i].get_columns_name);
                    columnIdList.push(result[i].get_columns_id);
                }
            },
            complete: function (data) {
                let cols = [];
                let colsPx = 100;
                for (let i = 0; i < columnNameList.length; i++) {
                    if (columnNameList[i].length > 5) colsPx = 130;
                    else if (columnNameList[i].length < 5) colsPx = 80;

                    if (i >= 0 && i < result.default_count) {
                        if (isVisible[i] == 'Y') cols.push({
                            field: columnIdList[i],
                            title: columnNameList[i],
                            sortable: true,
                            width: colsPx,
                            align: 'center'
                        });
                    } else {
                        if (isVisible[i] == 'Y') cols.push({
                            field: columnIdList[i],
                            title: columnNameList[i],
                            sortable: true,
                            width: colsPx,
                            align: 'center'
                        });
                    }
                    purchaseField(columnIdList[i], columnNameList[i], isVisible[i], i, result.default_count, result.module_count); // 컬럼아이디, 컬럼명, 보이는 여부, 인덱스, 기본컬럼수, 설비컬럼수
                }

                let colArr = [cols];

                _grid = new pagingGrid('table_list', '/channel/getChannelListJson', function () {
                    let data = {
                        site_seq: $('#site_seq').val(),
                        factory_seq: $('#factory_seq').val(),
                        area_seq: $('#area_seq').val(),
                        module_seq: $('#module_seq').val(),
                        manager_seq: $('#manager_seq').val(),
                        searchType: $('#searchType').val(),
                        searchValue: $('#searchValue').val(),
                        state: $('#state').val(),
                        type: $('#type').val(),
                        code: $('#code').val()
                    };
                    return data;
                });

                $('#table_list').datagrid({
                    emptyMsg: "데이터가 없습니다.",
                    columns: colArr,
                    onLoadSuccess: function () {
                        let modField = Number(result.default_count) + Number(result.module_count);
                        $('.datagrid-header-row td').css('background', '#BFBFBF');

                        for (let i = 0; i < Number(result.default_count); i++) {
                            if (isVisible[i] == 'Y') $('.datagrid-header-row td[field=' + columnIdList[i] + ']').css('background', '#FBE5D6');
                        }

                        for (let i = Number(result.default_count); i < modField; i++) {
                            if (isVisible[i] == 'Y') $('.datagrid-header-row td[field=' + columnIdList[i] + ']').css('background', '#F2F2F2');
                        }
                    }
                });
            }
        });
    }

    function purchaseField(colId, colName, isVisible, index, def_count, mod_count) {
        let modField = Number(def_count) + Number(mod_count);
        let html = "";
        html += '<tr>';
        html += '	<td class="val">';
        html += '		<input type="hidden" class="colId"     id="colId' + index + '"     name="colId' + index + '"     value="' + colId + '">';
        html += '		<input type="hidden" class="isVisible" id="isVisible' + index + '" name="isVisible' + index + '" value="' + isVisible + '">';
        if (isVisible == 'Y') {
            html += '		<input type="checkbox" class="colName" id="colName' + index + '" name="colName' + index + '" value="' + colName + '" checked>';
        } else {
            html += '		<input type="checkbox" class="colName" id="colName' + index + '" name="colName' + index + '" value="' + colName + '">';
        }
        html += '		<label for="choice' + index + '">' + colName + ' <input type="radio" id="choice' + index + '" name="choice"/></label>';
        html += '	</td>';
        html += '</tr>';

        if (index < def_count) {
            $("#defaultInfoTB").append(html);
        } else if (def_count <= index && index < modField) {
            $("#moduleInfoTB").append(html);
        } else {
            $("#channelInfoTB").append(html);
        }
    }

    function openModal() {
        $('#modal').window('open');
    }

    function moveUp(el) {
        let $tr = $('input[name="choice"]:checked').parent().parent().parent();
        $tr.prev().before($tr);
    }

    function moveDown(el) {
        let $tr = $('input[name="choice"]:checked').parent().parent().parent();
        $tr.next().after($tr);
    }

    function saveColumsInfo() {
        let colIdList = $("#defaultInfoTB tr .val .colId, #moduleInfoTB tr .val .colId, #channelInfoTB tr .val .colId");
        let colId = "";
        $.each(colIdList, function (index, item) {
            if (index != 0) colId += ',';
            colId += $(item).val();
        });
        $('#setColumsId').val(colId);

        let colNameList = $("#defaultInfoTB tr .val .colName, #moduleInfoTB tr .val .colName, #channelInfoTB tr .val .colName");
        let isVisible = "";
        let colName = "";
        $.each(colNameList, function (index, item) {
            if (index != 0) {
                colName += ',';
                isVisible += ',';
            }
            if ($(item).is(":checked") == true) {
                isVisible += 'Y';
            } else {
                isVisible += 'N';
            }
            colName += $(item).val();
        });
        $('#setColumsVisible').val(isVisible);
        $('#setColumsName').val(colName);

        let data = {
            'type': 'channel',
            'get_columns_id': $('#setColumsId').val(),
            'get_columns_name': $('#setColumsName').val(),
            'is_visible': $('#setColumsVisible').val()
        };

        ajaxCall('/user/setColumns', data, function (data) {
            if (data.success) {
                location.reload(true);
            }
        });
    }

    function systemStausGraph(work, nowork, total) {

        $('#module_total_count').html(total)
        const data = {
            labels: ['가동', '비가동'],
            datasets: [{
                label: 'Dataset 1',
                data: [work, nowork],
                backgroundColor: [CHART_COLORS.green, CHART_COLORS.red],
            }
            ]
        };

        const config = {
            type: 'doughnut',
            data: data,
            options: {
                responsive: true,
                plugins: {
                    legend: {
                        position: 'right',
                    }
                }
            }
        };
        let ctx = document.getElementById("systemStausGraph");
        if (systemStausGraphV == null) {
            systemStausGraphV = new Chart(ctx, config);
        } else {
            systemStausGraphV.data = data
            systemStausGraphV.update()
        }

    }

    function channelGraph(work, idle, tro) {

        const data = {
            labels: ['Runing', 'Idle', 'Trouble'],
            datasets: [{
                label: 'Dataset 1',
                data: [work, idle, tro],
                backgroundColor: [CHART_COLORS.green, CHART_COLORS.yellow, CHART_COLORS.red],
            }
            ]
        };

        const config = {
            type: 'doughnut',
            data: data,
            options: {
                responsive: true,
                plugins: {
                    legend: {
                        position: 'right',
                    }
                }
            }
        };

        let ctx = document.getElementById("channelGraph");
        if (channelGraphV == null) {
            channelGraphV = new Chart(ctx, config);
        } else {
            channelGraphV.data = data
            channelGraphV.update()
        }
        $('#channel_count').html(work + idle + tro)
    }

    function alarmGraph(ch, emg) {


        const data = {
            labels: ['EMG 알림', '채널 알림'],
            datasets: [
                {
                    data: [ch, emg],
                    backgroundColor: [CHART_COLORS.red],
                }
            ]
        };

        const config = {
            type: 'bar',
            data: data,
            options: {
                responsive: false,
                plugins: {
                    legend: {
                        display: false
                    }
                },
                scales: {
                    x: {
                        stacked: true,
                    },
                    y: {
                        stacked: true,
                        ticks: {
                            stepSize: 1
                        }
                    }
                }
            }
        };
        let ctx = document.getElementById("alarmGraph");
        if (alarmGraphV == null) {
            alarmGraphV = new Chart(ctx, config);
        } else {
            alarmGraphV.data = data
            alarmGraphV.update()
        }


    }

    function workOnDayGraph(map) {

        const data = {
            labels: map["days"],
            datasets: [
                {
                    label: '비가동율',
                    data: map["noWorkValues"],
                    backgroundColor: [CHART_COLORS.red],
                    fill: true
                },
                {
                    label: '가동율',
                    data: map["workValues"],
                    backgroundColor: [CHART_COLORS.green],
                    fill: true
                }
            ]
        };

        const config = {
            type: 'line',
            data: data,
            options: {
                responsive: false,
                plugins: {
                    legend: {
                        position: 'top',
                    }
                },
                scales: {
                    y: {
                        min: 0,
                        max: 100,
                        ticks: {
                            stepSize: 1
                        }
                    }
                }
            }
        };
        let ctx = document.getElementById("workOnDayGraph");


        if (dayWorkGraphV == null) {
            dayWorkGraphV = new Chart(ctx, config);
        } else {
            dayWorkGraphV.data = data
            dayWorkGraphV.update()
        }
    }

    function workOnMonthGraph(map) {


        const data = {
            labels: map["month"],
            datasets: [
                {
                    label: '비가동율',
                    data: map["noWorkValues"],
                    backgroundColor: [CHART_COLORS.red],
                    fill: true
                },
                {
                    label: '가동율',
                    data: map["workValues"],
                    backgroundColor: [CHART_COLORS.green],
                    fill: true
                }
            ]
        };

        const config = {
            type: 'line',
            data: data,
            options: {
                responsive: false,
                plugins: {
                    legend: {
                        position: 'top',
                    }
                },
                scales: {
                    y: {
                        min: 0,
                        max: 100,
                        ticks: {
                            stepSize: 1
                        }
                    }
                }
            }
        };
        let ctx = document.getElementById("workOnMonthGraph");


        if (monthWorkGraphV == null) {
            monthWorkGraphV = new Chart(ctx, config);
        } else {
            monthWorkGraphV.data = data
            monthWorkGraphV.update()
        }
    }

    const commonNationAreaAjax = function () {

        let nationAreaSeq = $('#nation_area_select option:selected').val();
        if (nationAreaSeq == 0) {
            onChangeSite();
            return
        }
        let data = {
            "nationAreaSeq": nationAreaSeq
        };

        $.ajax({

            type: 'POST',
            url: '/api/common/site/list',
            contentType: 'application/json',
            dataType: 'json',
            data: JSON.stringify(data)

        }).done(function (rsp) {
            if (rsp.success) {

                let html = '<option value="">선택해주세요.</option>';
                $.each(rsp.response, function (idx, x) {
                    html += '<option value="' + x.siteSeq + '">' + x.siteName + '</option>';
                });
                $("#site_select").html(html);
                onChangeNation()

            }
        }).fail(function (e) {
            console.log(e);
            let json = $.parseJSON(e.responseText);
            alert(json.error.message);
        });
    }

    function convertDateYYYYMM(date) {
        let yyyy = date.getFullYear().toString();
        let mm = (date.getMonth() + 1).toString();
        let mmChars = mm.split('');
        return yyyy + '-' + (mmChars[1] ? mm : "0" + mmChars[0]);
    }
*/

</script>


<div id="container">

    <div class="content">

        <div class="content-option">
            <div class="option-date" id="option-date"></div>

            <dl class="option-data">
                <dt>지역 구분</dt>
                <dd>
                    <select id="city_info_select" name="" title="" onchange="commonNationAreaAjax()">
                        <option value="">전체 지역</option>
                        <c:forEach var="city" items="${cityList}">
                            <option value="${city.cityNo}">${city.cityName}</option>
                        </c:forEach>
                    </select>
                </dd>
                <dt>사업장 구분</dt>
                <dd>
                    <select id="site_select" onchange="onChangeSite();">
                        <option value="">선택해주세요.</option>
                    </select>
                </dd>
            </dl>
        </div>
        <!-- // content-option -->

        <div class="content-chart">

            <div class="content-chart-box">
                <div class="inner-box">
                    <h3 class="box-head">설비현황 <em id="module_total_count">0</em></h3>
                    <div class="box-body">
                        <canvas data-check='false' id='systemStausGraph'/>
                    </div>
                </div>
            </div>

            <div class="content-chart-box">
                <div class="inner-box">
                    <h3 class="box-head">채널 현황 <em id="channel_count">0</em></h3>
                    <div class="box-body">
                        <canvas data-check='false' id='channelGraph'/>
                    </div>
                </div>
            </div>

            <div class="content-chart-box">
                <div class="inner-box">
                    <h3 class="box-head">알림 현황</h3>
                    <div class="box-body">
                        <canvas data-check='false' id='alarmGraph' class="lineGraph" width="330" height="250"/>
                    </div>
                </div>
            </div>

            <div class="content-chart-box">
                <div class="inner-box">
                    <h3 class="box-head">일별 가동율 그래프 <em id="em_month">x월</em></h3>
                    <div class="box-body">
                        <canvas data-check='false' id='workOnDayGraph' class="lineGraph" width="330" height="250"/>
                    </div>
                </div>
            </div>

            <div class="content-chart-box">
                <div class="inner-box">
                    <h3 class="box-head">월별 가동율 그래프 <em id="em_year">202x년</em></h3>
                    <div class="box-body">
                        <canvas data-check='false' id='workOnMonthGraph' class="lineGraph" width="330"
                                height="250"/>
                    </div>
                </div>
            </div>

            <div class="content-chart-box">
                <div class="inner-box">
                    <h3 class="box-head">알림 최근 리스트</h3>
                    <div class="box-body">
                        <table class="base-table">
                            <thead>
                            <tr>
                                <th scope="col">사업장</th>
                                <th scope="col">구역(룸)</th>
                                <th scope="col">설비명</th>
                                <th scope="col">오류명</th>
                            </tr>
                            </thead>
                            <tbody id="alarm_tbody">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
        <!-- // content-sect -->

    </div>

</div>