<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<script>
    $(document).ready(function () {

        // 모듈 선택시 채널리스트 조회
        $(".jSelectModule").click(function() {
            let moduleNo = $(this).attr("_no") ;

            setChannelTableTbody(moduleNo) ;
            setOvenTableTbody(moduleNo) ;
            setChillerTableTbody(moduleNo) ;
        })

        $(document).on("click", "#tbody-channel tr", function() {
            let channelNo = $(this).attr("_no") ;

            setAuxTableTbody(channelNo) ;
            setCanTableTbody(channelNo) ;
        })

        getUserTableColumns(USER_TABLE_NAME_CHANNEL);

    })

    // 채널 목록 헤더작성
    function setChannelTableThead() {
        let html = "" ;
        let nameList = localStorage[CHANNEL_COLUMN_NAME_ARRAY].split(",") ;
        let visibleList = localStorage[CHANNEL_VISIBLE_ARRAY].split(",");

        $.each(nameList, function (idx, key) {
            if ("Y" == visibleList[idx]) {
                html += '<th>' + key + '</th>';
            }
        })

        $("#table_channel_thead").html(html);
    }

    // 채널 목록
    function setChannelTableTbody(moduleNo) {
        setChannelTableThead() ;

        $.ajax({
            url: "/module/channelList",
            type: "post",
            dataType: "json",
            data: {moduleNo: moduleNo},
            success: function (data) {
                let html = '';
                let columnIdList = localStorage[CHANNEL_COLUMN_ID_ARRAY].split(",");
                let visibleList = localStorage[CHANNEL_VISIBLE_ARRAY].split(",");

                $.each(data.list, function (idx, obj) {
                    let stepDay = obj.stepDay > 0 ? ("D" + obj.stepDay + " ") : "" ;
                    let totalDay = obj.totalDay > 0 ? ("D" + obj.totalDay + " ") : "" ;

                    html += '<tr _no="' + obj.channelNo + '">';
                    if ("Y" == visibleList[columnIdList.indexOf("channel_no")]) {
                        html += '<td>' + obj.channelNo + '</td>';
                    }
                    if ("Y" == visibleList[columnIdList.indexOf("avg_voltage")]) {
                        html += '<td>' + obj.avgVoltage + '</td>';
                    }
                    if ("Y" == visibleList[columnIdList.indexOf("avg_current")]) {
                        html += '<td>' + obj.avgCurrent + '</td>';
                    }
                    if ("Y" == visibleList[columnIdList.indexOf("aux_count")]) {
                        html += '<td>' + obj.auxCount + '</td>';
                    }
                    if ("Y" == visibleList[columnIdList.indexOf("can_count")]) {
                        html += '<td>' + obj.canCount + '</td>';
                    }
                    if ("Y" == visibleList[columnIdList.indexOf("step_time")]) {
                        html += '<td>' + stepDay + (obj.stepTime / 100).toHHMMSS() + '</td>';
                    }
                    if ("Y" == visibleList[columnIdList.indexOf("total_time")]) {
                        html += '<td>' + totalDay + (obj.totalTime / 100).toHHMMSS() + '</td>';
                    }
                    if ("Y" == visibleList[columnIdList.indexOf("step_no")]) {
                        html += '<td>' + obj.stepNo + '</td>';
                    }
                    if ("Y" == visibleList[columnIdList.indexOf("total_cycle_num")]) {
                        html += '<td>' + obj.totalCycleNum + '</td>';
                    }
                    if ("Y" == visibleList[columnIdList.indexOf("relay_num")]) {
                        html += '<td>' + obj.relayNum + '</td>';
                    }
                    if ("Y" == visibleList[columnIdList.indexOf("last_calibration_date")]) {
                        html += '<td>' + obj.lastCalibrationDate + '</td>';
                    }
                    if ("Y" == visibleList[columnIdList.indexOf("state")]) {
                        html += '<td>' + obj.state + '</td>';
                    }

                    html += '</tr>';
                })

                $("#tbody-channel").html(html);
                $("#table-channel-list").attr("_no", moduleNo);
                $("#table-channel-list").show();
            }
        })
    }

    // Oven 리스트
    function setOvenTableTbody(moduleNo) {

        $.ajax({
            url: "/module/getOvenList",
            type: "post",
            dataType: "json",
            data: {moduleNo: moduleNo},
            success: function (data) {
                let html = '';

                $.each(data.list, function (idx, obj) {
                    html += '<tr>';
                    html += '<td>' + obj.ovenRoomNumber + '</td>';
                    html += '<td>' + obj.assignChannelNum + '</td>';
                    html += '<td>' + obj.almCode + '</td>';
                    html += '<td>' + obj.ovenMode + '</td>';
                    html += '<td>' + obj.pvHumi + '</td>';
                    html += '<td>' + obj.pvTemp + '</td>';
                    html += '<td>' + obj.svHumi + '</td>';
                    html += '<td>' + obj.svTemp + '</td>';
                    html += '<td>' + obj.state + '</td>';
                    html += '</tr>';
                })

                $("#tbody-oven").html(html);
                $("#table-oven-list").show();
            }
        })
    }

    // Chiller 리스트
    function setChillerTableTbody(moduleNo) {

        $.ajax({
            url: "/module/getChillerList",
            type: "post",
            dataType: "json",
            data: {moduleNo: moduleNo},
            success: function (data) {
                let html = '';

                $.each(data.list, function (idx, obj) {
                    html += '<tr>';
                    html += '<td>' + obj.chillerRoomNumber + '</td>';
                    html += '<td>' + obj.assignChannelNum + '</td>';
                    html += '<td>' + obj.flowAlmCode + '</td>';
                    html += '<td>' + obj.flowMode + '</td>';
                    html += '<td>' + obj.flowState + '</td>';
                    html += '<td>' + obj.pvFlow + '</td>';
                    html += '<td>' + obj.pvTemp + '</td>';
                    html += '<td>' + obj.svFlow + '</td>';
                    html += '<td>' + obj.svTemp + '</td>';
                    html += '<td>' + obj.tempAlmCode + '</td>';
                    html += '<td>' + obj.tempMode + '</td>';
                    html += '<td>' + obj.tempState + '</td>';
                    html += '</tr>';
                })

                $("#tbody-chiller").html(html);
                $("#table-chiller-list").show();
            }
        })
    }

    // Aux 리스트
    function setAuxTableTbody(channelNo) {

        $.ajax({
            url: "/module/getAuxList",
            type: "post",
            dataType: "json",
            data: {channelNo: channelNo},
            success: function (data) {
                let html = '';

                $.each(data.list, function (idx, obj) {
                    html += '<tr>';
                    html += '<td>' + obj.auxItem + '</td>';
                    html += '<td>' + obj.auxName + '</td>';
                    html += '<td>' + obj.auxValue + '</td>';
                    html += '<td>' + obj.safetyMax + '</td>';
                    html += '<td>' + obj.safetyMin + '</td>';
                    html += '<td>' + obj.divisionCode1 + '</td>';
                    html += '</tr>';
                })

                $("#tbody-aux").html(html);
                $("#table-aux-list").show();
            }
        })
    }

    // Can 리스트
    function setCanTableTbody(channelNo) {

        $.ajax({
            url: "/module/getCanList",
            type: "post",
            dataType: "json",
            data: {channelNo: channelNo},
            success: function (data) {
                let html = '';

                $.each(data.list, function (idx, obj) {
                    html += '<tr>';
                    html += '<td>' + obj.canNo + '</td>';
                    html += '<td>' + obj.canName + '</td>';
                    html += '<td>' + obj.canValue + '</td>';
                    html += '<td>' + obj.canId + '</td>';
                    html += '<td>' + obj.safetyMax + '</td>';
                    html += '<td>' + obj.safetyMin + '</td>';
                    html += '<td>' + obj.divisionCode1 + '</td>';
                    html += '</tr>';
                })

                $("#tbody-can").html(html);
                $("#table-can-list").show();
            }
        })
    }

    // 필드 설정파일 열기
    function openFieldSettingPopup(modalId) {
        let visibleList = localStorage[CHANNEL_VISIBLE_ARRAY].split(",");
        let columnIdList = localStorage[CHANNEL_COLUMN_ID_ARRAY].split(",");
        let columnNameList = localStorage[CHANNEL_COLUMN_NAME_ARRAY].split(",");
        let html = "" ;

        $.each(columnNameList, function(idx, name) {
            html += '<div class="check"><input type="checkbox" id="' ;
            html += columnIdList[idx] + '" name="checkbox" ' + ("Y" == visibleList[idx] ? "checked" : "") + '><label for="' ;
            html += columnIdList[idx] + '">' + name + '</label></div>';
        })

        $("#check_table").html(html) ;
        openModal(modalId)
    }

    // 필드 설정 저장
    function updateChannelFieldSetting() {
        let visibleList = localStorage[CHANNEL_VISIBLE_ARRAY].split(",");
        $("#check_table [type='checkbox']").each(function(idx) {
            visibleList[idx] = $(this).is(":checked") ? "Y" : "N" ;
        })
        localStorage.setItem(CHANNEL_VISIBLE_ARRAY, visibleList) ;

        setUserTableColumns(USER_TABLE_NAME_CHANNEL, localStorage[CHANNEL_COLUMN_ID_ARRAY], localStorage[CHANNEL_COLUMN_NAME_ARRAY], localStorage[CHANNEL_VISIBLE_ARRAY]) ;

        setChannelTableTbody($("#table-channel-list").attr("_no")) ;
        closeModalId("modal-fieldSetting") ;
    }

</script>

<style>
    .base-table th {word-break: break-all}
</style>

<div id="wrap">
    <div id="container">
        <div class="content">
            <div class="content-head">
                <h2>- 설비 정보</h2>
            </div>

            <form id="searchForm">
                <%@include file="/WEB-INF/jsp/tiles_template/web/searchForm.jsp"%>
            </form>

            <div class="content-sect">
                <div class="content-option">
                    <div class="option-btn"></div>
                </div>
                <div class="table-wrap">
                    <table class="base-table">
                        <thead>
                        <tr>
                            <th scope="col">고유번호</th>
                            <th scope="col">Agent 이름</th>
                            <th scope="col">EPQ_TYPE</th>
                            <th scope="col">연결 상태</th>
                            <th scope="col">연결 시간</th>
                            <th scope="col">UPS 설치 날짜</th>
                            <th scope="col">비고</th>
                            <th scope="col">상태</th>
                        </tr>
                        </thead>
                        <tbody id="tbody-module">
                            <c:forEach var="module" items="${list}">
                                <tr class="jSelectModule" _no="${module.moduleNo}">
                                    <td>${module.moduleNo}</td>
                                    <td>${module.agentName}</td>
                                    <td>${module.eqpType}</td>
                                    <td>${module.connectState}</td>
                                    <td>${module.connectTime}, ${module.connectTimeDay}</td>
                                    <td>${module.upsInstallDate}</td>
                                    <td>${module.comment}</td>
                                    <td>${module.status}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="content-sect" id="table-channel-list" style="display: none;">
                <div class="content-option">
                    <h3 class="option-title">채널 목록</h3>
                    <div class="option-btn">
                        <a href="javascript:void(0);" onclick="openFieldSettingPopup('modal-fieldSetting')">필드 설정</a>
                    </div>
                </div>
                <div class="table-wrap">
                    <table class="base-table">
                        <thead>
                        <tr id="table_channel_thead">
                        </tr>
                        </thead>
                        <tbody id="tbody-channel">
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="content-sect-wrap">
                <div class="content-sect section-option" id="table-oven-list" style="display: none;">
                    <div class="content-option">
                        <h3 class="option-title">챔버 상태</h3>
                    </div>
                    <div class="table-wrap">
                        <table class="base-table">
                            <thead>
                            <tr>
                                <th scope="col">Room</th>
                                <th scope="col">Channel</th>
                                <th scope="col">almCode</th>
                                <th scope="col">ovenMode</th>
                                <th scope="col">pvHumi</th>
                                <th scope="col">pvTemp</th>
                                <th scope="col">svHumi</th>
                                <th scope="col">svTemp</th>
                                <th scope="col">state</th>
                            </tr>
                            </thead>
                            <tbody id="tbody-oven"></tbody>
                        </table>
                    </div>
                </div>

                <div class="content-sect section-option" id="table-chiller-list" style="display: none;">
                    <div class="content-option">
                        <h3 class="option-title">칠러 상태</h3>
                    </div>
                    <div class="table-wrap">
                        <table class="base-table">
                            <thead>
                            <tr>
                                <th scope="col">Room</th>
                                <th scope="col">Channel</th>
                                <th scope="col">almCode</th>
                                <th scope="col">flowAlowMode</th>
                                <th scope="col">flowState</th>
                                <th scope="col">pvFlow</th>
                                <th scope="col">pvTemp</th>
                                <th scope="col">svFlow</th>
                                <th scope="col">svTemp</th>
                                <th scope="col">tempAlmCode</th>
                                <th scope="col">tempMode</th>
                                <th scope="col">tempState</th>
                            </tr>
                            </thead>
                            <tbody id="tbody-chiller"></tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="content-sect-wrap">
                <div class="content-sect section-option" id="table-aux-list" style="display: none;">
                    <div class="content-option">
                        <h3 class="option-title">외부센서 정보</h3>
                    </div>
                    <div class="table-wrap">
                        <table class="base-table">
                            <thead>
                            <tr>
                                <th scope="col">no</th>
                                <th scope="col">이름</th>
                                <th scope="col">값</th>
                                <th scope="col">안전상</th>
                                <th scope="col">안전하</th>
                                <th scope="col">div code1</th>
                                <%--                                <th scope="col">div code2</th>--%>
                                <%--                                <th scope="col">div code3</th>--%>
                            </tr>
                            </thead>
                            <tbody id="tbody-aux"></tbody>
                        </table>
                    </div>
                </div>
                <!-- // content-sect -->
                <div class="content-sect section-option" id="table-can-list" style="display: none;">
                    <div class="content-option">
                        <h3 class="option-title">can 정보</h3>
                    </div>
                    <div class="table-wrap">
                        <table class="base-table">
                            <thead>
                            <tr>
                                <th scope="col">no</th>
                                <th scope="col">이름</th>
                                <th scope="col">값</th>
                                <th scope="col">ID(Hex)</th>
                                <th scope="col">안전상</th>
                                <th scope="col">안전하</th>
                                <th scope="col">div code1</th>
                            </tr>
                            </thead>
                            <tbody id="tbody-can"></tbody>
                        </table>
                    </div>
                </div>
                <!-- // content-sect -->
            </div>


        </div>
    </div>
</div>

<div class="modal-wrap" id="modal-fieldSetting">
    <div class="modal-content">

        <div class="modal-head">
            <h2 class="modal-head-title">필드 설정</h2>
            <button class="modal-close" onclick="closeModal(this)">닫기</button>
        </div>

        <div class="modal-body">
            <div class="modal-scroll">
                <div class="scroll-inner">

                    <div class="sect-wrap">
                        <div class="sect-item">
                            <h3 class="sect-title">채널 필드</h3>
                            <div class="sect-box" id="check_table">
                            </div>
                        </div>
                    </div>

                    <hr class="sect-line">
                    <div class="form-btn">
                        <a href="#" class="line" onclick="closeModal(this)">취소</a>
                        <a href="#" onclick="updateChannelFieldSetting()">저장</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>