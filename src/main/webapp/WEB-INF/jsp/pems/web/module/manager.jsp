<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<script>
    $(document).ready(function () {

        $("#search_input_name").on("keyup", function (key) {
            if (key.keyCode == 13) {
                $(".jSearchBtn").trigger("click");
            }
        })

        $("#search_value").on("keyup", function (key) {
            if (key.keyCode == 13) {
                getUserList();
            }
        })
        /*

                $(".jSearchBtn").click(function () {
                    $("#searchForm").append($('<input>', {
                        type: "hidden",
                        name: "userName",
                        value: $("#search_input_name").val()
                    }));
                    $("#searchForm").submit();
                })
        */

        $(".jSelectModule").click(function () {
            $("#tbody-userlist").attr("_selected", $(this).attr("_no"));
            getUserList();
            openModal("modal-manager");
        })


    })

    function setUpdateManager() {
        let userNoArr = new Array();
        $(".jCheck_Each:checked").each((function () {
            userNoArr.push($(this).val());
        }))

        // if ($(".jCheck_Each:checked").length > 0) {
        $.ajax({
            url: "/module/setInsertModuleManager",
            type: "post",
            contentType: "application/json; charset=UTF-8",
            data: JSON.stringify({
                userNoList: userNoArr.join(","),
                moduleNo: $("#tbody-userlist").attr("_selected")
            }),
            success: function (data) {
                alert(data.returnMessage)

                if ("1" == data.returnCode) {
                    location.reload();
                }
            }
        })
        // }

    }

    function getUserList() {
        let searchType = $(".user-list-div #search_type").val();
        let params = {};

        if ("userName" == searchType) {
            params.userName = $(".user-list-div #search_value").val();
        } else if ("dept" == searchType) {
            params.dept = $(".user-list-div #search_value").val();
        } else if ("phoneNo" == searchType) {
            params.phoneNo = $(".user-list-div #search_value").val();
        }

        $.ajax({
            url: "/user/getUserList",
            type: "post",
            dataType: "json",
            data: params,
            success: function (data) {
                let html = '';

                $.each(data.list, function (idx, obj) {
                    html += '<tr>';
                    html += '<td><input type="checkbox" id="check_' + idx + '" value="' + obj.userNo + '" ';
                    html += 'name="userNo" class="empty jCheck_Each"' + (isSelectedUser(obj.userNo) ? "checked" : "") + '><label for="check_' + idx + '"></label></td>';
                    html += '<td>' + obj.dept + '</td>';
                    html += '<td>' + obj.position + '</td>';
                    html += '<td>' + obj.userName + '</td>';
                    html += '<td>' + obj.decryptedPhoneNo + '</td>';
                    html += '</tr>';
                })

                $("#tbody-userlist").html(html);
            }, complete: function () {
                $(".jCheck_All").prop("checked", ($(".jCheck_Each:checked").length == $(".jCheck_Each").length));
            }
        })
    }

    function isSelectedUser(userNo) {
        let is = false;
        let moduleNo = $("#tbody-userlist").attr("_selected");

        $(".jSelectModule[_no='" + moduleNo + "']").find(".jSelectedUserNo").each(function () {
            is = $(this).val() == userNo;
            if (is) return false;
        })

        return is;
    }

</script>


<div id="wrap">
    <div id="container">
        <div class="content">
            <div class="content-head">
                <h2>- 설비 담당자 관리</h2>
            </div>

            <%--
            <c:import url="/WEB-INF/jsp/tiles_template/web/searchForm.jsp" charEncoding="UTF-8">
                <c:param name="page" value="moduleManager" />
                <c:param name="path" value="${requestScope['javax.servlet.forward.servlet_path']}" />
            </c:import>
            --%>
            <form id="searchForm">
                <%@include file="/WEB-INF/jsp/tiles_template/web/searchForm.jsp" %>

                <div class="content-option">
                    <h3 class="option-title">설비 목록</h3>
                    <div class="option-search">
                        <input type="text" id="search_input_name" name="userName" value="${param.userName}"
                               placeholder="담당자 이름">
                        <button type="submit"></button>
                    </div>
                </div>
            </form>

            <div class="table-wrap">
                <table class="base-table">
                    <thead>
                    <tr>
                        <th scope="col">사업장</th>
                        <th scope="col">공장</th>
                        <th scope="col">구역(룸)</th>
                        <th scope="col">Agent 이름</th>
                        <th scope="col">부서</th>
                        <th scope="col">직급</th>
                        <th scope="col">설비 담당자</th>
                    </tr>
                    </thead>
                    <tbody id="tbody-module-manager">
                        <c:forEach var="manager" items="${list}">
                            <c:set var="userList" value="${manager.userList}"/>
                            <c:set var="listLength" value="${fn:length(userList)}"/>

                            <tr class="jSelectModule" _no="${manager.moduleNo}">
                                <td rowspan="${listLength}">${manager.siteName}</td>
                                <td rowspan="${listLength}">${manager.factoryName}</td>
                                <td rowspan="${listLength}">${manager.areaName}</td>
                                <td rowspan="${listLength}">${manager.agentName}</td>
                                <c:forEach var="user" items="${userList}" varStatus="status">
                                    <c:if test="${not status.first}">
                                        <tr class="jSelectModule" _no="${manager.moduleNo}">
                                    </c:if>
                                    <td>${user.dept}</td>
                                    <td>${user.position}</td>
                                    <td>
                                        <input type="hidden" class="jSelectedUserNo" value="${user.userNo}">
                                            ${user.userName}
                                    </td>
                                    <c:if test="${not status.first}">
                                        </tr>
                                    </c:if>
                                </c:forEach>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<div class="modal-wrap w560" id="modal-manager">
    <div class="modal-content">

        <div class="modal-head">
            <h2 class="modal-head-title">설비 담당자 목록</h2>
            <button class="modal-close" onclick="closeModal(this)">닫기</button>
        </div>

        <div class="modal-body" style="height: 700px;">
            <div class="modal-scroll">
                <div class="scroll-inner">
                    <ul class="form-list">
                        <li>
                            <label class="form-title">검색</label>
                            <div class="form-box form-select-btn user-list-div">
                                <div class="select">
                                    <select id="search_type">
                                        <option value="userName">이름</option>
                                        <option value="dept">부서</option>
                                        <option value="phoneNo">전화번호</option>
                                    </select>
                                </div>
                                <div class="input"><input type="text" id="search_value"></div>
                                <div class="button">
                                    <button onclick="getUserList()">조회</button>
                                </div>
                            </div>
                        </li>
                        <li>
                            <p class="modal-table-title">* 설비 담당자를 선택해 주세요.</p>
                            <table class="inner-table">
                                <thead>
                                <tr>
                                    <th>
                                        <input type="checkbox" id="jCheck_All" class="empty jCheck_All">
                                        <label for="jCheck_All"></label>
                                    </th>
                                    <th scope="col">부서</th>
                                    <th scope="col">직급</th>
                                    <th scope="col">이름</th>
                                    <th scope="col">핸드폰</th>
                                </tr>
                                </thead>
                                <tbody id="tbody-userlist">
                                </tbody>
                            </table>
                            <form id="jUpdateForm"></form>
                        </li>
                    </ul>
                    <hr class="sect-line">
                    <div class="form-btn">
                        <a href="#" class="line" onclick="closeModal(this)">취소</a>
                        <a href="#" onclick="setUpdateManager()">저장</a>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
