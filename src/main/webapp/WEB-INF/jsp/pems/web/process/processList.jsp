<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<script>
    let VOLTAGE_UNIT = "VOLTAGE_UNIT";
    let CURRENT_UNIT = "CURRENT_UNIT";
    let CAPACITY_UNIT = "CAPACITY_UNIT";
    let WATT_UNIT = "WATT_UNIT";
    let WATT_HOUR_UNIT = "WATT_HOUR_UNIT";
    let DECIMAL_POINT = "DECIMAL_POINT";

    $(document).ready(function () {

        // 데이터 표현단위 설정
        $(".form-data select").change(function () {
            let thisId = $(this).attr("id");

            if ("voltage_unit" == thisId) {
                localStorage.setItem(VOLTAGE_UNIT, $(this).val());
            } else if ("current_unit" == thisId) {
                localStorage.setItem(CURRENT_UNIT, $(this).val());
            } else if ("capacity_unit" == thisId) {
                localStorage.setItem(CAPACITY_UNIT, $(this).val());
            } else if ("watt_unit" == thisId) {
                localStorage.setItem(WATT_UNIT, $(this).val());
            } else if ("watt_hour_unit" == thisId) {
                localStorage.setItem(WATT_HOUR_UNIT, $(this).val());
            } else if ("decimal_point" == thisId) {
                localStorage.setItem(DECIMAL_POINT, $(this).val());
            }

            setUnitRecordData();
        })

        // 공정 선택시 상세 팝업
        $(".jSelectProcess").click(function () {
            let processNo = $(this).attr("_no");

            $.ajax({
                url: "/process/processChannelList",
                type: "post",
                dataType: "json",
                data: {processNo: processNo},
                success: function (data) {
                    let html = '';

                    $.each(data.list, function (idx, obj) {
                        html += '<tr class="jSelectProcessChannel" _no="' + obj.processChannelNo + '">';
                        html += '<td>' + obj.channelName + '</td>';
                        html += '</tr>';
                    })

                    $("#tbody-process-channel").html(html);
                }
            })

            openModal("modal-process");
        })

        // 공정  채널목록 선택
        $(document).on("click", ".jSelectProcessChannel", function () {
            let processChannelNo = $(this).attr("_no");

            $.ajax({
                url: "/process/getCtsData",
                type: "post",
                dataType: "json",
                data: {processChannelNo: processChannelNo},
                success: function (data) {
                    let html = '';
                    let ctsList = data.ctsList;
                    let ctsHeader = data.ctsHeader;

                    $("#ctsInfoTable #process_name").text(ctsHeader.process_name);
                    $("#ctsInfoTable #process_channel_name").text(ctsHeader.channel_name);
                    $("#ctsInfoTable #process_file_name").text(ctsHeader.file_name);
                    $("#ctsInfoTable #sz_time").text(ctsHeader.szStartTime + " ~ " + ctsHeader.szEndTime);

                    $.each(ctsList, function (idx, obj) {

                        html += '<tr class="jSelectCtsRecord" _from="' + obj.nIndexFrom + '" _to="' + obj.nIndexTo + '">';
                        html += '<td>' + obj.step + '</td>';
                        html += '<td>' + getTypeStr(obj.chStepType) + '</td>';
                        html += '<td>' + obj.chCode + '</td>';
                        html += '<td>' + obj.fStepTime.toHHMMSS() + '</td>';
                        html += '<td><span></span><input type="hidden" class="set_unit_voltage" value="' + obj.fVoltage + '"></td>';
                        html += '<td><span></span><input type="hidden" class="set_unit_current" value="' + obj.fCurrent + '"></td>';
                        html += '<td><span></span><input type="hidden" class="set_unit_capacity" value="' + obj.fCapacity + '"></td>';
                        html += '<td><span></span><input type="hidden" class="set_unit_watt" value="' + obj.fWatt + '"></td>';
                        html += '<td><span></span><input type="hidden" class="set_unit_wattHour" value="' + obj.fWattHour + '"></td>';
                        html += '<td><span></span><input type="hidden" class="set_unit_voltage" value="' + obj.fAvgVoltage + '"></td>';
                        html += '<td><span></span><input type="hidden" class="set_unit_current" value="' + obj.fAvgCurrent + '"></td>';
                        html += '<td>' + obj.fImpedance + '</td>';
                        html += '</tr>';
                    })

                    $("#tbody-cts-record").attr("_no", processChannelNo);
                    $("#tbody-cts-record").html(html);
                    setUnitRecordData();
                }
            })
        })

        // CTS 목록 선택
        $(document).on("click", ".jSelectCtsRecord", function () {
            $("#tbody-cts-record").attr("_from", $(this).attr("_from"));
            $("#tbody-cts-record").attr("_to", $(this).attr("_to"));
            setCycTableTbody(1);
        })

        $(".table-paging #input_paging").on("keyup", function (key) {
            let goPage = $(this).val();
            let page = parseInt($("#pagingForm #page").val()) ;

            if ((key.keyCode == 13) && (goPage != page)) {
                let totalPage = parseInt($("#pagingForm #totalPage").val()) ;

                if (goPage < 1) {
                    goPage = 1 ;
                } else if (goPage > totalPage) {
                    goPage = totalPage ;
                }

                setCycTableTbody(goPage);
            }
        })


        setUnitData();
    })

    // CYC 목록 헤더작성
    function setCycTableThead() {
        let html = "";
        let nameList = localStorage[CYC_COLUMN_NAME_ARRAY].split(",");
        let visibleList = localStorage[CYC_VISIBLE_ARRAY].split(",");

        $("#cyc_table").css("width", nameList.length * 150);
        $.each(nameList, function (idx, key) {
            if ("Y" == visibleList[idx]) {
                html += '<th scope="col">' + key + '</th>';
            }
        })

        $("#table_cyc_thead").html(html);
    }

    function moveCycFirst() {
        setCycTableTbody(1);
    }

    function moveCycLeft() {
        setCycTableTbody($("#pagingForm #prevPage").val());
    }

    function moveCycRight() {
        setCycTableTbody($("#pagingForm #nextPage").val());
    }

    function moveCycLast() {
        setCycTableTbody($("#pagingForm #totalPage").val());
    }

    function moveCycList() {
        setCycTableTbody($(".table-paging #input_paging").val());
    }

    // CYC 리스트 출력
    function setCycTableTbody(page = 1) {
        if (page > 0) {
            setCycTableThead();

            let params = {
                processChannelNo: $("#tbody-cts-record").attr("_no")
                , nIndexFrom: $("#tbody-cts-record").attr("_from")
                , nIndexTo: $("#tbody-cts-record").attr("_to")
                , rowPerPage: $("#cyc-page-size").val()
                , page: page
            }

            $.ajax({
                url: "/process/getCycRecordList",
                type: "post",
                dataType: "json",
                data: params,
                success: function (data) {
                    let html = "";
                    let columnIdList = localStorage[CYC_COLUMN_ID_ARRAY].split(",");
                    let visibleList = localStorage[CYC_VISIBLE_ARRAY].split(",");

                    $.each(data.list, function (idx, obj) {
                        html += '<tr>';
                        if ("Y" === visibleList[columnIdList.indexOf("lSaveSequence")]) {
                            html += '<td>' + obj.lSaveSequence + '</td>';
                        }
                        if ("Y" === visibleList[columnIdList.indexOf("fStepTime_Day")]) {
                            html += '<td>' + obj.fStepTime_Day + '</td>';
                        }
                        if ("Y" === visibleList[columnIdList.indexOf("fStepTime")]) {
                            html += '<td>' + obj.fStepTime + '</td>';
                        }
                        if ("Y" === visibleList[columnIdList.indexOf("fVoltage")]) {
                            html += '<td><span></span><input type="hidden" class="set_unit_voltage" value="' + obj.fVoltage + '"></td>';
                        }
                        if ("Y" === visibleList[columnIdList.indexOf("fCurrent")]) {
                            html += '<td><span></span><input type="hidden" class="set_unit_current" value="' + obj.fCurrent + '"></td>';
                        }
                        if ("Y" === visibleList[columnIdList.indexOf("chCode")]) {
                            html += '<td>' + obj.chCode.split(".")[0] + '</td>';
                        }
                        if ("Y" === visibleList[columnIdList.indexOf("chInputState")]) {
                            html += '<td>' + obj.chInputState + '</td>';
                        }
                        if ("Y" === visibleList[columnIdList.indexOf("fOvenTemperature")]) {
                            html += '<td>' + obj.fOvenTemperature + '</td>';
                        }
                        if ("Y" === visibleList[columnIdList.indexOf("fOvenHumidity")]) {
                            html += '<td>' + obj.fOvenHumidity + '</td>';
                        }
                        if ("Y" === visibleList[columnIdList.indexOf("fChillerRefTemp")]) {
                            html += '<td>' + obj.fChillerRefTemp + '</td>';
                        }
                        if ("Y" === visibleList[columnIdList.indexOf("fChillerCurTemp")]) {
                            html += '<td>' + obj.fChillerCurTemp + '</td>';
                        }
                        if ("Y" === visibleList[columnIdList.indexOf("iChillerRefPump")]) {
                            html += '<td>' + obj.iChillerRefPump + '</td>';
                        }
                        if ("Y" === visibleList[columnIdList.indexOf("iChillerCurPump")]) {
                            html += '<td>' + obj.iChillerCurPump + '</td>';
                        }
                        if ("Y" === visibleList[columnIdList.indexOf("dCellBALChData")]) {
                            html += '<td>' + obj.dCellBALChData + '</td>';
                        }
                        if ("Y" === visibleList[columnIdList.indexOf("fVoltageInput")]) {
                            html += '<td><span></span><input type="hidden" class="set_unit_voltage" value="' + obj.fVoltageInput + '"></td>';
                        }
                        if ("Y" === visibleList[columnIdList.indexOf("fVoltagePower")]) {
                            html += '<td><span></span><input type="hidden" class="set_unit_voltage" value="' + obj.fVoltagePower + '"></td>';
                        }
                        if ("Y" === visibleList[columnIdList.indexOf("fVoltageBus")]) {
                            html += '<td><span></span><input type="hidden" class="set_unit_voltage" value="' + obj.fVoltageBus + '"></td>';
                        }
                        if ("Y" === visibleList[columnIdList.indexOf("fCapacity")]) {
                            html += '<td><span></span><input type="hidden" class="set_unit_capacity" value="' + obj.fCapacity + '"></td>';
                        }
                        if ("Y" === visibleList[columnIdList.indexOf("fChargeAh")]) {
                            html += '<td><span></span><input type="hidden" class="set_unit_capacity" value="' + obj.fChargeAh + '"></td>';
                        }
                        if ("Y" === visibleList[columnIdList.indexOf("fDisChargeAh")]) {
                            html += '<td><span></span><input type="hidden" class="set_unit_capacity" value="' + obj.fDisChargeAh + '"></td>';
                        }
                        if ("Y" === visibleList[columnIdList.indexOf("fWatt")]) {
                            html += '<td><span></span><input type="hidden" class="set_unit_watt" value="' + obj.fWatt + '"></td>';
                        }
                        if ("Y" === visibleList[columnIdList.indexOf("fWattHour")]) {
                            html += '<td><span></span><input type="hidden" class="set_unit_wattHour" value="' + obj.fWattHour + '"></td>';
                        }
                        if ("Y" === visibleList[columnIdList.indexOf("fChargeWh")]) {
                            html += '<td><span></span><input type="hidden" class="set_unit_wattHour" value="' + obj.fChargeWh + '"></td>';
                        }
                        if ("Y" === visibleList[columnIdList.indexOf("fDisChargeWh")]) {
                            html += '<td><span></span><input type="hidden" class="set_unit_wattHour" value="' + obj.fDisChargeWh + '"></td>';
                        }
                        if ("Y" === visibleList[columnIdList.indexOf("lSyncDateTime")]) {
                            html += '<td>' + obj.lSyncDate + " " + obj.fSyncTime + '</td>';
                        }
                        if ("Y" === visibleList[columnIdList.indexOf("fCVTime")]) {
                            html += '<td>' + obj.fCVTime + '</td>';
                        }
                        if ("Y" === visibleList[columnIdList.indexOf("fAvgCurrent")]) {
                            html += '<td><span></span><input type="hidden" class="set_unit_current" value="' + obj.fAvgCurrent + '"></td>';
                        }
                        if ("Y" === visibleList[columnIdList.indexOf("fAvgVoltage")]) {
                            html += '<td><span></span><input type="hidden" class="set_unit_voltage" value="' + obj.fAvgVoltage + '"></td>';
                        }
                        if ("Y" === visibleList[columnIdList.indexOf("fLoadVoltage")]) {
                            html += '<td><span></span><input type="hidden" class="set_unit_voltage" value="' + obj.fLoadVoltage + '"></td>';
                        }
                        if ("Y" === visibleList[columnIdList.indexOf("fLoadCurrent")]) {
                            html += '<td><span></span><input type="hidden" class="set_unit_current" value="' + obj.fLoadCurrent + '"></td>';
                        }
                        if ("Y" === visibleList[columnIdList.indexOf("fPwrSetVoltage")]) {
                            html += '<td><span></span><input type="hidden" class="set_unit_voltage" value="' + obj.fPwrSetVoltage + '"></td>';
                        }
                        if ("Y" === visibleList[columnIdList.indexOf("fPwrCurVoltage")]) {
                            html += '<td><span></span><input type="hidden" class="set_unit_voltage" value="' + obj.fPwrCurVoltage + '"></td>';
                        }
                        if ("Y" === visibleList[columnIdList.indexOf("chCommState")]) {
                            html += '<td>' + obj.chCommState + '</td>';
                        }
                        if ("Y" === visibleList[columnIdList.indexOf("chOutputState")]) {
                            html += '<td>' + obj.chOutputState + '</td>';
                        }

                        html += '</tr>';
                    })

                    $("#tbody-cyc-record").html(html);
                    setUnitRecordData();

                    let paging = data.paging;
                    $("#pagingForm #prevPage").val(paging.prevPage);
                    $("#pagingForm #page").val(paging.page);
                    $("#pagingForm #nextPage").val(paging.nextPage);
                    $("#pagingForm #totalPage").val(paging.totalPage);
                    $(".table-paging #cyc-page-size").val(paging.rowPerPage);
                    $(".table-paging #input_paging").val(paging.page);
                    $(".table-paging #total-page").text(paging.totalPage);

                }
            })
        }
    }

    // CTS 리스트의 값을 단위와 소수점에 맞게 표현
    function setUnitRecordData() {
        let decimalPoint = localStorage[DECIMAL_POINT];

        $(".set_unit_voltage").each(function () {
            let voltage = $(this).val() / localStorage[VOLTAGE_UNIT];
            $(this).siblings("span").text(voltage.toFixed(decimalPoint));
        })
        $(".set_unit_current").each(function () {
            let current = $(this).val() / localStorage[CURRENT_UNIT];
            $(this).siblings("span").text(current.toFixed(decimalPoint));
        })
        $(".set_unit_capacity").each(function () {
            let capacity = $(this).val() / localStorage[CAPACITY_UNIT];
            $(this).siblings("span").text(capacity.toFixed(decimalPoint));
        })
        $(".set_unit_watt").each(function () {
            let watt = $(this).val() / localStorage[WATT_UNIT];
            $(this).siblings("span").text(watt.toFixed(decimalPoint));
        })
        $(".set_unit_wattHour").each(function () {
            let wattHour = $(this).val() / localStorage[WATT_HOUR_UNIT];
            $(this).siblings("span").text(wattHour.toFixed(decimalPoint));
        })
    }

    // 단위 초기화
    function setUnitData() {
        getUserTableColumns("cycrecord");
        if (isEmpty(localStorage[VOLTAGE_UNIT])) {
            localStorage.setItem(VOLTAGE_UNIT, "1000");
        }
        if (isEmpty(localStorage[CURRENT_UNIT])) {
            localStorage.setItem(CURRENT_UNIT, "1000");
        }
        if (isEmpty(localStorage[CAPACITY_UNIT])) {
            localStorage.setItem(CAPACITY_UNIT, "1000");
        }
        if (isEmpty(localStorage[WATT_UNIT])) {
            localStorage.setItem(WATT_UNIT, "1000");
        }
        if (isEmpty(localStorage[WATT_HOUR_UNIT])) {
            localStorage.setItem(WATT_HOUR_UNIT, "1000");
        }
        if (isEmpty(localStorage[DECIMAL_POINT])) {
            localStorage.setItem(DECIMAL_POINT, "3");
        }
        $(".form-data select[id='voltage_unit']").val(localStorage[VOLTAGE_UNIT]);
        $(".form-data select[id='current_unit']").val(localStorage[CURRENT_UNIT]);
        $(".form-data select[id='capacity_unit']").val(localStorage[CAPACITY_UNIT]);
        $(".form-data select[id='watt_unit']").val(localStorage[WATT_UNIT]);
        $(".form-data select[id='watt_hour_unit']").val(localStorage[WATT_HOUR_UNIT]);
        $(".form-data select[id='decimal_point']").val(localStorage[DECIMAL_POINT]);
    }

    // 필드 설정파일 열기
    function openFieldSettingPopup(modalId) {
        let visibleList = localStorage[CYC_VISIBLE_ARRAY].split(",");
        let columnIdList = localStorage[CYC_COLUMN_ID_ARRAY].split(",");
        let columnNameList = localStorage[CYC_COLUMN_NAME_ARRAY].split(",");
        let html = "";

        $.each(columnNameList, function (idx, name) {
            html += '<div class="check"><input type="checkbox" id="';
            html += columnIdList[idx] + '" name="checkbox" ' + ("Y" == visibleList[idx] ? "checked" : "") + '><label for="';
            html += columnIdList[idx] + '">' + name + '</label></div>';
        })

        $("#check_table").html(html);
        openModal(modalId)
    }

    // 필드 설정 저장
    function updateCycFieldSetting() {
        let visibleList = localStorage[CYC_VISIBLE_ARRAY].split(",");

        $("#check_table [type='checkbox']").each(function (idx) {
            visibleList[idx] = $(this).is(":checked") ? "Y" : "N";
        })

        localStorage.setItem(CYC_VISIBLE_ARRAY, visibleList);
        setUserTableColumns(USER_TABLE_NAME_CYC, localStorage[CYC_COLUMN_ID_ARRAY], localStorage[CYC_COLUMN_NAME_ARRAY], localStorage[CYC_VISIBLE_ARRAY]);

        setCycTableTbody();
        $("#modal-fieldSetting").removeClass("active");
    }

    function ctsExcelDownload() {
        let processChannelNo = $("#tbody-cts-record").attr("_no") ;

        if (!isEmpty(processChannelNo)) {
            location.href = "/excel/ctsDownload?processChannelNo=" + processChannelNo ;
        }
    }

    function getTypeStr(str) {
        let retVal = "";

        switch (str) {
            case 0:
                retVal = "PS_STEP_NONE";
                break;
            case 1:
                retVal = "충전";
                break;
            case 2:
                retVal = "방전";
                break;
            case 3:
                retVal = "휴지";
                break;
            case 4:
                retVal = "OCV";
                break;
            case 5:
                retVal = "임피던스";
                break;
            case 6:
                retVal = "완료";
                break;
            case 7:
                retVal = "작업중";
                break;
            case 8:
                retVal = "작업중";
                break;
            case 9:
                retVal = "Pettern";
                break;
            case 10:
                retVal = "LONG_REST";
                break;
            case 11:
                retVal = "임피던스";
                break;
            case 12:
                retVal = "임피던스";
                break;
            default:
                retVal = "작업중";
                break;
        }

        return retVal;
    }

</script>

<style>
    #tbody-cts-record::-webkit-scrollbar, .modal-detail-body::-webkit-scrollbar, .table-wrap::-webkit-scrollbar {
        display: none;
    }

    #tbody-cts-record td {
        min-width: 100px;
    }

    #ctsInfoTable th {
        width: 150px;
    }

    #ctsInfoTable th, td {
        text-align: center;
        vertical-align: middle;
    }
</style>

<div id="wrap">
    <div id="container">
        <div class="content">
            <div class="content-head">
                <h2>- 결과 데이터</h2>
            </div>

            <form id="searchForm">
                <%@include file="/WEB-INF/jsp/tiles_template/web/searchForm.jsp" %>
            </form>


            <div class="content-option">
                <h3 class="option-title">공정 목록</h3>
            </div>
            <div class="table-wrap">
                <table class="base-table">
                    <thead>
                    <tr>
                        <th scope="col">설비명</th>
                        <th scope="col" style="width: 30%;">공정명</th>
                        <th scope="col">사업장</th>
                        <th scope="col">공장</th>
                        <th scope="col">구역</th>
                        <th scope="col">채널 수</th>
                    </tr>
                    </thead>
                    <tbody id="tbody-process">
                    <c:forEach var="process" items="${list}">
                        <tr class="jSelectProcess" _no="${process.processNo}">
                            <td>${process.agentName}</td>
                            <td>${process.processName}</td>
                            <td>${process.siteName}</td>
                            <td>${process.factoryName}</td>
                            <td>${process.areaName}</td>
                            <td>${process.channelCount}</td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<div class="modal-wrap w1280" id="modal-process">
    <div class="modal-content">
        <div class="modal-head">
            <h2 class="modal-head-title">공정 목록</h2>
            <button class="modal-close" onclick="closeModal(this)">닫기</button>
        </div>

        <div class="modal-body" style="height: 800px;">
            <div class="modal-scroll">
                <div class="scroll-inner">

                    <dl class="form-data">
                        <dt>전압</dt>
                        <dd>
                            <select id="voltage_unit">
                                <option value="1000">V</option>
                                <option value="1">mV</option>
                            </select>
                        </dd>
                        <dt>전류</dt>
                        <dd>
                            <select id="current_unit">
                                <option value="1000">A</option>
                                <option value="1">mA</option>
                                <option value="0.001">uA</option>
                            </select>
                        </dd>
                        <dt>용량</dt>
                        <dd>
                            <select id="capacity_unit">
                                <option value="1000">Ah</option>
                                <option value="1">mAh</option>
                                <option value="0.001">uAh</option>
                            </select>
                        </dd>
                        <dt>Watt</dt>
                        <dd>
                            <select id="watt_unit">
                                <option value="1000">W</option>
                                <option value="1000000">KW</option>
                                <option value="1">mW</option>
                                <option value="0.001">uW</option>
                            </select>
                        </dd>
                        <dt>WattHour</dt>
                        <dd>
                            <select id="watt_hour_unit">
                                <option value="1000">Wh</option>
                                <option value="1000000">KWh</option>
                                <option value="1">mKh</option>
                                <option value="0.001">uKh</option>
                            </select>
                        </dd>
                        <dt>소수점</dt>
                        <dd>
                            <select id="decimal_point">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                            </select>
                        </dd>
                    </dl>

                    <div class="modal-detail">
                        <h3 class="detail-title">CTS 정보</h3>
                    </div>
                    <div class="modal-detail-wrap">
                        <div class="modal-detail-left">
                            <div class="modal-detail-head">채널 선택</div>
                            <div class="modal-detail-body">
                                <div class="table-wrap">
                                    <table class="inner-table">
                                        <thead>
                                        <tr>
                                            <th scope="col">채널명</th>
                                        </tr>
                                        </thead>
                                        <tbody id="tbody-process-channel">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="modal-detail-right">
                            <div class="modal-detail-head">CTS 정보</div>
                            <div class="modal-detail-body">

                                <div class="table-wrap">
                                    <table class="inner-table inner-table-left" id="ctsInfoTable">
                                        <colgroup>
                                            <col>
                                            <col>
                                            <col>
                                            <col style="width:200px">
                                        </colgroup>
                                        <tbody>
                                        <tr>
                                            <th scope="row">작업명</th>
                                            <td id="process_name"></td>
                                            <th scope="row">선택채널</th>
                                            <td id="process_channel_name"></td>
                                        </tr>
                                        <tr>
                                            <th scope="row">공정명</th>
                                            <td id="process_file_name" colspan="3"></td>
                                            <%--
                                            <th scope="row">스케줄</th>
                                            <td>
                                                <div class="detail-btn">
                                                    <a href="#" onclick="openModal('modal-schedule-info')"id="schedule_btn">
                                                        스케줄정보 상세보기
                                                    </a>
                                                </div>
                                            </td>
                                            --%>
                                        </tr>
                                        <tr>
                                            <th scope="row">기간</th>
                                            <td id="sz_time"></td>
                                            <th scope="row">CTS (Excel 다운)</th>
                                            <td>
                                                <div class="detail-btn">
                                                    <a onclick="ctsExcelDownload()" id="cts_csv_btn">CTS (다운로드)</a>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">설명</th>
                                            <td colspan="3" id="desc_item"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="table-wrap">
                                    <table class="inner-table inner-table-header-wide">
                                        <thead>
                                        <tr>
                                            <th>step</th>
                                            <th>Type</th>
                                            <th>State</th>
                                            <th>Time</th>
                                            <th>Voltage(V)</th>
                                            <th>Current(A)</th>
                                            <th>Capacity(Ah)</th>
                                            <th>Watt(W)</th>
                                            <th>WH(KWh)</th>
                                            <th>Avg(V)</th>
                                            <th>Avg(A)</th>
                                            <th>Imp</th>
                                        </tr>
                                        </thead>
                                        <tbody id="tbody-cts-record">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-detail">
                        <h3 class="detail-title">CYC 정보</h3>
                        <div class="option-btn">
                            <a href="javascript:void(0);" onclick="openFieldSettingPopup('modal-fieldSetting')">필드
                                설정</a>
                        </div>
                    </div>
                    <div class="modal-detail-wrap">
                        <div class="table-wrap">
                            <table class="inner-table" id="cyc_table">
                                <thead>
                                <tr id="table_cyc_thead">
                                </tr>
                                </thead>
                                <tbody id="tbody-cyc-record">
                                </tbody>
                                <tr id="tbody_bottom">
                                    <td colspan="13">
                                        <div class="table-paging">
                                            <form id="pagingForm">
                                                <input type="hidden" id="prevPage">
                                                <input type="hidden" id="page">
                                                <input type="hidden" id="nextPage">
                                                <input type="hidden" id="totalPage">
                                            </form>
                                            <div class="paging-select">
                                                <select id="cyc-page-size">
                                                    <option value="100">100</option>
                                                    <option value="200">200</option>
                                                    <option value="300">300</option>
                                                </select>
                                            </div>
                                            <a href="#" class="paging-arrow paging-first" onclick="moveCycFirst()">처음</a>
                                            <a href="#" class="paging-arrow paging-prev" onclick="moveCycLeft()">이전</a>
                                            <div class="paging-count">
                                                Page <input type="text" id="input_paging"> of
                                                <a id="total-page"></a>
                                            </div>
                                            <a href="#" class="paging-arrow paging-next" onclick="moveCycRight()">다음</a>
                                            <a href="#" class="paging-arrow paging-last" onclick="moveCycLast()">마지막</a>
                                            <a href="#" class="paging-refresh" onclick="moveCycList()">새로고침</a>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal-wrap" id="modal-fieldSetting">
    <div class="modal-content">

        <div class="modal-head">
            <h2 class="modal-head-title">필드 설정</h2>
            <button class="modal-close" onclick="closeModal(this)">닫기</button>
        </div>

        <div class="modal-body">
            <div class="scroll-inner">

                <div class="sect-wrap">
                    <div class="sect-item">
                        <div class="sect-box" id="check_table">
                        </div>
                    </div>
                </div>

                <hr class="sect-line">
                <div class="form-btn">
                    <a href="#" class="line" onclick="closeModal(this)">취소</a>
                    <a href="#" onclick="updateCycFieldSetting()">저장</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal-wrap w1024" id="modal-schedule-info">
    <div class="modal-content">

        <div class="modal-head">
            <h2 class="modal-head-title">모델/스케줄 정보</h2>
            <button class="modal-close" onclick="closeModal(this)">닫기</button>
        </div><!-- // modal-head -->

        <div class="modal-body" style="height: 516px;">
            <div class="modal-scroll">
                <div class="scroll-inner">

                    <div class="detail-btn">
                        <a href="#" onclick="closeModal(this)">공장정보로 돌아가기</a>
                    </div>

                    <ul class="modal-half">
                        <li>
                            <h3 class="modal-half-title">모델 정보</h3>
                            <div class="table-wrap">
                                <table class="inner-table inner-table-left">
                                    <colgroup>
                                        <col style="width:30%">
                                        <col>
                                    </colgroup>
                                    <tbody>
                                    <tr>
                                        <th scope="row">이름</th>
                                        <td id="schedule_model_name"></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">설명</th>
                                        <td id="schedule_model_description"></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </li>
                        <li>
                            <h3 class="modal-half-title">스케줄 정보</h3>
                            <div class="table-wrap">
                                <table class="inner-table inner-table-left">
                                    <colgroup>
                                        <col style="width:30%">
                                        <col>
                                    </colgroup>
                                    <tbody>
                                    <tr>
                                        <th scope="row">이름</th>
                                        <td id="schedule_info_name"></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">작성자</th>
                                        <td id="schedule_info_writher"></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">설명</th>
                                        <td id="schedule_info_description"></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">수정일</th>
                                        <td id="schedule_info_mot_day"></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </li>
                        <li>
                            <h3 class="modal-half-title">상세 내용</h3>
                            <div class="table-wrap">
                                <table class="inner-table">
                                    <colgroup>
                                        <col style="width:10%">
                                        <col style="width:12%">
                                        <col>
                                        <col>
                                        <col>
                                        <col style="width:21%">
                                    </colgroup>
                                    <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Type</th>
                                        <th scope="col">Charce V</th>
                                        <th scope="col">DisCharge</th>
                                        <th scope="col">Currents</th>
                                        <th scope="col">Temperature</th>
                                    </tr>
                                    </thead>
                                    <tbody id="schedule_tbody">
                                    </tbody>
                                </table>
                            </div>
                        </li>
                        <li>
                            <h3 class="modal-half-title">종료 조건</h3>
                            <div class="table-wrap">
                                <table class="inner-table inner-table-left">
                                    <colgroup>
                                        <col style="width:30%">
                                        <col>
                                        <col style="width:27%">
                                        <col>
                                    </colgroup>
                                    <tbody>
                                    <tr>
                                        <th scope="row">Voltage</th>
                                        <td id="schedule_exit_v"></td>
                                        <th scope="row">Current</th>
                                        <td id="schedule_exit_current"></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Capacity</th>
                                        <td id="schedule_exit_capa"></td>
                                        <th scope="row">Time</th>
                                        <td id="schedule_exit_time"></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Delta Vp</th>
                                        <td id="schedule_exit_delta"></td>
                                        <th scope="row">Watt</th>
                                        <td id="schedule_exit_w"></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">WattHour</th>
                                        <td id="schedule_exit_wh"></td>
                                        <th scope="row">Temp</th>
                                        <td id="schedule_exit_tem"></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">etc</th>
                                        <td colspan="3" id="schedule_exit_etc"></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <h3 class="modal-half-title">안전성 조건</h3>
                            <div class="table-wrap">
                                <table class="inner-table">
                                    <colgroup>
                                        <col style="width:30%">
                                        <col>
                                        <col>
                                    </colgroup>
                                    <thead>
                                    <tr>
                                        <th scope="col"></th>
                                        <th scope="col">max</th>
                                        <th scope="col">min</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th scope="row" class="left">Voltage</th>
                                        <td id="schedule_savefy_v_max"></td>
                                        <td id="schedule_savefy_v_min"></td>
                                    </tr>
                                    <tr>
                                        <th scope="row" class="left">Current</th>
                                        <td id="schedule_savefy_cu_max"></td>
                                        <td id="schedule_savefy_cu_min"></td>
                                    </tr>
                                    <tr>
                                        <th scope="row" class="left">Capacity</th>
                                        <td id="schedule_savefy_ca_max"></td>
                                        <td id="schedule_savefy_ca_min"></td>
                                    </tr>
                                    <tr>
                                        <th scope="row" class="left">Impedance</th>
                                        <td id="schedule_savefy_i_max"></td>
                                        <td id="schedule_savefy_i_min"></td>
                                    </tr>
                                    <tr>
                                        <th scope="row" class="left">Temperature</th>
                                        <td id="schedule_savefy_t_max"></td>
                                        <td id="schedule_savefy_t_min"></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <h3 class="modal-half-title">저장 조건</h3>
                            <div class="table-wrap">
                                <table class="inner-table inner-table-left">
                                    <colgroup>
                                        <col style="width:30%">
                                        <col>
                                        <col>
                                    </colgroup>
                                    <tbody>
                                    <tr>
                                        <th scope="row">Time Variation</th>
                                        <td id="schedule_save_t_v"></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Voltage Variation</th>
                                        <td id="schedule_save_v_v"></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Current Variation</th>
                                        <td id="schedule_save_c_v"></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </li>
                    </ul>

                </div>
            </div>
        </div>

    </div>
</div>
