<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<script>

    $(document).ready(function () {

        $("#update-file").change(function () {
            let fileInfo = $(this).prop("files")[0];
            $("#selected-filename").text(fileInfo.name);
            $("#jUploadBtnLabel").show();
        })

        $("#jUpdate-file-uploadbtn").click(function () {

            $("#jUpdateFileForm").ajaxSubmit({
                url: "/update/fileUpload",
                dataType: "json",
                type: "post",
                success: function (data) {
                    console.log(data);
                    alert(data.returnMessage);
                    if (data.returnCode == 1) {
                        location.reload() ;
                    }
                }
            });

        })

    })


</script>


<div id="wrap">
    <div id="container">
        <div class="content">
            <div class="content-head">
                <h2>- 업데이트 파일 관리</h2>
            </div>

            <div class="content-option">
                <div class="option-date" id="option-date"></div>
            </div>

            <div class="content-option">
                <h3 class="option-title">${vo.fileType} 업로드</h3>
            </div>

            <div class="table-wrap">
                <form id="jUpdateFileForm">
                    <input type="hidden" name="fileType" value="${vo.fileType}">

                    <table class="base-table">
                        <thead>
                        <tr>
                            <th scope="col" colspan="4">${vo.fileType}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><label class="form-title">업데이트 내역</label></td>
                            <td colspan="3">
                                <div class="form-box">
                                    <input type="text" name="comment" placeholder="필수 입력" autocomplete="off">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="file-wrap">
                                    <label>
                                        <input type="file" class="file-input" id="update-file" name="file"
                                               multiple="multiple">
                                        <em>신규 파일 업로드 하기</em>
                                    </label>
                                </div>
                            </td>
                            <td><span id="selected-filename"></span></td>
                            <td>
                                <div class="file-wrap">
                                    <label style="display: none" id="jUploadBtnLabel">
                                        <input class="file-input" id="jUpdate-file-uploadbtn">
                                        <em>업로드</em>
                                    </label>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            </div>

            <br />

            <div class="content-option">
                <h3 class="option-title">${vo.fileType} 업데이트 파일 목록</h3>
            </div>

            <div class="table-wrap">
                <table class="base-table">
                    <thead>
                    <tr>
                        <th scope="col">요청자</th>
                        <th scope="col">파일명</th>
                        <th scope="col">업데이트 내역</th>
                        <th scope="col">등록일</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="file" items="${list}">
                        <tr>
                            <td>${file.userName}</td>
                            <td>${file.original}</td>
                            <td>${file.comment}</td>
                            <td>${file.regDate}</td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
            <%-- 페이징 --%>
            <%@include file="/WEB-INF/jsp/tiles_template/web/paging.jsp" %>
        </div>
    </div>
</div>

<div class="modal-wrap" id="modal-agent">
    <div class="modal-content">
        <div class="modal-head">
            <h2 class="modal-head-title">에이전트 수정</h2>
            <button class="modal-close" onclick="closeModal(this)">닫기</button>
        </div>

        <div class="modal-body">
            <div class="modal-scroll">
                <div class="scroll-inner">

                    <form id="agentForm">
                        <input type="hidden" id="agentNo" name="agentNo">
                        <ul class="form-list">
                            <li>
                                <label class="form-title">이름</label>
                                <div class="form-box">
                                    <div class="input">
                                        <input type="text" id="agentName" name="agentName">
                                    </div>
                                </div>
                            </li>
                            <li>
                                <label class="form-title">메모</label>
                                <div class="form-box">
                                    <div class="input">
                                        <input type="text" id="comment" name="comment">
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </form>
                    <hr class="sect-line">
                    <div class="form-btn">
                        <a href="javascript:void(0);" class="line" onclick="closeModal(this)">취소</a>
                        <a href="javascript:void(0);" id="updateBtn" class="jRegBtn jUpdateAgent">수정</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>