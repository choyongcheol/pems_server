<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page isErrorPage="true"%>
<%
    response.setStatus(200);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>오류</title>
<style>
body {
	background-color: #EFEFEF;
	color: #2E2F30;
	text-align: center;
	font-family: arial, sans-serif;
	margin: 0;
}

div.dialog {
	width: 95%;
	max-width: 33em;
	margin: 4em auto 0;
}

div.dialog>div {
	border: 1px solid #CCC;
	border-right-color: #999;
	border-left-color: #999;
	border-bottom-color: #BBB;
	border-top: #B00100 solid 4px;
	border-top-left-radius: 9px;
	border-top-right-radius: 9px;
	background-color: white;
	padding: 7px 12% 0;
	box-shadow: 0 3px 8px rgba(50, 50, 50, 0.17);
}

h1 {
	font-size: 100%;
	color: #730E15;
	line-height: 1.5em;
}

div.dialog>p {
	margin: 0 0 1em;
	padding: 1em;
	background-color: #F7F7F7;
	border: 1px solid #CCC;
	border-right-color: #999;
	border-left-color: #999;
	border-bottom-color: #999;
	border-bottom-left-radius: 4px;
	border-bottom-right-radius: 4px;
	border-top-color: #DADADA;
	color: #666;
	box-shadow: 0 3px 8px rgba(50, 50, 50, 0.17);
}
</style>
</head>
<body>
    <div class="dialog">
        <div>
            <c:choose>
                <c:when test='${requestScope["javax.servlet.error.status_code"] eq 400}'>
                    <h1>잘못된 요청입니다.</h1>
                </c:when>
                <c:when test='${requestScope["javax.servlet.error.status_code"] eq 403}'>
                    <h1>접근이 금지되었습니다.</h1>
                </c:when>
                <c:when test='${requestScope["javax.servlet.error.status_code"] eq 404}'>
                    <h1>존재하지 않는 페이지입니다.</h1>
                </c:when>
                <c:when test='${requestScope["javax.servlet.error.status_code"] eq 405}'>
                    <h1>허용되지 않는 요청입니다.</h1>
                </c:when>
                <c:when test='${requestScope["javax.servlet.error.status_code"] eq 500}'>
                    <h1>요청중 서버에 오류가 발생하였습니다.</h1>
                </c:when>
                <c:when test='${requestScope["javax.servlet.error.status_code"] eq 502}'>
                    <h1>서버가 과부화 상태입니다.</h1>
                </c:when>
                <c:when test='${requestScope["javax.servlet.error.status_code"] eq 503}'>
                    <h1>서비스를 사용할 수 없습니다.</h1>
                </c:when>
                <c:when test='${requestScope["javax.servlet.error.status_code"] eq 504}'>
                    <h1>요청 시간이 초과되었습니다.</h1>
                </c:when>
                <c:otherwise>
                    <h1>요청중 서버에 오류가 발생하였습니다.</h1>
                </c:otherwise>
            </c:choose>
            <h1>화면 새로고침을 해 보세요.</h1>
        </div>
        <p>문제가 지속되면 운영팀으로 연락 부탁드립니다.</p>
    </div>
</body>
</html>


