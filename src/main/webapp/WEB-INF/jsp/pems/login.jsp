<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>PEMS</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="icon" href="<c:url value="/resource/favicon.png"/>">

    <script src="<c:url value="/resource/js/jquery-3.4.1.min.js"/>"></script>
    <script src="<c:url value="/resource/js/common.js"/>"></script>
    <script src="<c:url value="/resource/js/cryptico.min.js"/>"></script>

    <link rel="stylesheet" href="<c:url value="/resource/css/snackbar.css"/>" type="text/css"/>
    <link rel="stylesheet" href="<c:url value="/resource/css/common.css"/>" type="text/css"/>
    <link rel="stylesheet" href="<c:url value="/resource/css/fonts.css"/>" type="text/css"/>
    <link rel="stylesheet" href="<c:url value="/resource/css/reset.css"/>" type="text/css"/>
<%--    <link rel="stylesheet" href="<c:url value="/resource/css/jquery-ui.css"/>" type="text/css"/>--%>

</head>

<script>

    let SAVED_LOGIN_ID = "SAVED_LOGIN_ID";

    $(document).ready(function () {

        if (localStorage[SAVED_LOGIN_ID]) {
            $("#signId").val(localStorage[SAVED_LOGIN_ID]);
            $("#saveSignId").prop("checked", true);
            $("#password").focus();
        } else {
            $("#signId").focus();
        }

        // 엔터키 이밴트
        $("#signId, #password").on("keyup", function (key) {

            if ($("#signType").val() == "") {
                alert("회원구분을 먼저 선택해주세요.");
                $(this).val("");
                return false;
            }

            if (key.keyCode == 13) {
                $(".jLogin").trigger("click");
            }
        })

        // 로그인
        $(".jLogin").click(function () {

            if ("" == $("#signId").val()) {
                alert("아이디를 입력해주세요.");
                $("#signId").focus();
                return false;
            }
            if ("" == $("#password").val()) {
                alert("비밀번호를 입력하세요");
                $("#password").focus();
                return false;
            }

            $('#sendpassword').val(getRAS($("#password").val()));

            $.ajax({
                url: "/login/login",
                type: "post",
                dataType: "json",
                data: $("#signForm").serialize(),
                success: function (data) {
                    if (data.returnCode == 1) {
                        if ($("#saveSignId").is(":checked")) {
                            localStorage[SAVED_LOGIN_ID] = $("#signForm #signId").val();
                        } else if (localStorage[SAVED_LOGIN_ID]) {
                            localStorage.removeItem(SAVED_LOGIN_ID);
                        }
                        location.href = data.rurl;
                    } else if (data.returnCode == -1000) {
                        location.reload() ;
                    } else {
                        alert(data.returnMessage);
                    }
                }
            })
        })

        $("#jGetVerificationCodeId").click(function () {
            if ($("#input_name_find_id").val().trim() == "") {
                alert("이름을 입력하세요.");
                $("#input_name_find_id").focus();
                return false;
            }
            if ($("#input_find_id_phone_no").val().trim() == "") {
                $("#input_find_id_phone_no").focus();
                alert("전화번호를 입력하세요.");
                return false;
            }

            $.ajax({
                url: "/login/sendValidationCode",
                type: "post",
                dataType: "json",
                data: $("#searchIdFm").serialize(),
                success: function (data) {
                    alert(data.returnMessage);
                    if (data.returnCode == "1") {
                        $("#searchIdFm [name='validationCode']").val(data.validationCode)

                        $("#searchIdFm #jGetVerificationCodeId").attr("disabled", true);
                        $("#searchIdFm #input_name_find_id").attr("disabled", true);
                        $("#searchIdFm #input_find_id_phone_no").attr("disabled", true);

                        $("#searchIdFm .jVerificationCode").attr("disabled", false);
                        $("#searchIdFm .jVerificationCode").focus();
                        $("#searchIdFm .jConfirmVerificationCode").attr("disabled", false);
                    }
                }
            })
        })

        $("#jGetVerificationCodePw").click(function () {
            if ($("#input_id_find_pw").val().trim() == "") {
                alert("아이디를 입력하세요.");
                $("#input_id_find_pw").focus();
                return false;
            }
            if ($("#input_name_find_pw").val().trim() == "") {
                alert("이름을 입력하세요.");
                $("#input_name_find_pw").focus();
                return false;
            }
            if ($("#input_find_pw_phone_no").val().trim() == "") {
                $("#input_find_pw_phone_no").focus();
                alert("전화번호를 입력하세요.");
                return false;
            }

            $.ajax({
                url: "/login/sendValidationCode",
                type: "post",
                dataType: "json",
                data: $("#searchPwFm").serialize(),
                success: function (data) {
                    alert(data.returnMessage);
                    if (data.returnCode == "1") {
                        $("#searchPwFm [name='validationCode']").val(data.validationCode)

                        $("#searchPwFm #jGetVerificationCodePw").attr("disabled", true);
                        $("#searchPwFm #input_id_find_pw").attr("disabled", true);
                        $("#searchPwFm #input_name_find_pw").attr("disabled", true);
                        $("#searchPwFm #input_find_pw_phone_no").attr("disabled", true);

                        $("#searchPwFm .jVerificationCode").attr("disabled", false);
                        $("#searchPwFm .jVerificationCode").focus();
                        $("#searchPwFm .jConfirmVerificationCode").attr("disabled", false);
                    }
                }
            })
        })

        $("#searchPwFm .jConfirmVerificationCode").click(function () {
            if ($("#searchPwFm .jVerificationCode").val().trim() == "") {
                alert("인증번호를 입력하세요.");
                return false;
            }

            $.ajax({
                url: "/login/resetPassword",
                type: "post",
                dataType: "json",
                data: $("#searchPwFm").serialize(),
                success: function (data) {
                    alert(data.returnMessage);

                    if (data.returnCode == "1") {
                        location.reload();
                    }
                }
            })

        })

        $("#searchIdFm .jConfirmVerificationCode").click(function () {
            if ($("#searchIdFm .jVerificationCode").val().trim() == "") {
                alert("인증번호를 입력하세요.");
                return false;
            }

            $.ajax({
                url: "/login/confirmVerificationCode",
                type: "post",
                dataType: "json",
                data: $("#searchIdFm").serialize(),
                success: function (data) {
                    let vo = data.vo;
                    if (data.returnCode == "1") {
                        $(".jConfirmVerificationCode").attr("disabled", true);
                        let msg = vo.userName + "님의 아이디는 '" + vo.userId + "' 입니다.";
                        $("#label_ask_id").text(msg);
                        $("#label_ask_id").show();
                    } else {
                        alert(data.returnMessage);
                    }
                }
            })
        })

        // 입력된 아이디 변경시 중복체크 초기화
        $("#signup-signId").on("keyup", function() {
            $("#isCheckId").val(false) ;
        })

        // 아이디 중복체크
        $("#checkId").click(function() {
			if ("" != $("#signup-signId").val() && "false" == $("#isCheckId").val()) {
				$.ajax({
					url : "/login/duplicateId",
					type : "post",
					dataType : "json",
					data : {signId: $("#signup-signId").val()},
					success : function(data) {
						alert(data.returnMessage) ;

						if (data.returnCode == 1) {
							$("#isCheckId").val(true) ;
						}
					}
				})
			}
		})

        // 회원가입
        $("#jSignup").click(function() {

            if ("" == $("#signup-signId").val()) {
				alert("아이디를 입력하세요") ;
				$("#signup-signId").focus() ;
				return false ;
			}

			if ("false" == $("#isCheckId").val()) {
				alert("중복체크는 필수사항 입니다.") ;
				$("#signup-signId").focus() ;
				return false ;
			}

			if ("" == $("#signup-name").val()) {
				alert("이름 를 입력하세요") ;
				$("#signup-name").focus() ;
				return false ;
			}

            if ("" == $("#signup-password").val()) {
				alert("비밀번호를 입력하세요") ;
				$("#signup-password").focus() ;
				return false ;
			}

			if ("" == $("#password_check").val()) {
				alert("비밀번호를 입력하세요") ;
				$("#password_check").focus() ;
				return false ;
			}

            if ($("#signup-password").val() != $("#password_check").val()) {
				alert("비밀번호를 확인하세요") ;
				$("#password_check").focus() ;
				return false ;
			}

			if ("" == $("#signPhoneNo").val()) {
				alert("전화번호를 입력하세요") ;
				$("#signPhoneNo").focus() ;
				return false ;
			}

			if ("" != $("#email").val() && !checkEmail($("#email").val())) {
				alert("올바른 이메일을 입력해 주세요") ;
				$("#signId").focus() ;
				return false ;
			}

			$("#signup-sendpassword").val(getRAS($("#signup-password").val()));

			$.ajax({
				url : "/login/signup",
				type : "post",
				dataType : "json",
				data : $("#signupForm").serialize(),
				success : function(data) {
                    alert(data.returnMessage) ;
					if (data.returnCode == 1) {
						location.reload() ;
					}
				}
			})
		})


    })

    function checkEmail(str) {
		var reg_email = /^([0-9a-zA-Z_\.-]+)@([0-9a-zA-Z_-]+)(\.[0-9a-zA-Z_-]+){1,2}$/ ;

		return reg_email.test(str) ;
	}

    function openModal(modalId) {
        $("body").addClass("show-modal");
        $("#" + modalId).toggleClass("active");
        return false;
    }

    function closeModal(obj) {
        $("body").removeClass("show-modal");
        $("#" + $(obj).closest(".modal-wrap").attr("id")).removeClass("active");
        return false;
    }


</script>

<body id="wrap" class="login-wrap">

<div class="login-head">
    <h1 class="login-logo">
        <img src="<c:url value="/resource/images/login/login_logo@3x.png"/>" alt="WONIKPNE">
    </h1>
</div>

<div class="login-body">
    <h2 class="login-title">로그인</h2>
    <p class="login-text">* 아이디와 비밀번호는 대소문자를 구분합니다.</p>

    <input type="hidden" id="publicKeyModulus" name="publicKeyModulus"
           value="<%=session.getAttribute("publicKeyModulus") %>">
    <input type="hidden" id="publicKeyExponent" name="publicKeyExponent"
           value="<%=session.getAttribute("publicKeyExponent") %>">

    <form id="signForm">
        <input type="hidden" id="sendpassword" name="signPassword">

        <div class="login-input">
            <input type="text" class="form-control" id="signId" name="signId" placeholder="아이디를 입력해주세요." maxlength="25">
        </div>

        <div class="login-input">
            <input type="password" class="form-control" id="password" placeholder="비밀번호를 입력해주세요." maxlength="25">
        </div>

        <div class="login-etc">
            <div class="login-etc-left">
                <input type="checkbox" id="saveSignId" value="1">
                <label for="saveSignId">아이디 저장</label>
            </div>
            <div class="login-etc-right">
                <a onclick="openModal('modal-find-id');" data-toggle="modal" data-target="#find_idpw"
                   style='cursor:pointer'>아이디 찾기</a>
                <a onclick="openModal('modal-find-pw');" data-toggle="modal" data-target="#find_idpw"
                   style='cursor:pointer'>비밀번호 재설정</a>
            </div>
        </div>

        <div class="login-btn color">
            <button type="button" class="jLogin">로그인</button>
        </div>
        <div class="login-btn line">
            <a href="javascript:void(0);" onclick="openModal('modal-signup');">사용자 등록</a>
        </div>
    </form>
</div>

<%-- 아이디 찾기 --%>
<div class="modal-wrap" id="modal-find-id">
    <div class="modal-content">

        <div class="modal-head">
            <h2 class="modal-head-title">아이디 찾기</h2>
            <button class="modal-close" onclick="closeModal(this)">닫기</button>
        </div>

        <div class="modal-body">
            <div class="modal-scroll">
                <div class="scroll-inner">
                    <form id="searchIdFm">
                        <ul class="form-list">
                            <li>
                                <label class="form-title">이름</label>
                                <div class="form-box">
                                    <input type="text" id="input_name_find_id" name="userName">
                                </div>
                            </li>
                            <li>
                                <label class="form-title">휴대폰 번호</label>
                                <div class="form-box form-find-tel-btn">
                                    <div class="tel">
                                        <input type="text" id="input_find_id_phone_no" class="number" name="phoneNo"
                                               placeholder="휴대폰 번호 숫자만 입력">
                                    </div>
                                    <div class="button">
                                        <button type="button" id="jGetVerificationCodeId">인증번호 받기</button>
                                    </div>
                                </div>
                                <div class="form-box form-find-tel-btn">
                                    <div class="tel">
                                        <input type="hidden" id="idValidationCode" name="validationCode" readonly>
                                        <input type="text" class="jVerificationCode number" name="verificationCode"
                                               placeholder="인증번호 6자리 숫자 입력" maxlength="6" disabled>
                                    </div>
                                    <div class="button">
                                        <button type="button" class="jConfirmVerificationCode" disabled>확인</button>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </form>

                    <hr class="sect-line">
                    <label class="form-title" id="label_ask_id" style="display: none"></label>
                    <div class="form-btn">
                        <a href="#" class="line" onclick="closeModal(this)">닫기</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<%-- 비밀번호 찾기 --%>
<div class="modal-wrap" id="modal-find-pw">
    <div class="modal-content">

        <div class="modal-head">
            <h2 class="modal-head-title">비밀번호 재설정</h2>
            <button class="modal-close" onclick="closeModal(this)">닫기</button>
        </div>

        <div class="modal-body">
            <div class="modal-scroll">
                <div class="scroll-inner">
                    <form id="searchPwFm">
                        <ul class="form-list">
                            <li>
                                <label class="form-title">아이디</label>
                                <div class="form-box">
                                    <input type="text" id="input_id_find_pw" name="userId">
                                </div>
                            </li>
                            <li>
                                <label class="form-title">이름</label>
                                <div class="form-box">
                                    <input type="text" id="input_name_find_pw" name="userName">
                                </div>
                            </li>
                            <li>
                                <label class="form-title">휴대폰 번호</label>
                                <div class="form-box form-find-tel-btn">
                                    <div class="tel">
                                        <input type="text" id="input_find_pw_phone_no" class="number" name="phoneNo"
                                               placeholder="휴대폰 번호 숫자만 입력">
                                    </div>
                                    <div class="button">
                                        <button type="button" id="jGetVerificationCodePw">인증번호 받기</button>
                                    </div>
                                </div>

                                <div class="form-box form-find-tel-btn">
                                    <div class="tel">
                                        <input type="hidden" id="pwValidationCode" name="validationCode" readonly>
                                        <input type="text" class="jVerificationCode number" name="verificationCode"
                                               placeholder="인증번호 6자리 숫자 입력" maxlength="6" disabled>
                                    </div>
                                    <div class="button">
                                        <button type="button" class="jConfirmVerificationCode" disabled>확인</button>
                                    </div>
                                </div>

                            </li>
                        </ul>
                    </form>
                    <hr class="sect-line">
                    <div class="form-btn">
                        <a href="#" class="line" onclick="closeModal(this)">닫기</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<%--회원 가입--%>
<div class="modal modal-wrap" id="modal-signup" data-keyboard="false" data-backdrop="static">
    <div class="modal-content">

        <div class="modal-head">
            <h2 class="modal-head-title">회원가입</h2>
            <button class="modal-close" onclick="closeModal(this)">닫기</button>
        </div>

        <div class="modal-body">
            <div class="modal-scroll">
                <div class="scroll-inner">
                    <h3 class="sect-title">개인 정보<span class="req">* 필수 입력 사항</span></h3>

                    <form id="signupForm">
                        <input type="hidden" id="signup-sendpassword" name="signPassword">
                        <ul class="form-list">
                            <li>
                                <label class="form-title">아이디<span class="req">*</span></label>
                                <div class="form-box form-checkbtn">
                                    <div class="input"><input type="text" id="signup-signId" name="signId"></div>
                                    <div class="button">
                                        <input type="hidden" id="isCheckId" value="false">
                                        <button type="button" id="checkId">ID 중복체크</button>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <label class="form-title">이름<span class="req">*</span></label>
                                <div class="form-box">
                                    <input type="text" id="signup-name" name="signName">
                                </div>
                            </li>
                            <li>
                                <label class="form-title">비밀번호<span class="req">*</span></label>
                                <div class="form-box">
                                    <input type="password" id="signup-password">
                                </div>
                            </li>
                            <li>
                                <label class="form-title">비밀번호 확인<span class="req">*</span></label>
                                <div class="form-box">
                                    <input type="password" id="password_check">
                                </div>
                            </li>
                            <li>
                                <label class="form-title">휴대폰 번호<span class="req">*</span></label>
                                <div class="form-box form-tel">
                                    <div class="tel">
                                        <input type="text" id="signPhoneNo" class="number" name="signPhoneNo"
                                               placeholder="휴대폰 번호 숫자만 입력">
                                    </div>
                                </div>
                            </li>
                            <li>
                                <label class="form-title">부서</label>
                                <div class="form-box">
                                    <input type="text" id="dept" name="dept">
                                </div>
                            </li>
                            <li>
                                <label class="form-title">직급</label>
                                <div class="form-box">
                                    <input type="text" id="position" name="position">
                                </div>
                            </li>
                            <li>
                                <label class="form-title">이메일</label>
                                <div class="form-box form-email">
                                    <div class="email"><input type="text" id="email" name="email"></div>
                                </div>
                            </li>
                        </ul>
                    </form>

                    <hr class="sect-line">
                    <div class="form-btn">
                        <a href="javascript:void(0);" class="line" onclick="closeModal(this)">취소</a>
                        <a href="javascript:void(0);" id="jSignup">등록</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>