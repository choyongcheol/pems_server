String.prototype.toHHMMSS = function () {
    var sec_num = parseInt(this, 10); // don't forget the second param
    console.log("sec_num")
    console.log(sec_num)
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    let time = hours + ":" + minutes +":" + seconds ;
    return time.substring(0, 8) ;
}
Number.prototype.toHHMMSS = function () {
    var sec_num = this
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    let time = hours + ":" + minutes +":" + seconds ;
    return time.substring(0, 8) ;
}

function nullToSpace(value) {
    return isEmpty(value) ? "" : value ;
}

function isEmpty(str) {
    return (typeof str == "undefined" || str == null || str == "") ;
}

function modalScroll() {
    let $window_height = $(window).outerHeight();

    $(".modal-content").each(function () {
        let $modal_height = $(this).outerHeight();
        let $scroll_height = $(this).find(".modal-body").outerHeight();
        let $mhead_height = $(this).find(".modal-head").outerHeight();
        if ($scroll_height >= $window_height - 60) {
            $(this).find(".modal-body").height($modal_height - $mhead_height);
        }
    });
}

function getRurl() {
    let path = window.location.pathname + "?" + window.location.search.substring(1);
    return encodeURIComponent(path);
}

function goUrl(theURL) {
    location.href = theURL;
}

// 오늘 일자
function getCommonNowDate(date) {
    date = isEmpty(date) ? new Date() : date ;
    return getCommonFullYear(date) + getCommonMonth(date) + getCommonDate(date) + getCommonTime(date);
}

function getCommonFullYear(date) {
    date = isEmpty(date) ? new Date() : date ;
    return date.getFullYear() + "년";
}

function getCommonMonth(date) {
    date = isEmpty(date) ? new Date() : date ;
    let month = date.getMonth() + 1;
    return (month < 10 ? "0" + month : month) + "월";
}

function getCommonDate(date) {
    date = isEmpty(date) ? new Date() : date ;
    let day = date.getDate();
    return (day < 10 ? "0" + day : day) + "일 ";
}

function getCommonTime(date) {
    date = isEmpty(date) ? new Date() : date ;
    return getCommonHours(date) + ":" + getCommonMinutes(date) + ":" + getCommonSeconds(date);
}

function getCommonHours(date) {
    date = isEmpty(date) ? new Date() : date ;
    let hour = date.getHours();
    return (hour < 10 ? "0" + hour : hour);
}

function getCommonMinutes(date) {
    let min = date.getMinutes();
    return (min < 10 ? "0" + min : min);
}

function getCommonSeconds(date) {
    date = isEmpty(date) ? new Date() : date ;
    let sec = date.getSeconds();
    return (sec < 10 ? "0" + sec : sec);
}

// 비밀번호 암호화
function getRAS(str) {
    let rtnStr = "";
    let publicKeyModulus = $("#publicKeyModulus").val();
    let publicKeyExponent = $("#publicKeyExponent").val();

    let rsa = new RSAKey();

    rsa.setPublic(publicKeyModulus, publicKeyExponent);

    // 사용자ID와 비밀번호를 RSA로 암호화한다.
    rtnStr = rsa.encrypt(str);

    // 암호화된 문자 뒤에 금일 일자 붙여야 함
    rtnStr = rtnStr + getRsaDate();

    return rtnStr;
}

function getRsaDate() {
    let date = new Date();
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let day = date.getDate();
    let hour = date.getHours();
    let min = date.getMinutes();
    let sec = date.getSeconds();
    return year + "" + (month < 10 ? '0' + month : month) + "" + (day < 10 ? '0' + day : day) + "" + (hour < 10 ? '0' + hour : hour) + "" + (min < 10 ? '0' + min : min) + (sec < 10 ? '0' + sec : sec);
}

$(document).on("keyup", ".number", function () {
    this.value = this.value.replace(/[^0-9]/g, "");
    // this.value = this.value.replace(/(^0+)/, "").replace(/[^0-9]/g, "");
});


