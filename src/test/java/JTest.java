import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.client.RestTemplate;
import pems.Constants;
import pems.utils.SmsUtil;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.net.URI;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class JTest {


	public static void main(String[] args) {

		try {
//			ranTest() ;
//			toJsonTest() ;
			sendSMSTest("01020362133") ;
//			stringTest("[1200.0,600.0]") ;
//			test("01020362133", "wonikpne", "원익피앤이입니다.'테스트(test)'") ;
		} catch (Exception e) {
			e.printStackTrace();
		}

	}


	private static void test(String phoneNumber, String templateCode, String message) throws Exception {
		
		long time = LocalDateTime.now(ZoneId.of("UTC")).toInstant(ZoneOffset.UTC).toEpochMilli();
		String signature = makeSignature(Constants.ALIMTALK_URL, time) ;


//		SmsUtil.toSendKakao(phoneNumber, templateCode, message) ;
	}


	public static void stringTest(String str) throws Exception {
		String aa = str.replaceAll("\\[", "").replaceAll("\\]", "") ;
		System.out.println("aa = " + aa);
//		String re = "[^a-zA-Z0-9 ㄱ-ㅎㅏ-ㅣ가-힣]" ;
//		String testStr = str.replaceAll(re, "") ;
//		System.out.println("testStr = " + testStr);
	}

	public static String sendSMSTest(String phoneNumber) throws Exception {
		HttpHeaders headers = new HttpHeaders();
		Map<String, Object> params = new HashMap<>();
		Map<String, Object> toNumber = new HashMap<>();
		String tempPw = "12345672" ;
		String meesage = "재설정된 임시 비밀번호는 [" + tempPw + "] 입니다.";
		long time = LocalDateTime.now(ZoneId.of("UTC")).toInstant(ZoneOffset.UTC).toEpochMilli();

		headers.add("content-type", "application/json");
		headers.add("x-ncp-iam-access-key", Constants.ACCESS_KEY);
		headers.add("x-ncp-apigw-timestamp", Long.toString(time));
		headers.add("x-ncp-apigw-signature-v2", makeSignature(Constants.SMS_URL, time));
		params.put("type", "SMS");
		params.put("from", "0312990100");
		params.put("content", meesage);
		toNumber.put("to", phoneNumber);
		params.put("messages", Collections.singletonList(toNumber));

		ObjectMapper mapper = new ObjectMapper();
		String body = mapper.writeValueAsString(params);
		System.out.println("body = " + body);

		HttpEntity<?> request = new HttpEntity<>(body, headers);
		RestTemplate restTemplate = new RestTemplate();

		restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8"))) ;

		System.out.println("Constants.N_CLOUD + Constants.SMS_URL = " + Constants.N_CLOUD + Constants.SMS_URL);

		String ret = restTemplate.postForObject(new URI(Constants.N_CLOUD + Constants.SMS_URL), request, String.class);
		System.out.println("ret = " + ret);

		return ret ;
	}

	public static String makeSignature(String url, Long time) throws Exception {
		String space = " ";                    // one space
		String newLine = "\n";                    // new line
		String method = "POST";                    // method
//        String url = "sms".equals(type) ? "/sms/v2/services/" + Constants.KEY_ID + "/messages" : "/alimtalk/v2/services/" + Constants.KAKAOKEY_ID + "/messages";
		String timestamp = Long.toString(time);            // current timestamp (epoch)

		String message = method + space + url + newLine + timestamp + newLine + Constants.ACCESS_KEY;

		System.out.println("message = " + message);
		SecretKeySpec signingKey = new SecretKeySpec(Constants.SECRET_KEY.getBytes(StandardCharsets.UTF_8), "HmacSHA256");
		Mac mac = Mac.getInstance("HmacSHA256");
		mac.init(signingKey);

		byte[] rawHmac = mac.doFinal(message.getBytes(StandardCharsets.UTF_8));
		return Base64.encodeBase64String(rawHmac);

	}


	public static void toJsonTest() throws Exception {

		Map<String, Object> map1 = new HashMap<>() ;
		Map<String, Object> map2 = new HashMap<>() ;

		map1.put("key11", "key1101") ;
		map1.put("key12", "key1202") ;
		map1.put("key13", "key1303") ;
		map2.put("key21", "key2101") ;
		map2.put("key22", "key2202") ;
		map2.put("key23", "key2303") ;
		map1.put("map2", map2) ;

		ObjectMapper mapper = new ObjectMapper() ;
		String json = mapper.writeValueAsString(map1) ;
		System.out.println("json = " + json);
	}

	public static void ranTest() throws Exception {
		Random ran = new Random();


		for (int i = 0; i < 100; i++) {
			int number = ran.nextInt(999999);
			String digital = String.format("%06d", number);
			System.out.println("digital = " + digital);
		}
	}


}
